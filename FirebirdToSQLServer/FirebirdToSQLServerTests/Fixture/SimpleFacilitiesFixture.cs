using Ramsoft.TestFixtures;

namespace FirebirdToSQLServerTests.Fixture
{
    internal class SimpleFacilitiesFixture : BaseFixture
    {
        public const string Facilities1 =
            @"{
                      ""FACILITIES"": {
                                    ""destination"": ""[dbo].[Facility]"",
                        ""identity"": ""FACILITYNAME"",
                        ""key"": [ ""int"", ""INTERNALFACILITYID"", ""InternalFacilityId"" ],
                        ""depends"": [ ""CACHEDISSUEROFPATIENTIDS"" ],
                        ""fields"": [
                          [ ""FACILITYNAME"", ""[FacilityName]"" ],
                          [ ""ISSUEROFPATIENTID"", ""InternalAssigningAuthorityID"", ""[dbo].[AssigningAuthority]"", ""[AssigningAuthority]"", ""DEFAULT"" ],
                          [ ""IMAGING"", ""IsImaging"" ],
                          [ ""REFERRING"", ""IsReferring"" ],
                          [ ""MAMMOTRACKING"", ""IsMammoTracking"" ],
                          [ ""*"", ""[ExtJSON]"" ]

                        ]
                      }
                    }";

        // fields: [
        //       [ InField, OutField, SelectTable, SelectField, SelectDefaultValue, DefaultValueType],
        // ]

        // a procedurally generated lookup string that lets us find the facility name in firebird
        // from its internalfacilityid internal PK.
        public const string FirebirdFacilityIdentityLookupSql1 =
            "SELECT FIRST 1 FACILITYNAME FROM FACILITIES WHERE INTERNALFACILITYID = @VALUE";


        public SimpleFacilitiesFixture() : base(Facilities1, "FACILITIES")
        {
            // firebird column info reader with a few fields,  FACILITYNAME, ISSUEROFPATIENTID
            MockFbReader.Setup(t => t.FieldCount).Returns(2);
            MockFbReader.Setup(t => t.GetName(0)).Returns("FACILITYNAME");
            MockFbReader.Setup(t => t.GetName(1)).Returns("ISSUEROFPATIENTID");
            MockFbReader.Setup(t => t.GetFieldType(0)).Returns(typeof (string));
            MockFbReader.Setup(t => t.GetFieldType(1)).Returns(typeof (string));
            MockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("varchar");
            MockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("varchar");


            MockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(3);

            MockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("InternalAssigningAuthorityID");
            // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("nvarchar");

            MockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("FacilityName");
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("nvarchar");

            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            MockSqlColumnInfo.Setup(t => t.GetName(2)).Returns("ExtJSON"); // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("nvarchar");
        }
    }
}