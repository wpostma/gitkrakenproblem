﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RamSoft.FirebirdToSqlServer.Exceptions
{

    [Serializable()]
    public class DataConversionFailureException : ApplicationException
    {
        public DataConversionFailureException() { }
        public DataConversionFailureException(string message) : base(message) { }

        public DataConversionFailureException(string message, Exception inner) : base(message, inner) { }
    }


    [Serializable()]
    public class DataConversionNoOperationException : ApplicationException
    {
        public DataConversionNoOperationException() { }
        public DataConversionNoOperationException(string message) : base(message) { }
    }

    [Serializable()]
    public class MappingFailureException : ApplicationException
    {
        public MappingFailureException() { }
        public MappingFailureException(string message) : base(message) { }
    }

    [Serializable()]
    public class DicomDataFormatException : ApplicationException
    {
        public DicomDataFormatException() { }
        public DicomDataFormatException(string message) : base(message) { }
    }

}
