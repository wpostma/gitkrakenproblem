### Firebird To SQL Server Tools

This is a utility for converting RamSoft PACS database data that is in Firebird into a new SQL schema 
designed for native SQL Server 2016.

## INTERACTIVE USE

The interactive version looks like this:

![screenshot](FirebirdToSQLServer/screenshot.png)


## CONFIGURATION

The configuration/mapping is done via a Mapping.JSON file which looks like this:

![screenshot](FirebirdToSQLServer/config_screenshot.png)

A configuration file starts at the top level as a dictionary, containing one or more tables, a minimal complete example is shown:

    {
    "ROLELIST": {
        "destination": "[dbo].[Role]",
        "identity": "ROLENAME",
        "key": [ "int", "ROLEID", "[InternalRoleId]" ],
        "fields": [
        [ "ROLENAME", "[NAME]" ],
        [ "*", "[ExtJSON]" ]

        ]
    }
    }
    
* The incoming table is the firebird table named ROLELIST
* The destination MS SQL table, and all MS SQL entities use brackets, so that the reader of the file can keep it straight.
* It so happens that the whole thing is case insensitive, but an effort to use all capitals for firebird identifiers is made.
* The "*" is a wildcard and it means "copy every field from source table in a json-typed field in the destination table.
* Things not shown in the above sample:
    + Lookups
    + Required flags
    + Indirection
    + Custom queries
    
* Eventually a proper set of documentation will be required, which is not yet made.     
 
 
 
 
## Written by Warren Postma, &copy; 2016 RamSoft inc
 

