using System;
using System.Linq;
using RamSoft.FirebirdToSqlServer.Exceptions;

namespace RamSoft.FirebirdToSqlServer.Util
{
    /// <summary>
    /// DicomNameHelper helps with creating dicom syntax names, see  DICOM specification section
    /// 6.2.1 Person Name (PN) Value Representation.
    /// </summary>
    public class DicomPersonNameHelper
    {
        private static char[] _dicomPnSep = new char[] { '^' };

        private string[] _field;

        public string FamilyName { get { return _field[0]; } set { _field[0] = value; } }
        public string GivenName { get { return _field[1]; } set { _field[1] = value; } }
        public string MiddleName { get { return _field[2]; } set { _field[2] = value; } }
        public string NamePrefix { get { return _field[3]; } set { _field[3] = value; } }
        public string NameSuffix { get { return _field[4]; } set { _field[4] = value; } }

        public DicomPersonNameHelper()
        {
            _field = new string[5];

        }

        // WINDOW LEVEL - VALUE MULTIPLICITY (ARRAY VALUES)

        // Patient Name (PN)  - part 7 - STRUCTURE HANDLING FOR CARET (^) DELIMETED NAME
        public DicomPersonNameHelper(   string afamilyName,
            string agivenName,
            string amiddleName = "",
            string anamePrefix = "",
            string anameSuffix = "")
        {
            _field = new string[5];
            _field[0] = afamilyName;
            _field[1] = agivenName;
            _field[2] = amiddleName;
            _field[3] = anamePrefix;
            _field[4] = anameSuffix;


        }

        public DicomPersonNameHelper( string formattedInput )
        {
            _field = new string[5];
            if (!String.IsNullOrEmpty(formattedInput))
            {

                var elements = formattedInput.Split(_dicomPnSep,  StringSplitOptions.None);
                if (elements.Count() > 5)
                    throw new DicomDataFormatException("More than 5 elements in formatted input for PN is invalid");
                for (int i = 0; i < 5;i++)
                {
                    if (i < elements.Count())
                    {
                        _field[i] = elements[i];
                    }
                    else
                    {
                        _field[i] = String.Empty;
                    }
                }

            }


        }

        public override string ToString()
        {
            return FormattedString();
        }

        public string FormattedString()
        {
            string fmt = FamilyName;
            int last = 2;
            for (int n = 4; n>2;n--)
            {
                if (!String.IsNullOrEmpty(_field[n]))
                {
                    last = n;
                    break;
                }
            }

            for (int n = 1; n <= last;n++)
            {
                fmt = fmt + "^" + _field[n];
            }

            if (fmt.Length < 64)
            {
                return fmt;
            }
            else
            {
                return fmt.Substring(0, 63);
            }


        }

    }
}