namespace RamSoft.FirebirdToSqlServer.Model
{
    public enum CopyType
    {
        Direct,
        Lookup
    }
}