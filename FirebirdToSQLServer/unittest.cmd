@echo off
if not defined DevEnvDir ( call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat" )
REM cd FirebirdToSQLServerTests\bin\Debug
REM test results output is nicer but generates annoying output folder junk
REM mstest /detail:duration /testcontainer:FirebirdToSQLServerTests.dll 
REM no test results folder, but also no execution times
vstest.console.exe FirebirdToSQLServerTests\bin\Release\FirebirdToSQLServerTests.dll
