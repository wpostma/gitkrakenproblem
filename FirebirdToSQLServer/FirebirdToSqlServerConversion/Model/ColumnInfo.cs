using System.Collections.Generic;
using RamSoft.FirebirdToSqlServer.Interface;

namespace RamSoft.FirebirdToSqlServer.Model
{
    public class ColumnInfo : IColumnInfo
    {
        protected List<ColumnInfoItem> _items;
        public string GetDataTypeName(int index)
        {
            return _items[index].DataTypeName;
        }

        public int GetFieldCount()
        {
            return _items.Count;
        }

        public string GetName(int index)
        {
            return _items[index].Name;
        }

        public void AddColumnInfoItem(string name, string dataTypeName)
        {
            var item = new ColumnInfoItem
            {
                DataTypeName = dataTypeName,
                Name = name
            };
            _items.Add(item);
        }

        public ColumnInfo()
        {
            _items = new List<ColumnInfoItem>();
        }
    }
}