namespace RamSoft.FirebirdToSqlServer
{
    public class ColumnInfoItem
    {
        public string  DataTypeName { get; set; }
        public string  Name { get; set; }
    }
}