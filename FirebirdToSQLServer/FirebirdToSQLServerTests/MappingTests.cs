﻿using System.Data;
using FirebirdToSQLServerTests.Fixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json.Linq;
using Ramsoft.TestFixtures;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Exceptions;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;
using StatementType = RamSoft.FirebirdToSqlServer.Interface.StatementType;

// unit under test: FirebirdToSqlMap
//   fields as a single element inside a map (ExportMappedField)
//   maps as a list of fields (ExportMapping)
// All tests in this unit focus on a simple single level table without lookups (foreign keys).

namespace RamSoft.FirebirdToSqlServer
{
    // Tests...


    [TestClass]
    public class MappingTests
    {
        //
        private const string FmtTemplate1 = @"{{
                      ""{0}"": {{
                        ""destination"": ""{1}"",
                        ""oldkey"": [ ""int"", ""SOMEOLDKEY"" ],
                        ""newkey"": [ ""bigint"", ""SomeNewKey"" ],
                        ""identity"": ""ITEMNAMEIN"",
                        ""fields"": [
                          [ ""ITEMNAMEIN"", ""[ItemNameOut]"" ] ,
                          {2} ,
                          [ ""*"", ""[ExtJSON]"" ]
                        ]
                      }}{3}

                }}";


        private const string Countrylist1 = @"{
                      ""COUNTRYLIST"": {
                        ""destination"": ""[tzdb].[Country]"",
                        ""oldkey"": [ ""int"", ""id"" ],
                        ""newkey"": [ ""char(2)"", ""CountryCode"" ],
                        ""identity"": ""CODE"",
                        ""fields"": [
                          [ ""CODE"", ""[CountryCode]"" ],
                          [ ""COUNTRYNAME"", ""[CountryName]"" ],
                          [ ""*"", ""[ExtJSON]"" ]
                        ]
                      }

                }";

        // has no destination, no identity, no key information, only fields.
        private const string CountrylistOnlyfields = @"{
                      ""COUNTRYLIST"": {
                        ""fields"": [
                          [ ""CODE"", ""[CountryCode]"" ],
                          [ ""COUNTRYNAME"", ""[CountryName]"" ],
                          [ ""*"", ""[ExtJSON]"" ]
                        ]
                      }

                }";


        private const string FacilitiesinsertSql1 =
            "INSERT INTO [dbo].[Facility] ( [FacilityName], [InternalAssigningAuthorityID], [ExtJSON] ) OUTPUT INSERTED.InternalFacilityId VALUES (  @FacilityName,  @InternalAssigningAuthorityID, @ExtJSON )";


        [TestMethod]
        [TestCategory("Mapping.Basic")]
        public void MappingFieldBasics()
        {
            // Fields exist in order to map a column in Firebird to a column in MS SQL,
            // and have basic ToString behaviour

            // Missing key values shown via ToString as ? (unknown)
            var f0 = new ExportMappedField();
            var emptyOutputExample = "? ? => ? ?";
            Assert.IsTrue(f0.ToString() == emptyOutputExample, "Expected field format " + emptyOutputExample);

            // Minimal complete mapped-field debug output
            var f1 = new ExportMappedField();
            f1.InField = "FIREBIRDFIELD";
            f1.InDbType = "int";
            f1.OutField = "SqlServerField";
            f1.OutDbType = "smallint";
            var outputExample = "FIREBIRDFIELD int => SqlServerField smallint";
            Assert.IsTrue(f1.ToString() == outputExample, "Expected field format " + outputExample);
        }

        /// <summary>
        ///  Create a complete minimal Mapping.JSON containing a standard test case table with one or more custom fields
        /// </summary>
        /// <param name="srcTableName">optional: incoming table name</param>
        /// <param name="destTableName">optional: outgoing table name</param>
        /// <param name="fieldDeclaration">optional: field declaration in brackets</param>
        /// <returns></returns>
        private static string FakeTableWithFieldStr(string srcTableName = "FIREBIRDTABLE",
            string destTableName = "[dbo].[MSSqlTable]",
            string fieldDeclaration = "[ \"A\", \"[B]\" ]",
            string otherTables = "")
        {
            return string.Format(FmtTemplate1, srcTableName, destTableName, fieldDeclaration, otherTables);
        }


        [TestMethod]
        [TestCategory("Mapping.Identity")]
        public void MappingFieldSubIdentity()
        {
            // New feature, March 23: Anonymous Sub Identity with in-field specified as "+"
            // outfield specified as an IDENTITY(1,1) value in some table, in this case, [phi].[ImagingServiceRequest]
            var fkStr = "InternalImagingServiceRequestId";

            // Create a complete minimal Mapping.JSON containing a standard test case table with one or more custom fields
            string tableDeclarationStr = FakeTableWithFieldStr(fieldDeclaration:
                $@"[ ""+"", ""[{fkStr}]"", ""[phi].[ImagingServiceRequest]"",
           [ [ ""INTERNALPATIENTID"", ""[InternalPatientId]"", ""[phi].[Patient]"", ""[InternalPatientID]"", ""int"" ],
            [ ""ACCESSIONNUMBER"", ""[AccessionNumber]"" ]
           ]
           ] ");

            // fakes/fixtures
            var mockFbReader = new Mock<IDataReader>(); // supports IDataReader
            var mockSqlColumnInfo = new Mock<IColumnInfo>(); // supports IDataReader
            SetupMockReaders(mockFbReader, mockSqlColumnInfo, fkStr);
                // Want somewhere for the sub-identity to land in the real schema.

            var mapdict = JObject.Parse(tableDeclarationStr);
            Assert.IsTrue(mapdict != null, "Dictionary must be readable");

            //--------- map is the item under test!-----------------------
            // Get our table which contains 3 fields, the one we passed in above and two bog standard boilerplate ones.
            var m1 = new ExportMapping("FIREBIRDTABLE", mapdict);
            //------------------------------------------------------------

            Assert.IsTrue(m1 != null, "map object constructor should not return nil");
            Assert.IsTrue(!m1.IsFaulted(),
                "map should not be faulted, perhaps JSON dictionary entry with name FIREBIRDTABLE was not found");


            // If this throws unable-to-read-fields exception then the mock reader is not good.
            m1.MapFields(mockFbReader.Object, mockSqlColumnInfo.Object, null);
            Assert.IsTrue(!m1.IsFaulted(),
                "map should not be faulted after MapFields, something was flagged as invalid while parsing map fields.");

            Assert.AreEqual(3, m1.GetSelectedFieldsCount(),  "output three fields");

            Assert.AreEqual(1, m1.GetMinorErrorCount());

            var field1 = m1.GetSelectedField(0);
            var field2 = m1.GetSelectedField(1);
            var field3 = m1.GetSelectedField(2);

            Assert.AreEqual("ITEMNAMEIN", field1.InField );
            Assert.AreEqual("ItemNameOut", field1.OutField );
            Assert.IsTrue(!field1.SecondaryFlag);
            Assert.IsTrue(field1.GetSubFieldCount() == 0, "Should not have sub fields in ITEMNAMEIN");

            Assert.AreEqual("+", field2.InField);
            Assert.AreEqual(fkStr, field2.OutField);
            Assert.IsTrue(field2.SecondaryFlag);
            Assert.IsTrue(field2.GetSubFieldCount() == 2, "Should have 2 sub fields in \"+\" field");

            Assert.AreEqual(  expected:null, actual:field2.GetSubFieldInsertSQL(),
                              message: "Sub Field Insert SQL not set yet");

            Assert.AreEqual("*", field3.InField);
            Assert.AreEqual("ExtJSON", field3.OutField);
            Assert.IsTrue(!field3.SecondaryFlag);


            // This is the magic bit: The + field has two sub-fields.

            var subfield1 = field2.GetSubField(0);
            Assert.AreEqual("INTERNALPATIENTID", subfield1.InField.ToUpper());
            Assert.AreEqual("INTERNALPATIENTID", subfield1.OutField.ToUpper());
            Assert.IsTrue(subfield1.SecondaryFlag);


            var subfield2 = field2.GetSubField(1);
            Assert.AreEqual("ACCESSIONNUMBER", subfield2.InField.ToUpper());
            Assert.AreEqual("ACCESSIONNUMBER", subfield2.OutField.ToUpper());
            Assert.IsTrue(subfield1.SecondaryFlag);


            // check generated top level INSERT sql


            string actualSqlIns = TestHelper.StripCommentsAndWhiteSpace(m1.BuildStatement(StatementType.Insert));
            string expectSqlIns = $"INSERT INTO [dbo].[MSSqlTable] ( [ItemNameOut], [{fkStr}], [ExtJSON] ) " +
                                  $"OUTPUT INSERTED.SomeNewKey VALUES ( @ItemNameOut, @{fkStr}, @ExtJSON )";
            Assert.AreEqual(expectSqlIns, actualSqlIns);


            // check generation of sub-insert statements.
            int count = m1.BuildSubInsertStatements();
            Assert.AreEqual(1, count);

            // after BuildSubInsertStatements, field2 should contain some SQL for a sub-insert.
            var actualSubInsert = TestHelper.StripCommentsAndWhiteSpace(field2.GetSubFieldInsertSQL());
            var expectSubInsert = $"INSERT INTO [phi].[ImagingServiceRequest] ( [InternalPatientId], [AccessionNumber] ) OUTPUT INSERTED.[{fkStr}] VALUES ( @InternalPatientId, @AccessionNumber )";
            Assert.AreEqual(expectSubInsert, actualSubInsert, "Sub Field Insert SQL generation is incorrect");
        }

        /// <summary>
        ///     Extracted repetitive mock-reader setup.
        /// </summary>
        /// <param name="mockFbReader"></param>
        /// <param name="mockSqlColumnInfo"></param>
        /// <param name="fkStr"></param>
        private static void SetupMockReaders(Mock<IDataReader> mockFbReader, Mock<IColumnInfo> mockSqlColumnInfo,
            string fkStr)
        {
            mockFbReader.Setup(t => t.FieldCount).Returns(3);
            mockFbReader.Setup(t => t.GetName(0)).Returns("SOMEOLDKEY");
            mockFbReader.Setup(t => t.GetFieldType(0)).Returns(typeof (int));
            mockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("int");

            mockFbReader.Setup(t => t.GetName(1)).Returns("ITEMNAMEIN");
            mockFbReader.Setup(t => t.GetFieldType(1)).Returns(typeof (string));
            mockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("varchar");

            mockFbReader.Setup(t => t.GetName(3)).Returns("SOMESTRING");
            mockFbReader.Setup(t => t.GetFieldType(3)).Returns(typeof (string));
            mockFbReader.Setup(t => t.GetDataTypeName(3)).Returns("varchar");


            mockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(4);

            mockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("SomeNewKey"); // NOT in the same order as above!!
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("int");

            mockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("Address");
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("nvarchar");

            mockSqlColumnInfo.Setup(t => t.GetName(2)).Returns(fkStr);
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("int");

            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            mockSqlColumnInfo.Setup(t => t.GetName(3)).Returns("ExtJSON"); // NOT in the same order as above!!
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(3)).Returns("nvarchar");
        }


        [TestMethod]
        [TestCategory("Mapping.Fail")]
        [ExpectedException(typeof (MappingFailureException))]
        public void MappingNonExistantTableThrowsConfigurationError()
        {
            // fakes/fixtures
            var mockFbReader = new Mock<IDataReader>(); // supports IDataReader
            var mockSqlColumnInfo = new Mock<IColumnInfo>(); // supports IDataReader

            var mapdict = JObject.Parse(Countrylist1);
            Assert.IsTrue(mapdict != null, "Dictionary must be readable");

            // map is the item under test!

            // check handling when we read a non-existent table (content missing from mapping.json)
            var m1 = new ExportMapping("NONEXISTANTFIREBIRDTABLE", mapdict);
            Assert.IsTrue(m1 != null, "map object constructor should not return nil");
            Assert.IsTrue(m1.IsFaulted());


            // Expect this throws:
            m1.MapFields(mockFbReader.Object, mockSqlColumnInfo.Object, null);

            //not expected to reach end of function.
        }


        [TestMethod]
        [TestCategory("Mapping.Basic")]
        public void MappingUpdateBasics()
        {
            var fac = new SimpleFacilitiesFixture();

            fac.Mapping.MapFields(fac.MockFbReader.Object, fac.MockSqlColumnInfo.Object, null);
            Assert.AreEqual(3, fac.Mapping.GetSelectedFieldsCount());

            string s = fac.Mapping.GetFirebirdIdentityFieldListStr("FACILITIES");
            Assert.AreEqual("FACILITYNAME", s);

            string update1 = TestHelper.StripCommentsAndWhiteSpace(fac.Mapping.BuildStatement(StatementType.Update));

            var expect =
                "UPDATE [dbo].[Facility] SET [ExtJSON] = @ExtJSON WHERE ([FacilityName] = @FacilityName ) AND ([InternalAssigningAuthorityID] = @InternalAssigningAuthorityID )";

            Assert.AreEqual(expect, update1);
        }

        [TestMethod]
        [TestCategory("Mapping.Basic")]
        public void MappingMapBasics()
        {
            // fakes/fixtures
            var mockFbReader = new Mock<IDataReader>(); // supports IDataReader
            var mockSqlColumnInfo = new Mock<IDataReader>(); // supports IDataReader

            // Configuration content in application file Mapping.json simulated and ability to read that
            // json dictionary content. At the end we expect the code to die with mapping failure exception.

            var mapdict = JObject.Parse(Countrylist1);


            // check json is readable (sanity test)
            var tableDict = (JObject) mapdict["COUNTRYLIST"];
            var dest = (string) tableDict["destination"];
            Assert.IsTrue(dest == "[tzdb].[Country]", "Sanity test failure, JSON parser not working.");


            // now we introduce the map to a reader.


            // check handling when we read a non-existant table (content missing from mapping.json)
            var mapping = new ExportMapping("COUNTRYLIST", mapdict);
            Assert.AreEqual(0, mapping.GetAllFieldsCount(), 0.01,
                "Count of fields is zero until the dictionary is applied to a real field list from an SQL reader");
            //Assert.AreEqual("CODE", mapping.identity); // "field identity value should be CODE"

            // do not call BuildStatement, we want this test NOT to raise, so we know we reach this point.

            string inkeyfieldtype;
            string inkeyfieldname;
            string outkeyfieldtype;
            string outkeyfieldname;
            bool result1 = mapping.GetKeyFieldInfo("COUNTRYLIST", out inkeyfieldtype, out inkeyfieldname,
                out outkeyfieldtype, out outkeyfieldname);
            Assert.IsTrue(result1);
            Assert.AreEqual(inkeyfieldname, "id");
            Assert.AreEqual(inkeyfieldtype, "int");
        }

        [TestMethod]
        [TestCategory("Mapping.Identity")]
        public void MappingMapGetIdentityStuff()
        {
            // tests for firebird identity mapping
            // GetFirebirdIdentityFieldListStr
            // GetFirebirdIdentityFieldListFromDict
            // GetFieldLookupFbIdentityMapSql

            var mapdict = JObject.Parse(Countrylist1);
            var mapping = new ExportMapping("COUNTRYLIST", mapdict);

            string identity1 = mapping.GetFirebirdIdentityFieldListStr("COUNTRYLIST");
            Assert.AreEqual("CODE", identity1);

            // this test is repeated in case of gross side effects in
            for (var i = 0; i < 99; i++)
            {
                string query1 = mapping.GetFieldLookupFbIdentityMapSql("[tzdb].[Country]");
                Assert.AreEqual("SELECT FIRST 1 CODE FROM COUNTRYLIST WHERE id = @VALUE", query1);
            }


            // fallback case: identity is not explicitly defined, so the first field is the user-identity (row-or-item-or-object-name).

            var mapdict2 = JObject.Parse(CountrylistOnlyfields);
            var mapping2 = new ExportMapping("COUNTRYLIST", mapdict2);

            string identity2 = mapping2.GetFirebirdIdentityFieldListStr("COUNTRYLIST");
            Assert.AreEqual("CODE", identity2);
        }

        [TestMethod]
        [TestCategory("Mapping.Identity")]
        public void MappingIdentityJSONListCase()
        {
            const string jsonlist = @"{ ""PATIENTMODULE"": {
                ""destination"": ""[phi].[Patient]"",
                ""identity"": [ ""PATIENTID"", ""ISSUEROFPATIENTID"" ] } }";

            var mapdict = JObject.Parse(jsonlist);
            var mapping = new ExportMapping("PATIENTMODULE", mapdict);

            string identity = mapping.GetFirebirdIdentityFieldListStr("PATIENTMODULE");
            Assert.AreEqual("PATIENTID, ISSUEROFPATIENTID", identity);

            // related? mapping.GetMicrosoftSqlUpdateWhereClause()
        }


        [TestMethod]
        [TestCategory("Mapping.Fail")]
        [ExpectedException(typeof (MappingFailureException))]
        public void MappingMapThrowMappingFailure()


        {
            var mapdict = JObject.Parse(Countrylist1);
            var mapping = new ExportMapping("COUNTRYLIST", mapdict);
            Assert.AreEqual(0, mapping.GetAllFieldsCount(), 0.01,
                "Count of fields is zero until the dictionary is applied to a real field list from an SQL reader");
            //Assert.AreEqual("CODE", mapping.identity); // "field identity value should be CODE"

            // now we introduce the map to a reader.
            var mockFbReader = new Mock<IDataReader>(); // supports IDataReader
            var mockSqlColumnInfo = new Mock<IColumnInfo>(); // supports IDataReader


            // now we introduce the map to a reader, and check that when the firebird table exists in the mapping
            // but we can't read the fields from the physical DB (here signified by our useless mock)
            // it will throw MappingFailureException
            mapping.MapFields(mockFbReader.Object, mockSqlColumnInfo.Object, null);
            mapping.BuildStatement(StatementType.Insert);
        }

        [TestMethod]
        [TestCategory("Mapping.Basic")]
        public void MappingMapSimpleCase()
        {
            // We plan to start from this point and go in a lot of different directions
            // so the repeated code is moved into this case fixture.
            var x = new SimpleFacilitiesFixture();


            // x.mapping = map object under test, set up with mocks for the IDataReaders it needs
            // to be able to build an SQL Server insert statement.
            x.Mapping.MapFields(x.MockFbReader.Object, x.MockSqlColumnInfo.Object, null);

            string sql2 = TestHelper.StripCommentsAndWhiteSpace(FacilitiesinsertSql1);

            // BuildStatement must be repeatable without side-effects.
            for (var i = 0; i < 99; i++)
            {
                // normalized "INSERT INTO <table> (  <fields> ) OUTPUT INSERTED.<pk> VALUES (  @params,... )"
                string sql1 = TestHelper.StripCommentsAndWhiteSpace(
                    x.Mapping.BuildStatement(StatementType.Insert));


                Assert.AreEqual(sql2, sql1);
            }

            Assert.AreEqual(3, x.Mapping.GetAllFieldsCount());
            Assert.AreEqual(3, x.Mapping.GetSelectedFieldsCount());

            // Verify map field-level SELECTED FIELD content is correct.
            var field1 = x.Mapping.GetSelectedField(0);
            Assert.AreEqual("nvarchar", field1.OutDbType);
            Assert.AreEqual("FACILITYNAME", field1.InField);
            Assert.AreEqual("FacilityName", field1.OutField);
            Assert.AreEqual(null, field1.SelectField);
            Assert.AreEqual(0, field1.Index);
            Assert.IsTrue(field1.InsertOutField);
            Assert.IsFalse(field1.UpdateSetField);
            // false because the existing entity checks in the tool assume FACILITYNAME is how we tell which facility is which. RAMSOFT is teh

            // [ "ISSUEROFPATIENTID", "InternalAssigningAuthorityID", "[dbo].[AssigningAuthority]", "[AssigningAuthority]", "DEFAULT"            ],
            //        ^InField^            ^OutField^                         ^SelectTable^              ^SelectField^      ^SelectDefaultValue^
            var field2 = x.Mapping.GetSelectedField(1);
            Assert.AreEqual("nvarchar", field2.OutDbType);
            Assert.AreEqual("ISSUEROFPATIENTID", field2.InField);
            Assert.AreEqual("InternalAssigningAuthorityID", field2.OutField);
            Assert.AreEqual(1, field2.Index);

            Assert.AreEqual("[AssigningAuthority]", field2.SelectField); // QUESTION: Why does SelectField contain brackets?
            Assert.AreEqual("[dbo].[AssigningAuthority]", field2.SelectTable); // QUESTION: Why does SelectField contain brackets?
            Assert.AreEqual("DEFAULT", field2.SelectDefaultValue);
            Assert.AreEqual(null, field2.SelectFieldType );



            Assert.IsTrue(field2.InsertOutField);
            Assert.IsFalse(field2.UpdateSetField); // foreign identity, so don't UPDATE.

            // Any field which should be included in an UPDATE  has UpdateSetField
            // set.
            var field3 = x.Mapping.GetSelectedField(2);
            Assert.AreEqual("*", field3.InField);
            Assert.AreEqual("ExtJSON", field3.OutField);
            Assert.IsTrue(field3.UpdateSetField); // ExtJSON field should be updated on an UPDATE .
        }

        [TestMethod]
        [TestCategory("Mapping.Basic")]
        public void MappingMapSimpleCase2()
        {
            // We plan to start from this point and go in a lot of different directions
            // so the repeated code is moved into this case fixture.
            var x = new SimpleFacilitiesFixture();


            // x.mapping = map object under test, set up with mocks for the IDataReaders it needs
            // to be able to build an SQL Server insert statement.
            x.Mapping.MapFields(x.MockFbReader.Object, x.MockSqlColumnInfo.Object, null);
            // normalized "INSERT INTO <table> (  <fields> ) OUTPUT INSERTED.<pk> VALUES (  @params,... )"
            string sql1 = TestHelper.StripCommentsAndWhiteSpace(
                x.Mapping.BuildStatement(StatementType.Insert));
            string sql2 = TestHelper.StripCommentsAndWhiteSpace(FacilitiesinsertSql1);
            Assert.AreEqual(sql2, sql1);

            Assert.AreEqual(3, x.Mapping.GetAllFieldsCount());
            Assert.AreEqual(3, x.Mapping.GetSelectedFieldsCount());


            // NOTE: differs from previous case by checking GetAllField instead of GetSelectedField
            var field1 = x.Mapping.GetAllField(0);
            Assert.AreEqual("nvarchar", field1.OutDbType);
            Assert.AreEqual("FACILITYNAME", field1.InField);
            Assert.AreEqual("FacilityName", field1.OutField);
            Assert.AreEqual(null, field1.SelectField);
            Assert.AreEqual(0, field1.Index);
            Assert.IsTrue(field1.InsertOutField);

            //  params: bool addBrackets = true, string prefix = "  "
            //
            string fieldNames = x.Mapping.GetMicrosoftSqlSelectFieldNames(true);
        }

        [TestMethod]
        [TestCategory("Mapping.Lookup")]
        // [ExpectedException(typeof(System.Collections.Generic.KeyNotFoundException))]  --> Now we catch and swap to MappingFailureException.
        [ExpectedException(typeof (MappingFailureException))]
        public void MappingGetFieldLookupFbIdentityMapSqlThrowsMappingFailureException()
        {
            // We plan to start from this point and go in a lot of different directions
            // so the repeated code is moved into this case fixture.
            var x = new SimpleFacilitiesFixture();

            // we used to expect System.Collections.Generic.KeyNotFoundException
            // now we translate that to an application exception.
            string identifier = x.Mapping.GetFieldLookupFbIdentityMapSql("x");
        }


        [TestMethod]
        [TestCategory("Mapping.Lookup")]
        public void MappingGetFieldLookupTableIdentityQuery()
        {
            // In this test, we're expecting a known firebird-SQL SELECT expression to be returned.
            //
            // It is important to know, given an MS SQL Table name like [dbo].[Facility]
            // how to fetch the data from firebird that gave us the current row in [dbo].[Facility]
            // So if we have a row in [dbo].[Facility], we need to first find the firebird
            // table name from that SQL table name, then we need to generate a query so that
            // we can translate from a non-user significant value like the PK to a user-significant
            // value. For example, a resource named ROOM101, the user significant
            // value would be ResourceName or something like that. It might be
            // that a pair of fields can together form a user-identity (a person's last
            // name and first name, maybe is in fact the canonical informal identity example,
            // because it's non-unique, there could be more than one John Smith.)
            //

            // We plan to start from this point and go in a lot of different directions
            // so the repeated code is moved into this case fixture.
            var x = new SimpleFacilitiesFixture();

            // expect NOT to get System.Collections.Generic.KeyNotFoundException
            // instead expect some SELECT TOP 1 x from y where z = q
            string sql1 =
                TestHelper.StripCommentsAndWhiteSpace(
                    x.Mapping.GetFieldLookupFbIdentityMapSql("dbo.Facility"));
            Assert.AreEqual(SimpleFacilitiesFixture.FirebirdFacilityIdentityLookupSql1, sql1);

            string sql2 =
                TestHelper.StripCommentsAndWhiteSpace(x.Mapping.GetFieldLookupFbIdentityMapSql("[dbo].[Facility]"));
            Assert.AreEqual(sql1, sql2);
        }

        [TestMethod]
        [TestCategory("Mapping.Basic")]
        public void MappingWriterBasics()
        {
            var x = new WriterFacilitiesFixture();

            Assert.AreNotEqual(null, x.Writer);
            Assert.AreNotEqual(null, x.Mapping);

            int n = x.Writer.Execute();

            Assert.AreEqual(2, n); //  Writes two rows.


            // It.IsAny<string> --> accept all values.
            // It.IsRegex("-- .*INSERT.*") --> Accept certain values.

            // verify it has had the expected interactions with the DAL:
            x.MockDal.Verify(v => v.SetupInsertSql(It.IsAny<string>()),
                Times.Once());
            x.MockDal.Verify(v => v.Commit(0), Times.Between(2, 3, Range.Inclusive));

            x.MockDal.Verify(v => v.InsertSqlExecute(0), Times.Exactly(2));

            int m = x.Writer.WriteRowWithDefaults(); // this would be expected to bump the InsertSqlExecute count.

            x.MockDal.Verify(v => v.InsertSqlExecute(0), Times.Exactly(3));
        }

        [TestMethod]
        [TestCategory("Mapping.Complex")]
        public void MappingPatientDoesNotMapAutoIdentityField()
        {
            // this test case is isolated from the real db, and uses a mock (moq) or "fake" object to simulate the data
            // access layer.
            //
            // There is a clever auto-matchup feature so that we don't even have to specify a large list of UNMODIFIED
            // fields where the field name before is the same as the field name after.
            // An exception to that clever rule is where the new field is auto-generated like the InternalPatientID field
            // which is IDENTITY(1,1) and must not be specified in the generated INSERT statement.
            //
            // Another clever thing we need in the patient translation is the ability to do the @LOOKUP syntax
            // shown here. This is a special dereferenced lookup where the left side (firebird) has a string user name
            // stored and the right side (MS SQL) has the better designed schema, where we use the foreign key.
            // When the left side is DENORMALIZED and the right side is NORMALIZED, we have an imbalance, which
            // we correct with the @FIELDNAME syntax which will find the InternalUserID value for the field.


            // NOTE: Features which access the "existing" property shown here are tested in DuplicateCheckingTests.

            // TODO: Test the depends clause.

            // TODO: Test IDENTITY rules.

            const string patientmodule1 = @"{

            ""AMODULE"": { ""X"": ""Y""
            },

            ""PATIENTMODULE"": {
                ""destination"": ""[PHI].[Patient]"",
                ""destintidentity"": 1,
                ""identity"": ""PATIENTID"",
                ""depends"": [ ""CACHEDISSUEROFPATIENTIDS"", ""FACILITIES"", ""MODALITIES"", ""RESOURCES"" ],
                ""key"": [ ""int"", ""INTERNALPATIENTID"", ""[InternalPatientId]"" ],
                ""existing"": ""select InternalPatientID from phi.Patient where InternalAssigningAuthorityID = (select InternalAssigningAuthorityID from [dbo].[AssigningAuthority] where Issuer= @ISSUEROFPATIENTID and PatientID = @PATIENTID"",
                ""fields"": [
                  [ ""ISSUEROFPATIENTID"", ""InternalAssigningAuthorityID"", ""[dbo].[AssigningAuthority]"", ""[AssigningAuthority]"", ""DEFAULT"" ],
                  [ ""PATIENTID"", ""[PatientID]"" ],
                  [ ""$DICOMNAME"", ""PatientName"" ],
                  [ ""PATIENTTIMESTAMP"", ""Timestamp"" ],
                  [ ""$GUID_GENERATE()"", ""PatientGUID"" ],
                  [ ""+ISSUEROFPATIENTID"", ""Issuer"" ],
                  [ ""COUNTRY"", ""COUNTRYCODE"" ],
                  [ ""HOMEPHONE"", ""PHONEHOME"" ],
                  [ ""BUSINESSPHONE"", ""PHONEBUSINESS"" ],
                  [ ""@LOOKUP"", ""LastUpdateUserID"", ""[dbo].[User]"" ],
                  [ 1, ""IsActive"" ],
                  [ ""EMAIL"", ""EmailAddress"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              }
            }";

            var mapdict = JObject.Parse(patientmodule1);
            var mapping = new ExportMapping("PATIENTMODULE", mapdict);


            // now we introduce the map to a reader, but to do that we need to mock the
            // data readers.
            var mockFbReader = new Mock<IDataReader>(); // mock IDataReader
            var mockSqlColumnInfo = new Mock<IColumnInfo>(); // mock IColumnInfo

            SetupMockReaderPatient(mockFbReader, mockSqlColumnInfo);

            // DONE MOCKING. Now we execute the method we are testing.

            mapping.MapFields(mockFbReader.Object, mockSqlColumnInfo.Object, null);
            //  CODE UNDER TEST:
            string sql1 = TestHelper.StripCommentsAndWhiteSpace(
                mapping.BuildStatement(StatementType.Insert));


            // Get primary result, which should NOT include [InternalPatientId]
            var sql2 =
                "INSERT INTO [PHI].[Patient] ( [Address], [PatientName], [PatientGUID], [LastUpdateUserID], [IsActive], [ExtJSON] ) OUTPUT INSERTED.InternalPatientId VALUES ( @Address, @PatientName, @PatientGUID, @LastUpdateUserID, @IsActive, @ExtJSON )";

            // bad:"INSERT INTO [PHI].[Patient] ( [Address], [PatientName], [PatientGUID], [IsActive], [ExtJSON] ) OUTPUT INSERTED.InternalPatientId VALUES ( @Address, @PatientName, @PatientGUID, @IsActive, @ExtJSON )";


            Assert.AreEqual(sql2, sql1);

            Assert.AreEqual(8, mapping.GetSevereErrorCount());

            Assert.AreEqual(0, mapping.GetMinorErrorCount());


            Assert.IsTrue(mapping.IsFaulted());
                // Our data reader didn't contain all the expected columns including, but not limited to the +ISSUEROFPATIENTID case.

            Assert.AreEqual(8, mapping.GetAllFieldsCount());
            Assert.AreEqual(6, mapping.GetSelectedFieldsCount());

            // Verify map field-level SELECTED FIELD content is correct.
            var field1 = mapping.GetSelectedField(0);
            Assert.AreEqual("nvarchar", field1.OutDbType);
            Assert.AreEqual("ADDRESS", field1.InField);
            Assert.AreEqual("Address", field1.OutField);
            Assert.AreEqual(1, field1.Index);
            Assert.IsTrue(field1.InsertOutField);
            Assert.IsFalse(field1.SpecialFunctionDetect(), "address is NOT a special function"); // !

            var field2 = mapping.GetSelectedField(1);
            Assert.AreEqual("internal", field2.OutDbType); // special functions like $DICOMNAME
            Assert.AreEqual("PatientName", field2.OutField);
            Assert.AreEqual(3, field2.Index);
            Assert.IsTrue(field2.InsertOutField);
            Assert.IsTrue(field2.SpecialFunctionDetect(), "$DICOMNAME is a special function");

            var field3 = mapping.GetSelectedField(2);
            Assert.AreEqual("$GUID_GENERATE()", field3.InField);

            var field4 = mapping.GetSelectedField(3);
            Assert.AreEqual("int", field4.OutDbType); // special functions like $DICOMNAME
            Assert.AreEqual("LastUpdateUserID", field4.OutField);
            Assert.AreEqual("DEREFERENCE", field4.InDbType); // ->@FIELD
            Assert.AreEqual(5, field4.Index);
            Assert.AreEqual("[dbo].[User]", field4.SelectTable);
            Assert.AreEqual("int", field4.OutDbType);
            Assert.IsTrue(field4.SpecialFunctionDetect(), "@LOOKUP syntax is considered a special function");
            Assert.IsTrue(field4.InsertOutField);


            var field5 = mapping.GetSelectedField(4);
            Assert.AreEqual("#1", field5.InField);

            var field6 = mapping.GetSelectedField(5);
            Assert.AreEqual("*", field6.InField);


            // verify it has had the expected interactions with the DAL:
            mockFbReader.Verify(r => r.GetFieldType(0), Times.Never);
            mockFbReader.Verify(r => r.GetFieldType(1), Times.Never);

            // These would change if we modified our algorithms or increase/decrease the number of items in the declaration.
            mockFbReader.Verify(r => r.GetName(0), Times.AtLeast(6)); // initial condition here was actually 8
            mockFbReader.Verify(r => r.GetName(1), Times.AtLeast(6)); // initial condition here was actually 6
        }

        // var mockSqlColumnInfo = new Mock<IColumnInfo>();  // mock IColumnInfo

        private static void SetupMockReaderPatient(Mock<IDataReader> mockFbReader, Mock<IColumnInfo> mockSqlColumnInfo)
        {
// firebird column info reader with a few fields,  FACILITYNAME, ISSUEROFPATIENTID

            mockFbReader.Setup(t => t.FieldCount).Returns(3);
            mockFbReader.Setup(t => t.GetName(0)).Returns("INTERNALPATIENTID");
            mockFbReader.Setup(t => t.GetFieldType(0)).Returns(typeof (int));
            mockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("int");

            mockFbReader.Setup(t => t.GetName(1)).Returns("ADDRESS");
            mockFbReader.Setup(t => t.GetFieldType(1)).Returns(typeof (string));
            mockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("varchar");

            mockFbReader.Setup(t => t.GetName(3)).Returns("LOOKUP");
            mockFbReader.Setup(t => t.GetFieldType(3)).Returns(typeof (string));
            mockFbReader.Setup(t => t.GetDataTypeName(3)).Returns("varchar");


            mockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(4);

            mockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("InternalPatientId"); // NOT in the same order as above!!
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("int");

            mockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("Address");
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("nvarchar");

            mockSqlColumnInfo.Setup(t => t.GetName(2)).Returns("LastUpdateUserID");
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("int");

            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            mockSqlColumnInfo.Setup(t => t.GetName(3)).Returns("ExtJSON"); // NOT in the same order as above!!
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(3)).Returns("nvarchar");
        }

        [TestMethod]
        [TestCategory("Mapping.Complex")]
        public void MappingMasterDetailSyntaxParse()
        {
            // If in firebird something is 1:1 and we want it to be 1:many in SQL, we have a ^
            // based syntax in the OUT field area.
            // ^[dbo].[AnotherTable].[AnotherInternalTableId]
            // ^ = marker
            // [dbo].[AnotherTable] - schema and table, MUST be in brackets for ^ syntax.
            // .[Field]  - the value to store in the other table.
            //
            var x = new MasterDetailFixture();

            x.Mapping.MapFields(x.MockFbReader.Object, x.MockSqlColumnInfo.Object, null);
            string sql = TestHelper.StripCommentsAndWhiteSpace(x.Mapping.BuildStatement(StatementType.Insert));

            Assert.AreEqual(1, x.Mapping.GetSelectedFieldsCount());

            var field1 = x.Mapping.GetAllField(0);
            Assert.AreEqual("nvarchar", field1.OutDbType);
            Assert.AreEqual("IN1", field1.InField);
            Assert.AreEqual("Out1", field1.OutField);
            Assert.AreEqual(0, field1.Index);
            Assert.IsTrue(field1.InsertOutField);
            Assert.IsFalse(field1.SpecialFunctionDetect(), "IN1->OUT1 is NOT a special function"); // !
            Assert.IsFalse(field1.SecondaryFlag);
            Assert.IsFalse(field1.MasterDetailFlag);


            var field3 = x.Mapping.GetAllField(2);
            Assert.AreEqual("MASTERDETAIL", field3.OutDbType);
            Assert.AreEqual("anotherinternaltableid", field3.InField);
            Assert.AreEqual("[dbo].[AnotherTable].[AnotherInternalTableId]", field3.OutField);
            Assert.AreEqual(2, field3.Index);
            Assert.IsFalse(field3.InsertOutField);
            Assert.IsTrue(field3.SecondaryFlag);
            Assert.IsTrue(field3.MasterDetailFlag);

            var sqlex = "INSERT INTO [dbo].[Table] ( [Out1] ) OUTPUT INSERTED.InternalTableId VALUES ( @Out1 )";
            Assert.AreEqual(sqlex, sql);
        }

        [TestMethod]
        [TestCategory("Mapping.Basic")]
        public void MappingStudyStatusIntKeyCopyCase()
        {
            // this case has a "key: []" specification.
            // That means "no identity translation is done, the key values from firebird are copied over verbatim into MS SQL".
            var x = new SimpleStudyStatusFixture();
            x.Mapping.MapFields(x.MockFbReader.Object, x.MockSqlColumnInfo.Object, null);

            Assert.AreEqual(4, x.Mapping.GetSelectedFieldsCount());

            var f1 = x.Mapping.GetSelectedField(0);
            Assert.AreEqual(f1.InField, "STATUSORDER");
            Assert.AreEqual(f1.OutField, "StatusValue");

            var f2 = x.Mapping.GetSelectedField(1);
            Assert.AreEqual(f2.InField, "STATUS");
            Assert.AreEqual(f2.OutField, "Status");

            var f3 = x.Mapping.GetSelectedField(2);
            Assert.AreEqual(f3.InField, "#0");
            Assert.AreEqual(f3.OutField, "Favorite"); // why is it inconsistent when we bracket?


            var f4 = x.Mapping.GetSelectedField(3);
            Assert.AreEqual(f4.InField, "*");
            Assert.AreEqual(f4.OutField, "ExtJSON");
        }

        [TestMethod]
        [TestCategory("Mapping.Existing")]
        public void MappingGroupExistingItemQueryGeneration()
        {
            var x = new GroupRoleFixture();
            x.Mapping.MapFields(x.MockFbReader.Object,x.MockSqlColumnInfo.Object,null);

            Assert.AreEqual(3, x.Mapping.GetSelectedFieldsCount());

            // if anything fundamental changed in parsing the mapping.json-type-content embedded in the fixture, detect that our entire test's basic assuptions are wrong or that the entire system is broken.
            var groupInsertSqlActual = TestHelper.StripCommentsAndWhiteSpace( x.Mapping.BuildStatement(StatementType.Insert) );
            var groupInsertSqlExpected = "INSERT INTO [dbo].[Group] ( [GroupName], [InternalRoleID], [ExtJSON] ) OUTPUT INSERTED.InternalGroupId VALUES ( @GroupName, @InternalRoleID, @ExtJSON )";
            Assert.AreEqual(groupInsertSqlExpected, groupInsertSqlActual, "[dbo].[Group] INSERT SQL is wrong");



            // Success case 1: Main Firebird table existing item query should return known value
            var groupSqlActual = x.Mapping.GetExistingItemQuery("GROUPLIST");
            var groupSqlExpected = "SELECT InternalGroupId FROM [dbo].[Group] WHERE [GroupName] = @GroupName ";
            Assert.AreEqual(groupSqlExpected,groupSqlActual);

            // Clean Failure handling case 1: A value which is defined in mapping.json but which is NOT the table we currently have mapped does not know how to generate the existing item query.
            // We don't crash but we do expect to return empty string, which means "I can't do that here".
            // This expected-failure occurs because  GetOutputFieldNameForInputFieldName only works for the fields that are currently mapped. In GroupRoleFixture that's GROUPLIST.
            var roleSqlActual = x.Mapping.GetExistingItemQuery("ROLELIST");

            Assert.AreEqual(string.Empty, roleSqlActual);


            //x.Mapping.GetFieldLookupFbIdentityMapSql() covered in other tests.
            // x.Mapping.GetKeyFieldInfo() covered in other tests.




        }

        // -- end tests.
    } //class MappingTests
} // namespace