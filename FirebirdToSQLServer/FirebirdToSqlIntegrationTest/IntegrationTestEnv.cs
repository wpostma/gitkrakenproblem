using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Util;

namespace RamSoft.FirebirdToSqlServer
{
    /// <summary>
    /// Integration Test Environment
    ///
    /// Note: Hard coded defaults for incoming firebird and outgoing SQL db
    /// and configuration file, are overridden by environment variables.
    ///
    /// PACSFDB -  incoming *.fdb file
    /// SQLDBSERVER - outgoing server such as (local)
    /// SQLDBNAME - outgoing db name on that outgoing server such as UNITTESTDB3
    /// MAPPINGJSON - mapping.json file name and path
    ///
    /// </summary>
    public class IntegrationTestEnv
    {
        protected volatile int InsertCount;
        private const int IntegrationTestOutputItemsCount = 300;

        public readonly List<LogEventArgs> LogItems = new List<LogEventArgs>();

        public readonly List<StatusEventArgs> OutputItems = new List<StatusEventArgs>();

        public readonly List<StatusEventArgs> StatusEventItems = new List<StatusEventArgs>();
        public Conversion Conversion;

        public TestDataAccessLayerFactory DataAccessLayerFactory;

        public void CountInserts(TestDataAccessLayer dal)
        {
            InsertCount++;


        }

        public void StatusEvent(object sender, StatusEventArgs args)
        {
            StatusEventItems.Add(args);
        }

        public void SampleOutputEvent(object sender, StatusEventArgs args)
        {
            OutputItems.Add(args);
        }

        public void WireEvents()
        {
            Conversion.StatusEvent += StatusEvent;
            Conversion.SampleOutputEvent += SampleOutputEvent;
        }

        protected void LogEventLocal(object context, LogEventArgs args)
        {
            // Output during DoConvert is captured for analysis.
            LogItems.Add(args);
        }

        public int DoTest(string tableName, int exactRowCount,
            int outputItemTotalCount = -1,
            bool fullConvertOfDependantTables = false)
        {
            Log.Write("-------------------------------------------------------------");
            Log.Write("INTEGRATION TEST FOR FIREBIRD TABLE " + tableName);

            Log.LogEvent += LogEventLocal;
            if (fullConvertOfDependantTables)
            {
                var tables = Conversion.GetDependenciesFor(tableName);
                foreach (string depTableName in tables)
                {
                    int depResult = Conversion.DoConvert(depTableName);
                    Log.Write(" >>>> " + depTableName + " Rows:" + depResult);
                }
            }



            int result = Conversion.DoConvert(tableName); // run the conversion.
            Log.LogEvent -= LogEventLocal;

            // Check that some data got written.
            Assert.AreEqual(exactRowCount, result);
            //"Expected " + tableName + " conversion to move " + exactRowCount.ToString());
            Log.Write(" >>>> " + tableName + " Rows:" + result);

            foreach (var item in OutputItems)
            {
                Log.Write("==================== SAMPLE " + tableName + " SQL==================== \n" + item.Message);
            }

            if (outputItemTotalCount > 0)
                Assert.AreEqual(outputItemTotalCount, OutputItems.Count );

            return result;
        }


        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public void QueryCheckCount(string query, int expectedQueryCountResult, string amessage)
        {
            // query should be in form select count(*) from X where Y or
            // select AFIELD from X where Y, if AFIELD is an integer field.
            using (var conn = DataAccessLayerFactory.GetMssqlConnection() as SqlConnection)
            {
                var cmd = new SqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                rdr.Read();
                var queryResult = rdr.GetFieldValue<int>(0);
                if (amessage == null)
                    amessage = "Select count(*) Result Incorrect";

                Assert.AreEqual(expectedQueryCountResult, queryResult, amessage +"\n"+ query);
            }
        }

        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public void QueryCheckStr(string query, string result)
        {
            // query should be in form select count(*) from X where Y or
            // select AFIELD from X where Y, if AFIELD is an integer field.
            using (var conn = DataAccessLayerFactory.GetMssqlConnection() as SqlConnection)
            {
                var cmd = new SqlCommand(query, conn);
                var rdr = cmd.ExecuteReader();
                rdr.Read();
                var queryResult = rdr.GetFieldValue<string>(0);
                Assert.AreEqual(result, queryResult, " query returned incorrect string value\n" + query);
            }
        }


        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public void QueryCheckHash(string query, string[] result=null)
        {
            int badDataCounter = 0;
            var invalidInfo = new StringBuilder();
            using (MD5 md5 = MD5.Create())
            {
                StringBuilder sbLine = new StringBuilder();
                int rowNumber = 0;
                // query should be in form select count(*) from X where Y or
                // select AFIELD from X where Y, if AFIELD is an integer field.
                using (var conn = DataAccessLayerFactory.GetMssqlConnection() as SqlConnection)
                {
                    var cmd = new SqlCommand(query, conn);
                    var rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        sbLine.Clear();
                        for (int i = 0; i < rdr.FieldCount; i++)
                        {

                            var queryResult = rdr.GetValue(i).ToString();// use something that doesn't throw, because rdr.GetFieldValue<string>(i) throws for various non-string types.
                            sbLine.Append(queryResult);
                            if (i < rdr.FieldCount - 1)
                            {
                                sbLine.Append(", ");
                            }
                            else
                            {
                                sbLine.Append("\n");
                            }
                        }
                        var s = sbLine.ToString();
                        //Log.Write(s);
                        byte[] inputBytes = Encoding.ASCII.GetBytes(s);
                        var hashBytes = md5.ComputeHash(inputBytes);
                        // Convert the byte array to hexadecimal string
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < hashBytes.Length; i++)
                        {
                            sb.Append(hashBytes[i].ToString("X2"));
                        }
                        var md5Str = sb.ToString();
                        // "AAAAAAAAAAAAAAAAAAAAAAAAAA",  // #0  <fields>
                        string out1 = "\"" + md5Str + "\", // #" + rowNumber + " " + s;
                        if (result != null)
                        {
                            bool ok =  md5Str == result[rowNumber];
                            if (ok)
                            {
                                Log.Write(out1 + " [OK] ");
                            }
                            else
                            {
                                Log.Write(out1 + " [BAD] ");
                                badDataCounter++;
                                invalidInfo.Append(out1);
                                invalidInfo.Append("[EXPECTED: ");
                                invalidInfo.Append(result[rowNumber]);
                                invalidInfo.Append(", ACTUAL: ");
                                invalidInfo.Append(md5Str);
                                invalidInfo.Append("]\n");


                            }

                        }
                        else
                            Log.Write( out1);


                        rowNumber++;
                    }
                }
            }
            Assert.IsTrue(badDataCounter==0,"Output data is wrong or no longer consistent with the test. Correct the test or the data after examining the expectedQueryCountResult. "+invalidInfo);
        }


        public static void Exec(string cmdText, SqlConnection conn)
        {
            var cmd = new SqlCommand(cmdText, conn);
            int result = cmd.ExecuteNonQuery();
            if (result != 0)
            {
                Log.Write(LogLevel.LWarning, "INFO: SQL Result=" + result);
            }
        }
        public static int ExecGetInt(string cmdText, SqlConnection conn, int defaultReturnValue)
        {
            var cmd = new SqlCommand(cmdText, conn);
            using (var reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    return reader.GetFieldValue<int>(0);
                }
                return defaultReturnValue;
            }
        }


        public static IntegrationTestEnv Factory(int maxRowCount)
        {
            try
            {
                return FactoryImpl(maxRowCount);
            }
            catch (SqlException ex) //when ( ex.ErrorCode == 0x80131904)
            {
                throw new IntegrationTestEnvironmentException("Check that SQL Server (local) instance is installed and running, and that UNITTESTDB5 is set up. Environment for Integration Tests is not set up properly.", ex);

            }

        }
        private static IntegrationTestEnv FactoryImpl(int maxRowCount)
        {
            if (maxRowCount < 0)
            {
                maxRowCount = 10000;
            }

            var env = new IntegrationTestEnv();


            var pacsfdb = Path.GetFullPath("..\\..\\DB\\INTEGRATIONTESTDATA.fdb");

            var pacsfdbenv = Environment.GetEnvironmentVariable("PACSFDB");
            if (!string.IsNullOrEmpty(pacsfdbenv))
            {
                pacsfdb = pacsfdbenv;
            }

            if (!File.Exists(pacsfdb))
            {
                Log.Write("WARNING: Missing or Invalid Firebird DB value ignored:" + pacsfdb);
                pacsfdb = "C:\\Program Files\\ramsoft\\DB\\pacs.fdb";
            }

            Log.Write("Firebird DB:" + pacsfdb);
            var fbConnStr =
                $"initial catalog=\"{pacsfdb}\";user id=SYSDBA;password=$db@dM1n;data source=localhost";

            // UNITTESTDB4 : has [dbo].[Issuer], state prior to assigning authority replacing issuer.
            // UNITTESTDB5 : assigning authority change.
            var db = "UNITTESTDB5";

            var ds = "(local)";

            var dsenv = Environment.GetEnvironmentVariable("SQLDBSERVER");
            if (!string.IsNullOrEmpty(dsenv))
                ds = dsenv;
            var dbenv = Environment.GetEnvironmentVariable("SQLDBNAME");
            if (!string.IsNullOrEmpty(dbenv))
                db = dbenv;

            // TODO: This is kind of ugly to hard-code relationship of source folders to integration test folders.
            string jsonfile = "..\\..\\..\\FirebirdToSqlServerConversion\\Mapping.json";
            var jsonfileenv = Environment.GetEnvironmentVariable("MAPPINGJSON");
            if (!string.IsNullOrEmpty(jsonfileenv))
                jsonfile = jsonfileenv;



            Log.Write("MSSQL DB:" + db+" on "+ds);

            var msSqlConnStr = $"Data Source={ds};Initial Catalog={db};Integrated Security=True";

            var lookups = new Lookups();

            // Absolute path is
            // C:\x\FirebirdToSQLServer\FirebirdToSQLServer\Conversion\Mapping.json
            // This test runs from
            // C:\x\FirebirdToSQLServer\FirebirdToSQLServer\FirebirdToSqlIntegrationTest\bin\Debug
            // So that's up three folders, and then down one folder.
            var dalf = new TestDataAccessLayerFactory(fbConnStr, msSqlConnStr);
            dalf.IntegrationTestEnv = env;


            var conv = new Conversion(
                jsonfile,
                dalf,
                lookups,
                maxRowCount);

            conv.SampleOutputCounter = IntegrationTestOutputItemsCount;

            env.Conversion = conv;
            env.DataAccessLayerFactory = dalf;

            env.WireEvents();


            /* Blank the destination tables */
            using (var conn = dalf.GetMssqlConnection() as SqlConnection)
            {
                // The order here is important. We delete higher level tables first, because they have FK constraints that prevent
                // other tables from being cleared. Note use of new ${db} interpolation in C# 6 and up.
                var acount = ExecGetInt( $@"SELECT  COUNT(DISTINCT(TABLE_NAME) )
                                            FROM {db}.INFORMATION_SCHEMA.COLUMNS
                                            WHERE
                                             TABLE_NAME = 'AssigningAuthority'
                                             OR TABLE_NAME = 'Patient'
                                             OR TABLE_NAME = 'Study' ",
                    conn, -1);
                Assert.AreEqual(acount,3,"expected that integration test database "+db+" contains Assigning Authority, Patient, and Study tables");




                Exec("DELETE FROM [code].[PhysicianSpecialty]", conn);

                Exec("DELETE FROM [dbo].[ReservedTime]", conn);

                Exec("DELETE FROM [dbo].[Resource]", conn);
                Exec("DELETE FROM [dbo].[Department]", conn);
                Exec("DELETE FROM [dbo].[Room]", conn);

                Exec("DELETE FROM [dbo].[StudyProcedureMap]",conn);
                Exec("DELETE FROM [phi].[Study]", conn);


                Exec("DELETE FROM [phi].[Patient]", conn);


                Exec("DELETE FROM [dbo].[Group]", conn); // created automatically when you create a user.

                Exec("DELETE FROM [dbo].[Role]", conn); // created automatically when you create a user.

                Exec("DELETE FROM [dbo].[Facility]", conn);

                Exec("DELETE from [dbo].[AssigningAuthority]", conn);

                Exec("DELETE FROM [code].[Status]", conn);

                Exec("DELETE FROM [code].[Modality]", conn);

                Exec("DELETE FROM [code].[ProcedureCode]", conn);



                //Exec("DELETE FROM [tzdb].[Country]", conn);  // We don't actually clean-migrate countries, that's an ISO table, it's not the customer's data.
            }


            return env;
        }
    }
}