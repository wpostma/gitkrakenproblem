﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;

namespace FirebirdToSQLServerTests.Fixture
{
    // subclass from real class and fake bits of it out, that's a SemiFake.
    public class SemiFakeExportWriter : ExportWriter
    {
        public SemiFakeExportWriter(IExportWriterOwner owner,
            IDataAccessLayer dataAccessLayer,
            IMapping mapping,
            int maxRowCount,
            // Optional special case fields.
            SingleDefaultValue single = null
            ) : base(owner, dataAccessLayer, mapping, maxRowCount, single)
        {
        }
    }


    // almost entirely faked, name it fake.  A fake is a different concept than a mock.

    public class FakeDataAccessLayerFactory : IDataAccessLayerFactory
    {
        public DbConnection GetFirebirdConnection()
        {
            throw new NotImplementedException();
        }

        public DbConnection GetMssqlConnection()
        {
            throw new NotImplementedException();
        }

        public IDataAccessLayer CreateDataAccessLayer()
        {
            return new FakeDataAccessLayer();
        }

        public void FinalizeDataAccessLayer(IDataAccessLayer dal)
        {
            (dal as IDisposable)?.Dispose();
        }

        public ExportWriter CreateExportWriter(IExportWriterOwner owner, IDataAccessLayer dal,
            IMapping mapping, int maxRowCount, SingleDefaultValue single = null)
        {
            return new SemiFakeExportWriter(owner, dal, mapping, maxRowCount, single);
        }

        public DbDataReader ExecuteQuery(DbConnection connection, DbTransaction transaction, string querySql)
        {
            throw new NotImplementedException();
        }

        public int ExecuteCommand(DbConnection connection, DbTransaction transaction, string commandSql)
        {
            throw new NotImplementedException();
        }
    }

    public class FakeDataAccessLayer : IDataAccessLayer
    {
        public string InsertSql { get; set; } = string.Empty;
        public static int fakeIdentity = 0;


        // Fake should be able to tell me how many times certain methods were called:
        public int CallSetupBeforeExecuteCount { get; set; }

        public int CallCommitCount { get; set; }


        // public int FirebirdReaderFieldCount { get; set; }

        // Data Access Layer Requires Some Plumbing, like Firebird and SQL Data readers.
        public Mock<IDataReader> MockFbReader = new Mock<IDataReader>(); // supports IDataReader. May want to write a helper to populate the fields info.

        public ColumnInfo FakeColumnInfo = new ColumnInfo(); // MSSQL column info.  We use the real model class here, but the data inside is "Fake".

        //public Mock<IColumnInfo> MockSqlColumnInfo = new Mock<IColumnInfo>(); // this information is critical.

        public int ReadCounter = 10;

        //public bool ReturnSetupBeforeMapping { get; set; }
        public bool FailSetupBeforeMapping { get; set; }

        public FakeDataAccessLayer()
        {

        }

        public bool Commit(int statementIndex)
        {
            CallCommitCount++;
            return false;
        }

        public bool SetupFieldSelectCommandQuery(IMapping mapping, DataReaderQueryType aType,
            IField field, object incomingValue)
        {
            throw new NotImplementedException();
        }

        public void SetupBeforeMapping(IMapping mapping, SingleDefaultValue single)
        {
            if (FailSetupBeforeMapping)
                throw new NotImplementedException();
            // return ReturnSetupBeforeMapping;
        }

        public int SetupBeforeWriteRowWithDefaults(IMapping mapping)
        {
            throw new NotImplementedException();
        }

        public int SetupBeforeExecute(int statementIndex, IMapping mapping)
        {
           // throw new NotImplementedException();
            CallSetupBeforeExecuteCount++;
            return 0;
        }

        public int SetupInsertSql(string insertSql)
        {
            //throw new NotImplementedException();
            if (InsertSql == string.Empty)
            {
                InsertSql = insertSql;
                return 0;
            }
            else
                throw new NotImplementedException();
        }

        public object FirebirdReaderGetValue(int indexIncoming)
        {
            throw new NotImplementedException();
        }

        public string FirebirdReaderGetColumnType(int indexIncoming)
        {
            throw new NotImplementedException();
        }

        public string FirebirdReaderGetValueByName(string fieldname)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, object> FirebirdReaderJsonSerializeRowFull()
        {
            throw new NotImplementedException();
        }

        public bool FirebirdRead()
        {
            if (ReadCounter <= 0)
            {
                return false;
            }
            ReadCounter--;
            return ReadCounter > 0;
        }


        public IDataReader FirebirdColumnReader()
        {
            return MockFbReader.Object;
        }


        public int FirebirdReaderGetFieldCount()
        {
            return FakeColumnInfo.GetFieldCount();
        }

        public string FirebirdLookupEntityName(IMapping mapping, IField field,
            object incomingValue)
        {
            if (incomingValue == null)
            {
                return "FAKE.NULL";
            }
            return "FAKE." + incomingValue;
        }

        public string GetLastInsertSql(int statementIndex)
        {
            return "FAKE";
        }

        public string GetFailedSql()
        {
            return "FAKE";
        }

        public string GetParamValueAsStr(IDbCommand dbCommand, int index)
        {
            // throw new NotImplementedException();
            return "FAKE";
        }

        public bool InsertSqlParamValue(int statementIndex, int paramIndex, object value)
        {
            if (statementIndex != 0)
                throw new NotImplementedException("InsertSqlParamValue only implemented for statementIndex 0");

            bool valueNotNull = (value != null) && (value != DBNull.Value);
            // throw new NotImplementedException();
            return valueNotNull;
            // sometimes when returning false the caller may want to dump a warning, especially if the caller knows that the field was required not to be null.
        }

        public object InsertSqlExecute(int statementIndex)
        {
            if (statementIndex != 0)
                throw new NotImplementedException("InsertSqlExecute only implemented for statementIndex 0");

            // FAKE IT
            fakeIdentity++;
            return fakeIdentity; // wrap it
        }

        public bool IdentityAlreadyExists(IMapping mapping)
        {
            return false;
        }

        public object GetLastIdentity()
        {
            return null;
        }

        public string GetInsertSqlParamTypeStr(int statementIndex, int paramIndex)
        {
            return "FAKE";
        }

        public bool GetInsertSqlParamValueIsNull(int statementIndex, int paramIndex)
        {
            return true;
        }

        public int GetInsertSqlParamCount(int statementIndex)
        {
            return 0;
        }

        public string GetInsertSqlParamDebugStr(int statementIndex, int paramIndex)
        {
            return "FAKE";
        }

        public int GetInsertSqlParamValueNullCount(int statementIndex)
        {
            return 0;
        }

        public void SetupBeforeSubFieldInsert(int statementIndex, IField field)
        {
            throw new NotImplementedException();
            // sets up insert for statementIndex of 1,2,3, but not for statementIndex 0, if we were implementing it.
        }

        public bool HandleExceptionLogging(int statementIndex, IMapping mapping, Exception ex, IDbCommand command)
        {
            var shouldRaise = false;

            //throw new NotImplementedException();
            return shouldRaise;
        }

        public IColumnInfo GetSqlColumnInfo(string tableName = null)
        {
            if (tableName == null)
                return FakeColumnInfo;
            else
                throw new NotImplementedException();


        }

        public IColumnInfo SqlColumnReaderExecute()
        {
            return FakeColumnInfo; //MockSqlColumnInfo.Object;
        }
    }
}
