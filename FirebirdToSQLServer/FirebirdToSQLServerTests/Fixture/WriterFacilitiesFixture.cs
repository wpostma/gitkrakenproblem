using Moq;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;
using RamSoft.FirebirdToSqlServer.Util;

namespace FirebirdToSQLServerTests.Fixture
{
    internal class WriterFacilitiesFixture : SimpleFacilitiesFixture, IExportWriterOwner
    {
        public int ContextRow;
        public int Identity = 1000;
        public int IdentityCounter;
        public Mock<IDataAccessLayer> MockDal;
        public int SimulateFirebirdReadActive = 10; // read success countdown during writer.execute main loop.
        public ExportWriter Writer;

        // test fixture that counts down, and returns false after it is called a certain number of times:


        public WriterFacilitiesFixture(int maxrow = 100, SingleDefaultValue single = null)
        {
            MockDalSetup();

            Writer = new ExportWriter(this,
                MockDal.Object,
                Mapping,
                maxrow,
                single
                );
        }

        public object InsertRowAndFetchIdentity(string firebirdTableName, object incomingValue, IField field)
        {
            // Debug.Assert(object);
            Identity++;
            return Identity;
        }


        // Validation Checkpoint (STUB) methods. If something is bad, throw an exception or log something.

        void IExportWriterOwner.ChecksBeforeExecuteOneRow(int paramCount)
            // validate that parameter count and firebird field count are acceptable.
        {
            if (ContextRow < 10)
            {
                Log.Write(LogLevel.LDebug, "ExecuteOneRow paramCount=" + paramCount + " _contextRow=" + ContextRow);
            }
        }

        public void SetContextRow(int rowNumber)
        {
            ContextRow = rowNumber;
        }

        public void ChecksAfterParameterSetValue(object context, CopyType type, int paramIndex, IField field,
            int indexIncoming, object value)
        {
            if (ContextRow < 10)
            {
                Log.Write("Set Param Value " + field.OutField + " = " + StrHelpers.NameOfObjectStr(value));
            }
        }

        public void ChecksAfterInsertIdentity(object context, IMapping mapping, IDataAccessLayer dal, object newIdentity)
        {
            IdentityCounter++;
            if (IdentityCounter < 10)
            {
                Log.Write("INSERT IDENTITY " + mapping.GetMicrosoftSqlTableName() + " : " +
                          StrHelpers.NameOfObjectStr(newIdentity));
            }
        }

        public CheckBeforeInsertAction ChecksBeforeInsertIdentity(object context, IMapping mapping, IDataAccessLayer dal)
        {
            // todo.
            Log.Write(LogLevel.LInfo, "BEFORE INSERT IDENTITY " + mapping.FirebirdTableName);


            return CheckBeforeInsertAction.Continue;
        }

        private void MockDalSetup()
        {
            MockDal = new Mock<IDataAccessLayer>();
            // TODO:setup mock DAL enough to actually get data and not just firebird and MS SQL column names and types
            MockDal.Setup(t => t.FirebirdColumnReader()).Returns(MockFbReader.Object);
            MockDal.Setup(t => t.GetSqlColumnInfo(null)).Returns(MockSqlColumnInfo.Object);

            // First two calls to FirebirdRead should return true, then false.
            MockDal.SetupSequence(t => t.FirebirdRead())
                .Returns(true)
                .Returns(true)
                .Returns(false);

            MockDal.SetupSequence(t => t.InsertSqlExecute(0))
                .Returns(1001)
                .Returns(1002)
                .Returns(1003)
                .Returns(1004);

            // just return hard coded parameter count of 3.
            MockDal.SetupSequence(t => t.SetupBeforeWriteRowWithDefaults(It.IsAny<ExportMapping>())).
                Returns(3);


            MockDal.Setup(t => t.SetupInsertSql(It.IsAny<string>())).
                Returns(0);

            /*     mockDal.Setup(t => t.SetupInsertSql(It.IsAny<string>())).
                      Throws(
                         new WrongSqlTestException("SetupInsertSql call")
                        );
                        */
        }
    }
}