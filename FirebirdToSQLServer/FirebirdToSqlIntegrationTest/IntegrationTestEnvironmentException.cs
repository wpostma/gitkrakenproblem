using System;

namespace RamSoft.FirebirdToSqlServer
{
    [Serializable]
    public class IntegrationTestEnvironmentException : ApplicationException
    {
        public IntegrationTestEnvironmentException() { }
        public IntegrationTestEnvironmentException(string message,
            Exception innerException)
            : base(message, innerException) { }
    }
}