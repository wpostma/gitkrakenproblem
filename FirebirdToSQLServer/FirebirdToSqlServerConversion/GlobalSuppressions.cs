[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#RamSoft.FirebirdToSqlServer.IDataAccessLayer.SetupInsertSql(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#DoSetupFieldSelectCommandQuery(RamSoft.FirebirdToSqlServer.ExportMapping,RamSoft.FirebirdToSqlServer.DataReaderQueryType,RamSoft.FirebirdToSqlServer.ExportMappedField,System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#DoSetupBeforeMapping(RamSoft.FirebirdToSqlServer.ExportMapping,RamSoft.FirebirdToSqlServer.SingleDefaultValue)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#RamSoft.FirebirdToSqlServer.IDataAccessLayer.FirebirdLookupEntityName(RamSoft.FirebirdToSqlServer.ExportMapping,RamSoft.FirebirdToSqlServer.ExportMappedField,System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayerFactory.#ExecuteQuery(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayerFactory.#ExecuteCommand(System.Data.Common.DbConnection,System.Data.Common.DbTransaction,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.ExportWriter.#SetupMapping(RamSoft.FirebirdToSqlServer.ExportMapping,RamSoft.FirebirdToSqlServer.SingleDefaultValue)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.JsonHelpers.#LoadJson(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#SetupInsertSql(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#FirebirdLookupEntityName(RamSoft.FirebirdToSqlServer.ExportMapping,RamSoft.FirebirdToSqlServer.ExportMappedField,System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "_fbCommand", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#Dispose(System.Boolean)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "_sqlColumns", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#Dispose(System.Boolean)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "_sqlCommandIns", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayer.#Dispose(System.Boolean)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "_fbConnection", Scope = "member", Target = "RamSoft.FirebirdToSqlServer.DataAccessLayerFactory.#Dispose(System.Boolean)")]
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

