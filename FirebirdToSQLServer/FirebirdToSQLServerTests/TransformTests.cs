﻿using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json.Linq;
using Ramsoft.TestFixtures;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Util;
using StatementType = RamSoft.FirebirdToSqlServer.Interface.StatementType;
// unit under test: FirebirdToSqlMap, in a more complex two-level case,
//   such as groups and roles.

namespace RamSoft.FirebirdToSqlServer
{
    internal class GroupListAndRoleListLookupCase
    {
        public const string GroupListAndRoleList1 =
            @"{

                      ""ROLELIST"": {
                        ""destination"": ""[dbo].[Role]"",
                        ""identity"": ""ROLENAME"",
                        ""key"": [ ""int"",""ROLEID"", ""[InternalRoleId]"" ],
                        ""fields"": [
                          [ ""ROLENAME"", ""[NAME]"" ],
                          [ ""*"", ""[ExtJSON]""]

                        ]
                      },
                      ""GROUPLIST"": {
                        ""destination"": ""[dbo].[Group]"",
                        ""identity"": ""ROLEID"",
                        ""key"": [ ""int"", ""GROUPID"", ""InternalGroupId"" ],
                        ""depends"": [ ""ROLELIST"" ],
                        ""fields"": [
                          [ ""ROLEID"", ""[InternalRoleID]"", ""[dbo].[Role]"", ""[Name]"", ""SYSTEM"", ""int"" ],
                          [ ""GROUPNAME"", ""[GroupName]"" ],
                          [ ""*"", ""[ExtJSON]"" ]

                        ]
                      },


                    }";

        // When parsing the JSON above, the ROLEID lookup should be detected as a foreign key
        // since the second value [InternalRoleID] == the key[2] field in the "ROLELIST" dictionary.


        public JObject Mapdict;
        public IMapping MappingGroup;
        public IMapping MappingRole;
        public Mock<IDataReader> MockFbReader; // mock IDataReader
        public Mock<IColumnInfo> MockSqlColumnInfo; // mock IColumnInfo


        public GroupListAndRoleListLookupCase()
        {
            Mapdict = JObject.Parse(GroupListAndRoleList1);
            MappingGroup = new ExportMapping("GROUPLIST", Mapdict);
            MappingRole = new ExportMapping("ROLELIST", Mapdict);

            // now we introduce the map to a reader, but to do that we need to mock the
            // data readers.
            MockFbReader = new Mock<IDataReader>(); // supports IDataReader
            MockSqlColumnInfo = new Mock<IColumnInfo>(); // supports IDataReader

            // firebird column info reader with a few fields, in the GROUPLIST firebird table.
            MockFbReader.Setup(t => t.FieldCount).Returns(3);
            MockFbReader.Setup(t => t.GetName(0)).Returns("GROUPID");
            MockFbReader.Setup(t => t.GetName(1)).Returns("ROLEID");
            MockFbReader.Setup(t => t.GetName(2)).Returns("GROUPNAME");

            MockFbReader.Setup(t => t.GetFieldType(0)).Returns(typeof (int));
            MockFbReader.Setup(t => t.GetFieldType(1)).Returns(typeof (int));
            MockFbReader.Setup(t => t.GetFieldType(2)).Returns(typeof (string));

            MockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("int");
            MockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("int");
            MockFbReader.Setup(t => t.GetDataTypeName(2)).Returns("varchar");


            MockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(4);


            MockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("InternalRoleId"); // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("int");

            MockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("InternalGroupId"); // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("int");

            MockSqlColumnInfo.Setup(t => t.GetName(2)).Returns("GroupName");
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("nvarchar");

            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            MockSqlColumnInfo.Setup(t => t.GetName(3)).Returns("ExtJSON"); // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(3)).Returns("nvarchar");
        }
    }

    [TestClass]
    public class TransformTests
    {
        [TestMethod]
        [TestCategory("Transform")]
        public void TransformTwoLevelStructureOfGroupsWithinRoles()
        {
            // We plan to start from this point and go in a lot of different directions
            // so the repeated code is moved into this case fixture.
            var x = new GroupListAndRoleListLookupCase();


            // x.mapping = map object under test, set up with mocks for the IDataReaders it needs
            // to be able to build an SQL Server insert statement.
            x.MappingGroup.MapFields(x.MockFbReader.Object, x.MockSqlColumnInfo.Object, null);
            string sql1 = TestHelper.StripCommentsAndWhiteSpace(
                x.MappingGroup.BuildStatement(StatementType.Insert));
            var sql2 =
                "INSERT INTO [dbo].[Group] ( [InternalRoleId], [GroupName], [ExtJSON] ) OUTPUT INSERTED.InternalGroupId VALUES ( @InternalRoleId, @GroupName, @ExtJSON )";

            int difference = StrHelpers.DiffText(sql2, sql1);
                // make it easier to spot differences by giving position of first difference.
            string differenceStr;
            if (difference == -1)
                differenceStr = "not found";
            else
                differenceStr = sql1.Substring(difference, 10);
            Assert.IsTrue(difference < 0,
                "Expected " + sql1 + " got " + sql2 + " - Difference at Position " + difference + " : " + differenceStr);

            Assert.AreEqual(4, x.MappingGroup.GetAllFieldsCount());
            Assert.AreEqual(3, x.MappingGroup.GetSelectedFieldsCount());

            // The Group table contains an FK link to the Role that it is IN.
            // We MUST have field foreignIdentity flag set here, or our lookup logic WILL FAIL at least when
            // ever we must do a translation from the identity type (integer?) of the other table to the display lookup value (NAME string?).
            var field1 = x.MappingGroup.GetSelectedField(0);
            Assert.AreEqual("int", field1.OutDbType.ToLower());
            Assert.AreEqual("ROLEID", field1.InField);
            Assert.AreEqual("INTERNALROLEID", field1.OutField.ToUpper());
            Assert.AreEqual(1, field1.Index, "first field expected to have index 1");
            Assert.AreEqual(1, field1.QueryIndex);
            Assert.IsTrue(field1.InsertOutField, "field should be in output");
            Assert.IsTrue(field1.ForeignIdentity,
                "field should be foreign identity, lookup feature won't work because of this problem");
            Assert.IsFalse(field1.Identity, "field should not be an identity");


            var field2 = x.MappingGroup.GetSelectedField(1);
            Assert.AreEqual("nvarchar", field2.OutDbType.ToLower());
            Assert.AreEqual("GROUPNAME", field2.InField.ToUpper());
            Assert.AreEqual("GROUPNAME", field2.OutField.ToUpper());
            Assert.AreEqual(2, field2.Index, "second field expected to have index 2");
            Assert.IsTrue(field2.InsertOutField);
            Assert.IsFalse(field2.Identity);
            Assert.IsFalse(field2.PrimaryKeyFlag);
            Assert.IsTrue(field2.InsertOutField);
        }
    }
}