using System;

namespace RamSoft.FirebirdToSqlServer.Util
{
    [Serializable()]
    public class StringHelperFailure : ApplicationException
    {
        public StringHelperFailure() { }
        public StringHelperFailure(string message) : base(message) { }
    }
}