﻿namespace FirebirdToSQLServerTests.Fixture
{
    internal class GroupRoleFixture : BaseFixture
    {
        // This string defines a three level structure, USERLIST requires GROUPLIST entries, which requires ROLELIST entries.
        // This base fixture centers on verifying GROUPLIST behaviors.
        public const string RoleGroupUser1 =
           @"{
              ""ROLELIST"": {
                ""destination"": ""[dbo].[Role]"",
                ""identity"": ""ROLENAME"",
                ""key"": [ ""int"", ""ROLEID"", ""[InternalRoleId]"" ],
                ""fields"": [
                  [ ""ROLENAME"", ""[NAME]"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              },

              ""GROUPLIST"": {

                ""destination"": ""[dbo].[Group]"",
                ""identity"": ""GROUPNAME"",
                ""key"": [ ""int"", ""GROUPID"", ""InternalGroupId"" ],
                ""depends"": [ ""ROLELIST"" ],
                ""fields"": [
                  [ ""GROUPNAME"", ""[GroupName]"" ],
                  [ ""ROLEID"", ""[InternalRoleID]"", ""[dbo].[Role]"", ""[Name]"", ""SYSTEM"", ""int"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              },

              ""USERLIST"": {
                ""destination"": ""[dbo].[User]"",
                ""identity"": ""USERNAME"",
                ""key"": [ ""int"", ""USERID"", ""[InternalUserID]"" ],
                ""depends"": [ ""EMPLOYER"", ""FACILITIES"", ""GROUPLIST"", ""PHYSICIANSPECIALTYTYPE"", ""ROLELIST"" ],
                ""fields"": [
                  [ ""USERNAME"", ""[UserName]"" ],
                  [ 0, ""[IsActive]"" ],
                  [ ""GROUPID"", ""[InternalGroupID]"", ""[dbo].[Group]"", ""[GroupName]"", ""SYSTEM"", ""int"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              }
        }";

        // fields: [
        //       [ InField, OutField, SelectTable, SelectField, SelectDefaultValue, SelectFieldType],
        // ]


        // a procedurally generated lookup string that lets us find the facility group in firebird by PK
        public const string FirebirdGroupIdentityLookupSql1 =
            "SELECT FIRST 1 GROUPNAME FROM GROUPLIST WHERE GROUPID = @VALUE";


        public GroupRoleFixture() : base(RoleGroupUser1, "GROUPLIST")
        {
            // firebird column info reader with a few fields,  FACILITYNAME, ISSUEROFPATIENTID
            MockFbReader.Setup(t => t.FieldCount).Returns(5);

            MockFbReader.Setup(t => t.GetName(0)).Returns("GROUPNAME");
            MockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("varchar");

            MockFbReader.Setup(t => t.GetName(1)).Returns("ROLEID");
            MockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("int");

            MockFbReader.Setup(t => t.GetName(2)).Returns("ACCESSTYPE");
            MockFbReader.Setup(t => t.GetDataTypeName(2)).Returns("int");

            MockFbReader.Setup(t => t.GetName(3)).Returns("STATUSSTART");
            MockFbReader.Setup(t => t.GetDataTypeName(3)).Returns("int");

            MockFbReader.Setup(t => t.GetName(4)).Returns("ACCESSALLFACILITIES");
            MockFbReader.Setup(t => t.GetDataTypeName(4)).Returns("T_YESNO");

            // ACCESSALLISSUERS : T_YESNO
            // ACCESSALLPRIORSTUDIES : T_YESNO


            MockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(3);

            MockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("InternalGroupID");
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("bigint");

            MockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("GroupName");
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("nvarchar");

            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            MockSqlColumnInfo.Setup(t => t.GetName(2)).Returns("ExtJSON");
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("nvarchar");
        }
    }
}