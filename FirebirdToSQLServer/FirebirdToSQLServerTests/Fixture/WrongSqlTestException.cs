using System;

namespace FirebirdToSQLServerTests.Fixture
{
    [Serializable]
    internal class WrongSqlTestException : ApplicationException
    {
        public WrongSqlTestException(string message) : base(message)
        {
        }
    }
}