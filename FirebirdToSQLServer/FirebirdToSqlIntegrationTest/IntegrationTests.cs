﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RamSoft.FirebirdToSqlServer.Util;

// Integration tests actually connect to a real firebird DB and a real MS SQL db.
//  A local SQL Server instance with a UNITTESTDB5 is required.

/*

    <connectionStrings>
    <add name="FirebirdToSqlServerTools.Properties.Settings.mssql" connectionString="Data Source=wpostma;Initial Catalog=PACS;Integrated Security=True"
        providerName="System.Data.SqlClient" />
    <add name="FirebirdToSqlServerTools.Properties.Settings.firebird" connectionString="initial catalog=d:\BigDb\pacs.fdb;user id=SYSDBA;password=$db@dM1n;data source=localhost"
        providerName="FirebirdSql.Data.FirebirdClient" />
    <add name="FirebirdToSqlServerTools.Properties.Settings.pacsdb" connectionString="initial catalog=&quot;C:\Program Files\ramsoft\DB\pacs.fdb&quot;;user id=SYSDBA;password=$db@dM1n;data source=localhost"
        providerName="FirebirdSql.Data.FirebirdClient" />
</connectionStrings>

*/

namespace RamSoft.FirebirdToSqlServer
{
    // Test helper class.


    [TestClass]
    public class IntegrationTests
    {
        /* EventHandlers (callbacks) */


        //protected static void LogCallback(LogLevel level, string message)
        protected static void LogEventGlobal(object context, LogEventArgs args)
        {
            // Visual Studio Output window
            string line = Log.GetPrefix(args.LogLogLevel) + ':' + args.Message;
            Trace.WriteLine(line);
            Console.WriteLine(line);
        }

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            Trace.WriteLine("Integration Test: " + context.TestName);

            //Log.LogCallback = LogCallback;
            Log.LogEvent += LogEventGlobal;
        }

        /* Exec("delete from [foo]", anSqlConnection);*/


        /* helper for each test case */

        protected IntegrationTestEnv SetupForConversion(int maxRowCount=-1)
        {
            return IntegrationTestEnv.Factory(maxRowCount);
        }


        // One table per unit test, so that the failures have a useful unit test name in the failure.
        // Each unit test might grow a specific set of data content checks too.
        /*
        [TestMethod]
        public void XferCountries()
        {
            // Setup Item under test.
            var env =  setupForConversion();

            env.DoTest("COUNTRYLIST"); // done at the same test as Patient.

        }
        */

        [TestMethod]
        [TestCategory("Integration.Complex")]
        public void XferStatus()
        {
            // Setup Item under test.
            var env = SetupForConversion();

            env.DoTest("STUDYSTATUSVALUES", 22);
        }


        [TestMethod]
        [TestCategory("Integration.Simple")]
        public void XferAssigningAuthority()

        {
            // Setup Item under test.
            var env = SetupForConversion();

            env.DoTest("CACHEDISSUEROFPATIENTIDS", 9);
        }

        [TestMethod]
        [TestCategory("Integration.Simple")]
        public void XferFacilities()
        {
            // Setup Item under test.
            var env = SetupForConversion();

            /*
            expected warnings:
                [WARN]:FACILITIES: MS SQL field [dbo].[Facility].LastupdateUTC not mapped to an incoming field.
                [WARN]:FACILITIES: MS SQL field [dbo].[Facility].ZoneID not mapped to an incoming field.
                [WARN]:FACILITIES: 2 fields not mapped to MS SQL table [dbo].[Facility]


            */

            env.DoTest("FACILITIES", 9, 11);

            // Check that data is correct, not only in top level facility but also assigning authorities.

            env.QueryCheckCount("SELECT COUNT(*) FROM [dbo].[Facility]", 9, "Wrong Count of dbo.Facility");

            env.QueryCheckCount("SELECT COUNT(*) FROM [dbo].[AssigningAuthority]", 3, "Wrong Count of [dbo].[AssigningAuthority] "); // Issuer objects in firebird become AssigningAuthority in MSSQL.

            env.QueryCheckCount("SELECT COUNT(*) FROM [dbo].[AssigningAuthority] where Name = 'DEFAULT'", 1,
                "Require EXACTLY ONE Default Assigning Authority after moving Facility");

            env.QueryCheckCount("SELECT COUNT(*) FROM [dbo].[AssigningAuthority] where Name = 'RAMSOFT'", 1,
                "Require EXACTLY ONE Ramsoft Assigning Authority after moving Facility");

            env.QueryCheckCount("SELECT COUNT(*) FROM [dbo].[AssigningAuthority] where Name = 'PETER'", 1,
                "Require EXACTLY ONE Peter Assigning Authority after moving Facility");
        }


        [TestMethod]
        [TestCategory("Integration.Simple")]
        public void XferRoles()
        {
            // Setup Item under test.
            var env = SetupForConversion();
            env.DoTest("ROLELIST", 15);
        }


        [TestMethod]
        [TestCategory("Integration.Simple")]
        public void XferGroups()
        {
            // Setup Item under test.
            var env = SetupForConversion();

            // To insert into [dbo].[GROUP] and satisfy constraints:
            // We must copy or create a dependant ROLE item and store it into ROLE
            // After that, we can insert the root GROUP item.


            env.DoTest("GROUPLIST", 4, 8); // --> [dbo].[Grouo]

            env.QueryCheckStr("SELECT [ExtJSON] from [dbo].[GROUP] where [GroupName] = 'SYSTEM'",
                @"{""GROUPID"":-1,""ROLEID"":-1,""GROUPNAME"":""SYSTEM"",""ACCESSTYPE"":5,""STATUSSTART"":0,""STATUSEND"":0,""ACCESSALLFACILITIES"":""Y"",""ACCESSALLISSUERS"":""Y"",""ACCESSALLPRIORSTUDIES"":""N""}"
                );
        }

        [TestMethod]
        [TestCategory("Integration.Simple")]
        public void XferPhysicianSpecialty()
        {
            // Setup Item under test.
            var env = SetupForConversion();


            env.DoTest("PHYSICIANSPECIALTYTYPE", 22, 22); // --> [dbo].[PhysicianSpecialty]
        }

        [TestMethod]
        [TestCategory("Integration.Complex")]
        public void XferEmployer()
        {
            // Setup Item under test.
            var env = SetupForConversion();


            env.DoTest("EMPLOYER", 2); // --> [dbo].[Employer]
        }


        [TestMethod]
        [TestCategory("Integration.Complex")]
        public void XferUserList()
        {
            // Setup Item under test.
            var env = SetupForConversion();

            // Expectation: Creating a user will create its group and role automatically.

            // a five item declaration is like this
            // [ "GROUPID", "[InternalGroupID]", "[dbo].[Group]", "[GroupName]", "SYSTEM", "int" ],
            // should expectedQueryCountResult in the incoming integer groupid being transformed via matching the firebird GROUPLIST table field GROUPNAME
            // like SYSTEM, which has a GROUPID of -1, to an equivalent USER ID.
            // The default role is SYSTEM, and the identity type is integer for this identity.  Int on the firebird side, and different Int
            // on the MS SQL side.


            env.DoTest("USERLIST", 45); // --> [dbo].[USER]
        }

        //[TestMethod]
        [TestCategory("Integration.Simple")]
        public void XferStudyAccess()
        {
            // Setup Item under test.
            var env = SetupForConversion();


            env.DoTest("STUDYACCESS", 50000); // --> [dbo].[?]
        }


        [TestMethod]
        [TestCategory("Integration.Simple")]
        public void XferDepartmentsForScheduler()
        {
            // Setup Item under test.
            var env = SetupForConversion();


            env.DoTest("DEPARTMENTS", 5); // --> [dbo].[?]
        }



        [TestMethod]
        [TestCategory("Integration.Complex")]
        public void XferProcedureCodes()
        {
            // This test is marked complex because of the LARGE number of rows it covers.

            var env = SetupForConversion( 300 );

           // env.Conversion.EnableDuplicateChecks = false; // time to execute when true = 1 minute, time to execute when false = 1 minute:



            env.DoTest("PROCCODES", 300 ); // --> [dbo].[?]  8923
        }



        [TestMethod]
        [TestCategory("Integration.Complex")]
        public void XferModalitiesAndResources()
        {
            // Setup Item under test.
            var env = SetupForConversion();

            // modalities should ALWAYS be done once before resources.
            env.DoTest("MODALITIES", 26);

            // depends on departments, rooms, facilities, issuers, modalities.
            env.DoTest("RESOURCES", 19);
        }


        [TestMethod]
        [TestCategory("Integration.Complex")]
        public void XferPatients()
        {
            // Setup Item under test.
            var env = SetupForConversion();

            // This test requires @LOOKUP feature to work!

            //env.DoTest("COUNTRYLIST"); // pre-migrated to ISO standard state

            env.DoTest("PATIENTMODULE", 36); // --> [dbo].[?]
        }

       // [TestMethod]
        [TestCategory("Integration.Complex2")]
        public void XferPatientStudies()
        {
            // Setup Item under test.
            var env = SetupForConversion();

            /*
            What's tricky about this is we're testing some crazy new extended sub field insertion syntax:
             "+", "[InternalImagingServiceRequestId]", "[phi].[ImagingServiceRequest]",
             [
               [ "INTERNALPATIENTID", "[InternalPatientId]", "[phi].[Patient]", "[InternalPatientID]", "int" ],
               [ "ACCESSIONNUMBER", "[AccessionNumber]" ]
             ]

            During IHE/MIMA work when this fails on select...
            ParamValueLookupSelectFromMssql() --> field.SelectCommand.ExecuteReader() --> Invalid SQL which was generated by 
            DataAccessLayer.DoSetupFieldSelectCommandQuery() because field.GetSelectFieldActual() is not correct.

            Field lookup error in INTERNALPATIENTID sql: SELECT TOP 1 [InternalPatientId]
                 FROM 
                [phi].[Patient]
                 WHERE [InternalPatientID] = '1.2.124.113540.0.201205291215.3.613'

               .. should be WHERE [PatientID] = '1.2.124.113540.0.201205291215.3.613'


            But if we change the above to:

             [
                "+", "[InternalImagingServiceRequestId]", "[phi].[ImagingServiceRequest]",
                [
                  [ "INTERNALPATIENTID", "[InternalPatientId]", "[phi].[Patient]", "[PatientID]", "nvarchar" ],
                  [ "ACCESSIONNUMBER", "[AccessionNumber]" ]
                ]
              ]

            We fail somewhere else.

            Test method RamSoft.FirebirdToSqlServer.IntegrationTests.XferPatientStudies threw exception: 
            RamSoft.FirebirdToSqlServer.Model.DataAccessLayerException: FB Lookup Failed / String Value Unexpected: 
            SELECT * FROM PATIENTMODULE WHERE INTERNALPATIENTID = @SINGLEVALUE 
                ---> System.FormatException: Input string was not in a correct format.


            */

            env.DoTest("PATIENTSTUDY", 272); // --> [dbo].[?]
        }


        //[TestMethod]
        [TestCategory("Integration.Complex2")]
        public void XferSchedulerTopLevel()
        {
            // What's tricky about this is we're testing some crazy new extended sub field insertion syntax.
            //
            // time estimate: 1 second
            // same as other call to DoPatientScheduleAppointments but this only those lower level rows that are needed to satisfy
            // FK dependencies in appointments table.
            DoPatientScheduleAppointments(false, true);
        }

        //[TestMethod]
        [TestCategory("Integration.Complex2")]
        public void XferSchedulerAll()
        {

            // What's tricky about this is we're testing some crazy new extended sub field insertion syntax.
            //
            // time estimate: 2 seconds.
            // same as other call to DoPatientScheduleAppointments but this converts all lower level tables as a block first,
            // even if they are not required to satisfy FK dependencies in appointments table.
            DoPatientScheduleAppointments(true, true);


        }

       // [TestMethod]
        [TestCategory("Integration.Complex2")]
        public void XferSchedulerAllWithoutDupeChecking()
        {
            // What's tricky about this is we're testing some crazy new extended sub field insertion syntax.
            //
            // time estimate: 2 seconds.
            // same as other call to DoPatientScheduleAppointments but this converts all lower level tables as a block first,
            // even if they are not required to satisfy FK dependencies in appointments table.
            DoPatientScheduleAppointments(true, false);


        }

        protected void DoPatientScheduleAppointments(bool dependantTablesFirstCase, bool duplicateChecking)
        {

        // Setup Item under test.
        var env = SetupForConversion();

            env.Conversion.EnableDuplicateChecks = duplicateChecking;


            // To move an appointment row, we would have to implicitly move its study and patient as well,
            // so this is a fairly advanced test, if it fails, and a patient or study integration test fails, debug
            // the patient or study integration test first!

            /*
            select A.appointmentid, A.starttime claimed,
            cast(A.starttime as date) startdate,
            cast(A.finishtime as date) enddate,
            cast(A.starttime as time) starttime,
            cast(A.finishtime as time) endtime,
            A.RESOURCEID,    A.INTERNALSTUDYID,    A.APPOINTMENTSTATE,
            A.LASTUPDATEUSERID,
            case BLOCKEDREASON
            when null then '?'
            when '' then
            COALESCE (P.FAMILYNAME || ' ' || P.GIVENNAME || ' : ' || S.STUDYDESCRIPTION,'UNKNOWN')
            else
            BLOCKEDREASON
            end
            DISPLAY
            from appointments A
            left join patientstudy S on A.INTERNALSTUDYId=S.internalstudyid
            left join patientmodule P on P.INTERNALPATIENTID=S.INTERNALPATIENTID
            */


            env.DoTest("APPOINTMENTS", 414, -1, dependantTablesFirstCase); // --> [dbo].[?]

            // patient duplication error catching logic
            env.QueryCheckCount("SELECT COUNT(*) FROM [phi].[Patient] WHERE PatientName LIKE 'WAGMAMA^JULIE%'", 1,
                "Expect EXACTLY ONE patient named JULIE WAGMAMA");

            // catch patient ID wrong error.
            env.QueryCheckStr("SELECT PATIENTID FROM [phi].[Patient] WHERE PatientName LIKE 'WAGMAMA^JULIE%'", "JULIEWAGMAMA001234.1234.1234"); // PatientID string carefully concocted in the unit test pacs.fdb

            // catch patient assigning authority (was called issuer in firebird) of ID wrong.
            env.QueryCheckStr(@"SELECT I.Issuer FROM [phi].[Patient] P
                                LEFT JOIN [dbo].[AssigningAuthority] I ON I.InternalAssigningAuthorityID = P.InternalAssigningAuthorityID
                                WHERE P.PatientName LIKE 'WAGMAMA^JULIE%'", "PETER");

            // facility duplication error or unexpected conversion check.
            if (dependantTablesFirstCase)
            {
                // march 16, 2015: This is failing, when we get 2 here, what's happening is we're doing two
                // sub-table conversions, and somewhere we're not logging the lookup.

                if (duplicateChecking)
                {
                    env.QueryCheckCount(
                        "SELECT COUNT(*) FROM dbo.Facility WHERE FacilityName = 'A REFERENCE FACILITY'", 1,
                        "Expect ONE FACILITY with given name");
                }
                else
                {
                    env.QueryCheckCount(
                    "SELECT COUNT(*) FROM dbo.Facility WHERE FacilityName = 'A REFERENCE FACILITY'", 2,
                    "Expect TWO FACILITIES with given name");
                }

            }
            else
            {
                env.QueryCheckCount("SELECT COUNT(*) FROM dbo.Facility WHERE FacilityName = 'A REFERENCE FACILITY'", 0,
                    "Expect NO FACILITY with given name");
            }


            // Appointment key visible information for a particular small set of patients is precisely correct:
            string resultCheck =
                  @"-- fetch integration test expectedQueryCountResult for patients last name ending in WAG
                    select  s.AccessionNumber,s.ModalityCode,rt.Display,StartDate,StartTime,EndDate,EndTime
                    FROM [dbo].[ReservedTime] rt
                    LEFT JOIN [phi].[Study] S ON s.InternalStudyID = rt.InternalStudyID
                    LEFT JOIN [dbo].[ReservedTimeResource] rtr ON rtr.InternalReservedTimeID = rtr.InternalReservedTimeID
                    WHERE rt.InternalStudyID IS NOT NULL
                    AND rt.display LIKE 'WAG%'
                    ORDER BY s.ModalityCode, rt.Display, rt.StartDate desc";

            /* equivalent firebird query:
                    select S.ACCESSIONNUMBER,S.SCHEDULEDMODALITY modalitycode,
                    case BLOCKEDREASON
                    when null then '?'
                    when '' then
                    COALESCE (P.FAMILYNAME || ' ' || P.GIVENNAME || ' : ' || S.STUDYDESCRIPTION,'UNKNOWN')
                    else
                    BLOCKEDREASON
                    end
                    DISPLAY,
                    cast(A.starttime as date) startdate,
                    cast(A.finishtime as date) enddate,
                    cast(A.starttime as time) starttime,
                    cast(A.finishtime as time) endtime
                    from appointments A

                    left join patientstudy S on A.INTERNALSTUDYId=S.internalstudyid
                    left join patientmodule P on P.INTERNALPATIENTID=S.INTERNALPATIENTID
                    where P.FAMILYNAME like 'WAG%'
                    order by S.SCHEDULEDMODALITY,P.FAMILYNAME,a.starttime desc

            */
            /* expected expectedQueryCountResult:
                AccessionNumber, ModalityCode, Display, StartDate, StartTime, EndDate, EndTime
                RAM665, CR, WAGMAMA JULIE : 100, 2014-11-07, 14:20:00, 2014-11-07, 15:20:00
                RAM831, CT, WAGMAMA JULIE : CT TYPE 1, 2015-04-21, 18:00:00, 2015-04-21, 19:00:00
                RAM818, CT, WAGMAMA JULIE : CT TYPE 1, 2015-04-16, 09:15:00, 2015-04-16, 09:45:00
                RAM728, CT, WAGMAMA JULIE : CT TYPE 1, 2015-02-27, 13:30:00, 2015-02-27, 14:30:00
                RAM624, CT, WAGMAMA JULIE : CT TYPE 1, 2015-02-03, 15:15:00, 2015-02-03, 16:00:00
                RAM621, CT, WAGMAMA JULIE : CT TYPE 1, 2015-01-08, 08:30:00, 2015-01-08, 09:00:00
                RAM636, CT, WAGMAMA JULIE : CT TYPE 1, 2014-10-03, 12:45:00, 2014-10-03, 13:15:00
                RAM632, CT, WAGMAMA JULIE : CT TYPE 1, 2014-09-27, 16:00:00, 2014-09-27, 16:45:00
                RAM643, CT, WAGMAMA JULIE : CT TYPE 1, 2014-09-11, 15:05:00, 2014-09-11, 15:35:00
                RAM633, CT, WAGMAMA JULIE : CT TYPE 1, 2014-07-28, 13:30:00, 2014-07-28, 14:00:00
                RAM630, CT, WAGMAMA JULIE : CT TYPE 1, 2014-07-08, 14:30:00, 2014-07-08, 15:00:00
                RAM631, CT, WAGMAMA JULIE : CT TYPE 1, 2014-07-08, 15:30:00, 2014-07-08, 16:00:00
                RAM628, CT, WAGMAMA JULIE : CT TYPE 1, 2014-07-03, 16:00:00, 2014-07-03, 16:30:00
                RAM623, CT, WAGMAMA JULIE : CT TYPE 1, 2014-06-26, 16:15:00, 2014-06-26, 16:45:00
                RAM620, CT, WAGMAMA JULIE : CT TYPE 1, 2014-06-24, 18:00:00, 2014-06-24, 19:30:00
                RAM824, CT, WAGNER WALTER : CT TYPE 1, 2015-04-22, 10:15:00, 2015-04-22, 10:45:00
                MERGE3, CT, WAGNER WALTER : CT TYPE 1, 2015-04-16, 09:00:00, 2015-04-16, 09:30:00
                RAM743, CT, WAGNER WALTER : CT TYPE 1, 2015-04-14, 15:00:00, 2015-04-14, 19:00:00
                RAM656, CT, WAGNER WALTER : CT TYPE 1, 2014-11-10, 13:20:00, 2014-11-10, 13:55:00
                RAM915, CT, WAGSTROM ANGELA : CT MAXILOFACIAL, 2015-10-14, 15:00:00, 2015-10-14, 16:00:00
                RAM916, CT, WAGSTROM ANGELA : CT MAXILOFACIAL SINUS W/O CONTRAST FOLLOWED BY CONTRAST, 2015-10-20, 13:00:00, 2015-10-20, 13:30:00
                RAM854, CT, WAGSTROM ANGELA : CT TYPE 1, 2015-05-08, 11:00:00, 2015-05-08, 12:00:00
                RAM650, DX, WAGMAMA JULIE : AAA, 2014-10-27, 10:45:00, 2014-10-27, 11:45:00
                RAM806, DX, WAGMAMA JULIE : AAA-DX, 2015-04-27, 09:00:00, 2015-04-27, 09:30:00
                RAM859, DX, WAGMAMA JULIE : AAA-DX, 2015-04-14, 20:30:00, 2015-04-14, 21:00:00
                RAM807, DX, WAGMAMA JULIE : AAA-DX, 2015-03-16, 10:00:00, 2015-03-16, 10:30:00
                RAM651, DX, WAGNER WALTER : AAA, 2014-10-27, 12:00:00, 2014-10-27, 13:00:00
                RAM644, DX, WAGSTROM ANGELA : AAA - DX, 2014-10-24, 18:20:00, 2014-10-24, 18:50:00
                RAM837, MG, WAGSTROM ANGELA : MG STUDY TYPE 1, 2015-04-30, 11:00:00, 2015-04-30, 11:45:00
                RAM727, MR, WAGMAMA JULIE : MRI TYPE 1, 2015-02-27, 17:15:00, 2015-02-27, 20:00:00
                RAM629, MR, WAGMAMA JULIE : MRI TYPE 1, 2014-07-04, 15:45:00, 2014-07-04, 16:30:00
                RAM625, MR, WAGMAMA JULIE : MRI TYPE 1, 2014-06-26, 11:45:00, 2014-06-26, 12:15:00
                RAM842, PT, WAGSTROM ANGELA : PT TYPE 1, 2015-04-16, 15:00:00, 2015-04-16, 16:15:00
                RAM862, PT, WAGSTROM ANGELA : PT2, 2015-04-22, 14:45:00, 2015-04-22, 15:00:00
                RAM763, RF, WAGNER WALTER : RF1S, 2015-04-21, 16:00:00, 2015-04-21, 17:00:00
                RAM772, RF, WAGSTROM ANGELA : RF1S, 2015-04-09, 10:30:00, 2015-04-09, 12:00:00
                RAM733, US, WAGNER WALTER : US STUDY TYPE 1, 2015-04-03, 11:45:00, 2015-04-03, 13:30:00
*/

            string[] resultHashList =
            {

            "ADF3B8F50A808F66EBC04153C68A9EE0", // #0 RAM665, CR, WAGMAMA JULIE : 100, 11/7/2014 12:00:00 AM, 14:20:00, 11/7/2014 12:00:00 AM, 15:20:00

            "3D6DE17063994BBDF90DCC56C75E9264", // #1 RAM831, CT, WAGMAMA JULIE : CT TYPE 1, 4/21/2015 12:00:00 AM, 18:00:00, 4/21/2015 12:00:00 AM, 19:00:00

            "33E882DD5BA8D207097879FEC87920EC", // #2 RAM818, CT, WAGMAMA JULIE : CT TYPE 1, 4/16/2015 12:00:00 AM, 09:15:00, 4/16/2015 12:00:00 AM, 09:45:00

            "62629747EC1E75C8EB8D80B85D375360", // #3 RAM728, CT, WAGMAMA JULIE : CT TYPE 1, 2/27/2015 12:00:00 AM, 13:30:00, 2/27/2015 12:00:00 AM, 14:30:00

            "48577B103F34D8510981EDA5E65B230E", // #4 RAM624, CT, WAGMAMA JULIE : CT TYPE 1, 2/3/2015 12:00:00 AM, 15:15:00, 2/3/2015 12:00:00 AM, 16:00:00

            "1EC953FDBC5E1AF3DA64B8E9D6F19295", // #5 RAM621, CT, WAGMAMA JULIE : CT TYPE 1, 1/8/2015 12:00:00 AM, 08:30:00, 1/8/2015 12:00:00 AM, 09:00:00

            "C7CCBCF78D33A84CC741E989A2FCCF4E", // #6 RAM636, CT, WAGMAMA JULIE : CT TYPE 1, 10/3/2014 12:00:00 AM, 12:45:00, 10/3/2014 12:00:00 AM, 13:15:00

            "FE4F32F6A7C5221E1F11312FD767D78E", // #7 RAM632, CT, WAGMAMA JULIE : CT TYPE 1, 9/27/2014 12:00:00 AM, 16:00:00, 9/27/2014 12:00:00 AM, 16:45:00

            "8395B2A09F2AAF26079727CC2EF44D75", // #8 RAM643, CT, WAGMAMA JULIE : CT TYPE 1, 9/11/2014 12:00:00 AM, 15:05:00, 9/11/2014 12:00:00 AM, 15:35:00

            "D75B5BB847F04093FCB224D57ABB2A98", // #9 RAM633, CT, WAGMAMA JULIE : CT TYPE 1, 7/28/2014 12:00:00 AM, 13:30:00, 7/28/2014 12:00:00 AM, 14:00:00

            "51E1EB1E39E17C176A5172B2AB9F2F9F", // #10 RAM630, CT, WAGMAMA JULIE : CT TYPE 1, 7/8/2014 12:00:00 AM, 14:30:00, 7/8/2014 12:00:00 AM, 15:00:00

            "A7699B1AC75D97D07262613E7D687515", // #11 RAM631, CT, WAGMAMA JULIE : CT TYPE 1, 7/8/2014 12:00:00 AM, 15:30:00, 7/8/2014 12:00:00 AM, 16:00:00

            "8B6926DFDEE3FCF65A895177B75EACBE", // #12 RAM628, CT, WAGMAMA JULIE : CT TYPE 1, 7/3/2014 12:00:00 AM, 16:00:00, 7/3/2014 12:00:00 AM, 16:30:00

            "4C5628431C3C5EC84DE1B598C4174600", // #13 RAM623, CT, WAGMAMA JULIE : CT TYPE 1, 6/26/2014 12:00:00 AM, 16:15:00, 6/26/2014 12:00:00 AM, 16:45:00

            "3107840A3884CA51ED2A05834B6F9E8E", // #14 RAM620, CT, WAGMAMA JULIE : CT TYPE 1, 6/24/2014 12:00:00 AM, 18:00:00, 6/24/2014 12:00:00 AM, 19:30:00

            "C2B267FE3678662A4E5B23CBC1F2FBA7", // #15 RAM824, CT, WAGNER WALTER : CT TYPE 1, 4/22/2015 12:00:00 AM, 10:15:00, 4/22/2015 12:00:00 AM, 10:45:00

            "F63478449E613FFEC43A114384B74C3B", // #16 MERGE3, CT, WAGNER WALTER : CT TYPE 1, 4/16/2015 12:00:00 AM, 09:00:00, 4/16/2015 12:00:00 AM, 09:30:00

            "1A6B2FC8B1C90EFEC205BD6B86E84ED9", // #17 RAM743, CT, WAGNER WALTER : CT TYPE 1, 4/14/2015 12:00:00 AM, 15:00:00, 4/14/2015 12:00:00 AM, 19:00:00

            "C3BC9EE0E52880120B27EA7E1020632B", // #18 RAM656, CT, WAGNER WALTER : CT TYPE 1, 11/10/2014 12:00:00 AM, 13:20:00, 11/10/2014 12:00:00 AM, 13:55:00

            "0D64B620EFC36FF6B4636EA4C11AF535", // #19 RAM915, CT, WAGSTROM ANGELA : CT MAXILOFACIAL, 10/14/2015 12:00:00 AM, 15:00:00, 10/14/2015 12:00:00 AM, 16:00:00

            "3C9877837F7961EF51DE2391B8E2A76E", // #20 RAM916, CT, WAGSTROM ANGELA : CT MAXILOFACIAL SINUS W/O CONTRAST FOLLOWED BY CONTRAST, 10/20/2015 12:00:00 AM, 13:00:00, 10/20/2015 12:00:00 AM, 13:30:00

            "8184FDE2F71FCFA9D25A979E65B40CCC", // #21 RAM854, CT, WAGSTROM ANGELA : CT TYPE 1, 5/8/2015 12:00:00 AM, 11:00:00, 5/8/2015 12:00:00 AM, 12:00:00

            "B480B2CCF10B66A8F3EB81A9CF5C2FE5", // #22 RAM650, DX, WAGMAMA JULIE : AAA, 10/27/2014 12:00:00 AM, 10:45:00, 10/27/2014 12:00:00 AM, 11:45:00

            "7D50AFEB25DBE6CA61653188076250E9", // #23 RAM806, DX, WAGMAMA JULIE : AAA-DX, 4/27/2015 12:00:00 AM, 09:00:00, 4/27/2015 12:00:00 AM, 09:30:00

            "E12316D25E022CA693FE720ED46ABC74", // #24 RAM859, DX, WAGMAMA JULIE : AAA-DX, 4/14/2015 12:00:00 AM, 20:30:00, 4/14/2015 12:00:00 AM, 21:00:00

            "784EF69908BAAFE8C1F5A68A45E59813", // #25 RAM807, DX, WAGMAMA JULIE : AAA-DX, 3/16/2015 12:00:00 AM, 10:00:00, 3/16/2015 12:00:00 AM, 10:30:00

            "1BF01EFCF419AAA6546AE402761AD00E", // #26 RAM651, DX, WAGNER WALTER : AAA, 10/27/2014 12:00:00 AM, 12:00:00, 10/27/2014 12:00:00 AM, 13:00:00

            "27EEFEE4305F26A99A7B35F5A8BEB9F7", // #27 RAM644, DX, WAGSTROM ANGELA : AAA - DX, 10/24/2014 12:00:00 AM, 18:20:00, 10/24/2014 12:00:00 AM, 18:50:00

            "5A757E096AE0068C6B733C7B65BB9994", // #28 RAM837, MG, WAGSTROM ANGELA : MG STUDY TYPE 1, 4/30/2015 12:00:00 AM, 11:00:00, 4/30/2015 12:00:00 AM, 11:45:00

            "C4975C199AB94B23ABB0995B645494C9", // #29 RAM727, MR, WAGMAMA JULIE : MRI TYPE 1, 2/27/2015 12:00:00 AM, 17:15:00, 2/27/2015 12:00:00 AM, 20:00:00

            "E482F109EDC22ED73837DE3F8435FDA6", // #30 RAM629, MR, WAGMAMA JULIE : MRI TYPE 1, 7/4/2014 12:00:00 AM, 15:45:00, 7/4/2014 12:00:00 AM, 16:30:00

            "8BBD1E40DBA74DD5F3B01F5C361B0D41", // #31 RAM625, MR, WAGMAMA JULIE : MRI TYPE 1, 6/26/2014 12:00:00 AM, 11:45:00, 6/26/2014 12:00:00 AM, 12:15:00

            "7F199928EE7F95643EE922CF387E8AA9", // #32 RAM842, PT, WAGSTROM ANGELA : PT TYPE 1, 4/16/2015 12:00:00 AM, 15:00:00, 4/16/2015 12:00:00 AM, 16:15:00

            "86957790EEADE696C8BB8BE908FA2B57", // #33 RAM862, PT, WAGSTROM ANGELA : PT2, 4/22/2015 12:00:00 AM, 14:45:00, 4/22/2015 12:00:00 AM, 15:00:00

            "7FCF2F96ADE2741FC662EEF3B2CE2496", // #34 RAM763, RF, WAGNER WALTER : RF1S, 4/21/2015 12:00:00 AM, 16:00:00, 4/21/2015 12:00:00 AM, 17:00:00

            "2EA0A246623A0F6203755E5DA655B603", // #35 RAM772, RF, WAGSTROM ANGELA : RF1S, 4/9/2015 12:00:00 AM, 10:30:00, 4/9/2015 12:00:00 AM, 12:00:00

            "58A27F9C8DF5F283F15F27368543262A" // #36 RAM733, US, WAGNER WALTER : US STUDY TYPE 1, 4/3/2015 12:00:00 AM, 11:45:00, 4/3/2015 12:00:00 AM, 13:30:00


            };

            env.QueryCheckHash(resultCheck, resultHashList); // resultHashList



            //--firebird: expect expectedQueryCountResult on incoming data is 1.
            //select count(*) from PATIENTMODULE where FAMILYNAME like 'WAGMAMA%' and GIVENNAME like 'JULIE%'





        }
    }
}