using System.Data.SqlClient;
using FirebirdSql.Data.FirebirdClient;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;

namespace RamSoft.FirebirdToSqlServer
{
    public class TestDataAccessLayer : DataAccessLayer
    {
        public object IntegrationTestOwner;

        public TestDataAccessLayer(FbConnection fbConnection,
            SqlConnection sqlConnection,
            object integrationTestEnv,
            IColumnInfoCache columnInfoCache) : base(fbConnection, sqlConnection, columnInfoCache)
        {
            IntegrationTestOwner = integrationTestEnv;
        }

        public override object InsertSqlExecute(int statementIndex)
        {
            object inheritedResult = base.InsertSqlExecute(statementIndex);
            //  IntegrationTestEnv should count Inserts.
            (IntegrationTestOwner as IntegrationTestEnv).CountInserts(this);

            return inheritedResult;

        }
    }
}