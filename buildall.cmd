set TOPHOMEDIR=%CD%
cd FirebirdToSQLServer
call build.cmd
if %ERRORLEVEL% gtr 0 goto fail
cd %TOPHOMEDIR%\FirebirdToSQLServer
call unittest.cmd
if %ERRORLEVEL% gtr 0 goto fail
cd %TOPHOMEDIR%\FirebirdToSQLServer
call integrationtest.cmd
if %ERRORLEVEL% gtr 0 goto fail

:fail