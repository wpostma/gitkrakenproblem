﻿using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json.Linq;
using Ramsoft.TestFixtures;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;
using StatementType = RamSoft.FirebirdToSqlServer.Interface.StatementType;

// implicitly depends on Helpers

namespace FirebirdToSQLServerTests
{
    [TestClass]
    public class DuplicateCheckingTests
    {
        private const string patientmodule1 = @"{
            ""AMODULE"": { ""X"": ""Y"",
                ""existing"": 0,
                },
            ""PATIENTMODULE"": {
                ""destination"": ""[PHI].[Patient]"",
                ""destintidentity"": 1,
                ""identity"": ""PATIENTID"",
                ""depends"": [ ""CACHEDISSUEROFPATIENTIDS"", ""FACILITIES"", ""MODALITIES"", ""RESOURCES"" ],
                ""key"": [ ""int"", ""INTERNALPATIENTID"", ""[InternalPatientId]"" ],
                ""existing"": ""select InternalPatientID from phi.Patient where InternalAssigningAuthorityID = (select InternalAssigningAuthorityID from [dbo].[AssigningAuthority] where Issuer= @ISSUEROFPATIENTID and PatientID = @PATIENTID"",
                ""fields"": [
                  [ ""ISSUEROFPATIENTID"", ""InternalAssigningAuthorityID"", ""[dbo].[AssigningAuthority]"", ""[AssigningAuthority]"", ""DEFAULT"" ],
                  [ ""PATIENTID"", ""[PatientID]"" ],
                  [ ""$DICOMNAME"", ""PatientName"" ],
                  [ ""PATIENTTIMESTAMP"", ""Timestamp"" ],
                  [ ""$GUID_GENERATE()"", ""PatientGUID"" ],
                  [ ""+ISSUEROFPATIENTID"", ""Issuer"" ],
                  [ ""COUNTRY"", ""COUNTRYCODE"" ],
                  [ ""HOMEPHONE"", ""PHONEHOME"" ],
                  [ ""BUSINESSPHONE"", ""PHONEBUSINESS"" ],
                  [ ""@LOOKUP"", ""LastUpdateUserID"", ""[dbo].[User]"" ],
                  [ 1, ""IsActive"" ],
                  [ ""EMAIL"", ""EmailAddress"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              }
            }";

        [TestMethod]
        [TestCategory("Duplicate")]
        public void DuplicateDisableInMappingJson()
        {
            var mapdict = JObject.Parse(patientmodule1);
            var mapping = new ExportMapping("AMODULE", mapdict);

            // table exists in mapping.json but the attribute existing is set but is intentionally put as flag value 0
            string notExisting2 = mapping.GetExistingItemQuery("AMODULE");
            Assert.AreEqual(notExisting2, string.Empty);
        }

        [TestMethod]
        [TestCategory("Duplicate")]
        public void DuplicatePatientModuleExistingQueryLoadedFromJSON()
        {
            // We're going to generate the Existing-Item (SELECT IDENTITY FROM TABLE WHERE FIELD1 = 'X')
            // query. To do this without a real data layer we are going to pretend.

            var mapdict = JObject.Parse(patientmodule1);
            var mapping = new ExportMapping("PATIENTMODULE", mapdict);


            // now we introduce the map to a reader, but to do that we need to mock the
            // data readers.
            var mockFbReader = new Mock<IDataReader>(); // supports IDataReader
            var mockSqlColumnInfo = new Mock<IColumnInfo>(); // supports IDataReader

            // firebird column info reader with a few fields,  FACILITYNAME, ISSUEROFPATIENTID
            mockFbReader.Setup(t => t.FieldCount).Returns(3);
            mockFbReader.Setup(t => t.GetName(0)).Returns("INTERNALPATIENTID");
            mockFbReader.Setup(t => t.GetFieldType(0)).Returns(typeof (int));
            mockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("int");

            mockFbReader.Setup(t => t.GetName(1)).Returns("ADDRESS");
            mockFbReader.Setup(t => t.GetFieldType(1)).Returns(typeof (string));
            mockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("varchar");

            mockFbReader.Setup(t => t.GetName(3)).Returns("LOOKUP");
            mockFbReader.Setup(t => t.GetFieldType(3)).Returns(typeof (string));
            mockFbReader.Setup(t => t.GetDataTypeName(3)).Returns("varchar");


            mockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(4);

            mockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("InternalPatientId"); // NOT in the same order as above!!
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("int");

            mockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("Address");
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("nvarchar");

            mockSqlColumnInfo.Setup(t => t.GetName(2)).Returns("LastUpdateUserID");
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("int");

            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            mockSqlColumnInfo.Setup(t => t.GetName(3)).Returns("ExtJSON"); // NOT in the same order as above!!
            mockSqlColumnInfo.Setup(t => t.GetDataTypeName(3)).Returns("nvarchar");

            // DONE MOCKING. Now we execute the method we are testing.

            mapping.MapFields(mockFbReader.Object, mockSqlColumnInfo.Object, null);

            // Until the MapFields is run, the Insert Statement cannot be built, the mapping object is not fully inspectable.
            // But we're not verifying that the actual result is valid, just that it's not empty.
            string sql1 = TestHelper.StripCommentsAndWhiteSpace(
                mapping.BuildStatement(StatementType.Insert));

            // case where query exists in mapping.json
            for (var i = 0; i < 99; i++) // for loop gives us more of an idea of the efficiency of the JSON layer.
            {
                string existing = mapping.GetExistingItemQuery("PATIENTMODULE");
                var expected =
                    "select InternalPatientID from phi.Patient where InternalAssigningAuthorityID = (select InternalAssigningAuthorityID from [dbo].[AssigningAuthority] where Issuer= @ISSUEROFPATIENTID and PatientID = @PATIENTID";
                Assert.AreEqual(existing, expected);
            }
            // On Warren's PC the above logic takes about 300 ms to run 100 times, so that means it's about 3 milliseconds per json value fetch.

            // case where no such table exists
            string notExisting = mapping.GetExistingItemQuery("SOMETHING");
            Assert.AreEqual(notExisting, string.Empty);


            Assert.IsTrue(mapping.GetSevereErrorCount() > 3,
                "Expected at least 3 errors, so we know the class does some parsing");
        }
    }
}