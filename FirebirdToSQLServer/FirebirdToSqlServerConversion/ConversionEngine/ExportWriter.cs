﻿using System;
using System.Data;
using Newtonsoft.Json;
using RamSoft.FirebirdToSqlServer.Exceptions;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;
using RamSoft.FirebirdToSqlServer.Util;
using StatementType = RamSoft.FirebirdToSqlServer.Interface.StatementType;

// main class: ExportWriter

namespace RamSoft.FirebirdToSqlServer.ConversionEngine
{



    public class SingleDefaultValue
    {
        public object Value;
        public string ValueField; // should end up as an @PARAM = defaultValue
        public string SelectTable;
        public object DefaultValue;
        public bool IdentityFlag; // avoid Cannot insert explicit value for identity column in table 'X' when IDENTITY_INSERT is set to OFF.
    }


    ///<summary>Firebird to SQL Server 2016 Data Writer
    ///</summary>
    ///<remarks>
    /// This is used from inside the main level Conversion layer, which is used inside a form or a console
    /// runner application.   It has one particular firebird table as a source, and one primary destination, and multiple secondary
    /// destination tables.  There are different kinds of secondary destinations.  Lookup values are foreign key values, typically an
    /// int auto-incrementing identity(1,1). There are also splits that can occur where one row in Firebird is split into two rows in two
    /// different rows in MS SQL.   Each field in firebird can be mapped automatically to a field with the same name in MS SQL, and with
    /// a compatible type.  It is also possible to specify for each firebird field name what the corresponding new field name is.
    /// The ExportMapping reads this information from a Mapping.json file.
    ///
    /// var mapping = new ExportMapping("TABLENAME");
    /// var writer = new ExportWriter(fbConnection,sqlConnection,Mapping, ...);
    ///</remarks>
    public class ExportWriter
    {
        private IExportWriterOwner _owner;
        private string _name;
        private int _lookupFail;
        private int _lookupSuccess;

        private int _maxRowCount;

        IDataAccessLayer _dataAccessLayer;


        protected int _subInsertStatementConfigured;
        public int UniqueConstraintSkipCount { get; set; } // skipped rows that already exist


        SingleDefaultValue _single;        // magic single value generator feature case; current single value info

        public object LastIdentity { get; set; }  // normally null or ordinal (number), but could be a GUID or something wild.


        //private SqlCommand _sqlCommandGetPK;
        // private string _sqlInsertTemplate; // insert into form INSERT INTO TABLE (Name, PhoneNo, Address) VALUES (@Name, @PhoneNo, @Address)");


        private IMapping _mapping; // some tables map via int, some via string.

        // Needs a connection to Firebird and to MS SQL, needs a mapping to determine what to read and what to write.
        public ExportWriter(IExportWriterOwner owner,
                                              IDataAccessLayer dataAccessLayer,
                                              IMapping mapping,
                                              int maxRowCount,

                                              // Optional special case fields.
                                              SingleDefaultValue single = null
                                              )
        {

            _owner = owner;
            _maxRowCount = maxRowCount;
            _dataAccessLayer = dataAccessLayer;
            _mapping = mapping;
            _single = single;

            dataAccessLayer.SetupBeforeMapping(mapping, single);
            SetupMapping(mapping, single);
            _name = mapping.FirebirdTableName + ":" + mapping.GetMicrosoftSqlTableName();

        }

        public override string ToString()
        {

            return $"{{ ExportWriter {_name} }}";
        }

        public SingleDefaultValue SingleDefaults()
        {
            return _single;
        }


        protected void SetupMapping(IMapping mapping, SingleDefaultValue single = null)
        {
            // TODO: Depend only on our own interface, instead of all of IDataReader.
            IDataReader fbReader = _dataAccessLayer.FirebirdColumnReader();
            if (fbReader==null)
                throw new DataConversionFailureException("SetupMapping: _dataAccessLayer.FirebirdColumnReader failed for " + mapping.FirebirdTableName);


            // Reset flags
            mapping.ResetFlags(); // set _ExtJSON = false;

            if (single != null)
            {
                Log.Write(LogLevel.LInfo, mapping.FirebirdTableName + ": SetupMapping single case START");
            }
            else
            {
                Log.Write(LogLevel.LInfo, mapping.FirebirdTableName + ": SetupMapping multi case START");
            }




            var sqlColumnsData = _dataAccessLayer.GetSqlColumnInfo();

                // we must have the field map fully initialized before we build a statement.
            mapping.MapFields(fbReader, sqlColumnsData, single);

            // Begin Mapping BUILD statement and sub insert statement phase.

            string insertSql = mapping.BuildStatement( StatementType.Insert);

            _subInsertStatementConfigured = mapping.BuildSubInsertStatements(); // fields with sub-fields have their own sub-table-item-insert statements

            // End Mapping BUILD statement and sub insert statement phase.

            //Log.Write("SQL insert: " + insertSql);
            if (String.IsNullOrEmpty(insertSql))
            {
                throw new DataConversionFailureException("SetupMapping: mapping.BuildStatement failed for " + mapping.FirebirdTableName);

            }
            _lookupSuccess = 0;
            _lookupFail = 0;



            _dataAccessLayer.SetupInsertSql( insertSql);




            Log.Write(LogLevel.LInfo, mapping.FirebirdTableName + ": setupmapping END");

        }





        protected string GenerateExtJsonStr()
        {
            var rowDictionary = _dataAccessLayer.FirebirdReaderJsonSerializeRowFull();



            // TODO: SerializeRowPartial could be used to just select the columns we want.
            return JsonConvert.SerializeObject(rowDictionary, Formatting.None);



        }


        public object InsertRowAndFetchIdentity(object incomingValue, IField field)
        {
            string originalTableName = _mapping.ReverseLookupTableName(field.SelectTable);
            // TODO Refactor this writer class.
            if (_owner == null)
                throw new WriterFailException("Internal failure: owner missing unable to look up "+field.SelectTable);

            return _owner.InsertRowAndFetchIdentity(originalTableName, incomingValue, field);

        }

        protected bool TryEvaluateSpecialValue(int statementIndex, int paramIndex, IField field)
        {
            string inputValue = field.InField;
            object value = null;

            // Sometimes we get data from a source OTHER than the firebird table, or we have a literal value,
            // in that case, it's okay to get here, otherwise it's not okay.
            if (inputValue[0]== '@')
            {
                string fieldName = inputValue.Substring(1);
                //int indexIncoming = field.queryIndex;

                int fieldIndex = _mapping.FindOutOrInFieldIndex(fieldName);
                if (fieldIndex < 0)
                {
                    Log.Write(LogLevel.LError, "Operator @ notation requires valid field name, field " + fieldName + " not valid.");
                    return false;
                }


                string literalValue = (string)_dataAccessLayer.FirebirdReaderGetValue(fieldIndex);
                // example PATIENTMODULE: @LASTUPDATEUSER Dereferenced Identity incoming value is ADMIN
                Log.Write(LogLevel.LFine, _mapping.FirebirdTableName + ": " + field.InField + " Dereferenced Identity incoming value is " + literalValue);
                // Now fetch the foreign key value on MS SQL side for something with entity name
                // specified (ROLE=SYSTEM, GROUP=FRED, or whatever.)

                _dataAccessLayer.SetupFieldSelectCommandQuery(_mapping,
                    DataReaderQueryType.EntityNamed,
                    field,
                    literalValue);

                // now if we fail to find the existing entity, we can auto-create it if we know the mapping for it.


                // read and get key value or auto-create key on destination MS-SQL:
                return ParamValueLookupSelectFromMssql( /*incoming*/ literalValue, statementIndex, paramIndex, field, fieldIndex);










            }
            if (inputValue[0] == '#')
            {
                // special case: integer literal mode. inField=="#0" or inField="#1".
                value = Int32.Parse(inputValue.Substring(1));
                if ((int)value >= 0)
                {
                    _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, value);
                    return true;
                }

            }
            else if (inputValue == "$GUID_GENERATE()")
            {
                var guid = Guid.NewGuid();
                value = guid.ToString();
                _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, value);
                return true;
            }
            else if (inputValue == "$DICOMNAME")
            {
                // DICOM PN 6.2.1 format POSTMA^WARREN^A^MR^ESQ
                var name = new DicomPersonNameHelper(
                    _dataAccessLayer.FirebirdReaderGetValueByName("FAMILYNAME"),
                    _dataAccessLayer.FirebirdReaderGetValueByName("GIVENNAME"),
                    _dataAccessLayer.FirebirdReaderGetValueByName("MIDDLENAME"),
                    _dataAccessLayer.FirebirdReaderGetValueByName("NAMEPREFIX"),
                    _dataAccessLayer.FirebirdReaderGetValueByName("NAMESUFFIX")
                    );

                value = name.FormattedString();
                _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, value);
                return true;
            }

            return false;
        }

        public int WriteRowWithDefaults()
        {
            const int statementIndex = 0;

            int mappedFieldCount = _mapping.GetSelectedFieldsCount(); // BEWARE: can contain the same field mapped more than once, so if there are 10 fields to copy, and one is copied twice, this could be 11!
            if (mappedFieldCount < 1)
            {
                throw new WriterFailException("Internal failure: mapping information contains no field mappings");
            }



            int paramCount = _dataAccessLayer.SetupBeforeWriteRowWithDefaults(_mapping);

            if (paramCount<=0)
            {
                throw new WriterFailException("Internal failure: parameter setup failure in data access layer");
            }

            for (int paramIndex = 0; paramIndex < paramCount; paramIndex++)
            {
                var field = _mapping.GetSelectedField(paramIndex);
                int indexIncoming = field.QueryIndex;

                if (field.InField == "*")
                {
                    // skip it for now.
                    _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, "{ \"DEFAULT\": \"DEFAULT\" }");
                }
                else
                // validate that the field.index value is in the range of values that exist inside _fbReader's fields.
                if (indexIncoming >= 0)
                {
                    if ((_single != null) && (field.InField == _single.ValueField))
                    {
                        _dataAccessLayer.InsertSqlParamValue(statementIndex,paramIndex,  _single.Value);
                    }
                    else
                    {
                        _dataAccessLayer.InsertSqlParamValue(statementIndex,paramIndex, field.SelectDefaultValue);
                    }
                }
                else
                {

                    if (!TryEvaluateSpecialValue(statementIndex, paramIndex, field))
                    {
                        throw new WriterFailException("Internal failure: unable to determine value/meaning of " + StrHelpers.NameOfObjectStr(field.InField) + " during write row with defaults");
                    }


                }


            };

            object newIdentity = _dataAccessLayer.InsertSqlExecute(statementIndex);



            int newIdentityInt = Convert.ToInt32(newIdentity);

            if (newIdentityInt > 0)
            {
                // Question: is it even worth creating a cache here when the cache can get stale?
                //_owner.addIdentity(_mapping.getMicrosoftSQLTableName, _sqlCommandIns.Parameters[0].Value.ToString());

                Log.Write(LogLevel.LDebug, "Insert automatic row into " + _mapping.GetMicrosoftSqlTableName() + " identity "+newIdentityInt.ToString() );
                LastIdentity = newIdentityInt;

                _dataAccessLayer.Commit(statementIndex);

                return newIdentityInt;

            }
            else
            {
                Log.Write(LogLevel.LError, "Insert automatic row into " + _mapping.GetMicrosoftSqlTableName() +  " FAILED");
                return 0;
            }
        }

        protected static bool FieldCompatibilityCheck(string fbtype, string sqltype)
        {
            if (StrHelpers.SameText(fbtype, sqltype))
            {
                return true;
            }
            else if ((StrHelpers.SameText(fbtype, "timestamp") || StrHelpers.SameText(fbtype, "date"))
                     && ( StrHelpers.SameText(sqltype, "datetime2") || StrHelpers.SameText(sqltype, "date")))
            {
                return true;
            }
            else if ((StrHelpers.SameText(fbtype, "varchar") || StrHelpers.SameText(fbtype, "char"))
                      && (StrHelpers.SameText(sqltype, "nvarchar")|| StrHelpers.SameText(sqltype, "char")))
            {
                return true;
            }
            else if (  StrHelpers.SameText(fbtype, "integer") &&
                     ( StrHelpers.SameText(sqltype, "bigint") ||
                       StrHelpers.SameText(sqltype, "smallint") ||
                       StrHelpers.SameText(sqltype, "int") )
                    )
            {
                return true;
            }
            else if (StrHelpers.SameText(fbtype, "NUMERIC") &&
                    (StrHelpers.SameText(sqltype, "smallmoney") || StrHelpers.SameText(sqltype, "money"))
                   )
            {
                return true;
            }


            else
                return false;
        }

        protected static string GetFirebirdDbTz()
        {  // TODO Support timezones the way we are going to do this in real production
           return "+05:00";
        }

        protected static int GetSqlBoolValue(object aValue)
        {
            if (aValue == null)
                return 0;
            if ((aValue is string) && ((string)aValue == ""))
                return 0;
            string avalue = aValue.ToString();
            if (avalue == "Y")
                return 1;
            int nvalue= 0;
            if (Int32.TryParse(avalue, out nvalue))
            {
                if (nvalue > 0)
                    return 1;
            }

            return 0;

        }

        // In order to prove correctness of our writer's value-copy/translation we need to be
        // able to hook in and watch its moves.
        protected void  ParamValueCopyDirectWithValue(int statementIndex, int paramIndex, IField field, int indexIncoming, object value )
        {   // the reason the other two fields are here is to make this method more unit testable,
            // and so we could subclass this writer and make specialized overrides/translations.

            _dataAccessLayer.InsertSqlParamValue(statementIndex,paramIndex, value);
            _owner.ChecksAfterParameterSetValue(this, CopyType.Direct, paramIndex, field, indexIncoming, value);


        }


        protected int ParamValueCopyDirect(int statementIndex, int paramIndex,  IField field)
        {
            int indexIncoming = field.QueryIndex;


            // Copy Firebird Data Values Directly and Literally into SQL Server without Change.
            // Except for coercing T_YESNO ('Y','N') to bit (integer 0 or 1).
            if (field.OutDbType == null)
            {
                field.OutDbType = _dataAccessLayer.GetInsertSqlParamTypeStr(statementIndex, paramIndex);
            };

            var inValue = _dataAccessLayer.FirebirdReaderGetValue(indexIncoming);// _fbReader.GetValue(indexIncoming);

            if (StrHelpers.SameText(field.OutDbType, "bit"))
            {
                ParamValueCopyDirectWithValue(statementIndex, paramIndex, field, indexIncoming, GetSqlBoolValue(inValue));


            }
            else if (StrHelpers.SameText(field.InDbType, "TIMESTAMP") && StrHelpers.SameText(field.OutDbType, "datetimeoffset"))
            {

                if (!ObjHelper.ValueNullOrDbNull(inValue))
                {
                    ParamValueCopyDirectWithValue(statementIndex, paramIndex, field, indexIncoming, inValue + " " + GetFirebirdDbTz()); // TODO This TZ-determination function is currently faked out.
                }
                else
                {
                    /* no change */
                    ParamValueCopyDirectWithValue(statementIndex, paramIndex, field, indexIncoming, inValue);
                }

            }
            else
            {
                if (!FieldCompatibilityCheck(field.InDbType, field.OutDbType))
                {
                    Log.Write(LogLevel.LWarning, _mapping.FirebirdTableName+": "+field.OutField + " type requires unsupported Implicit Data conversion: " + field.InField + ":" + field.InDbType + " => " + field.OutDbType);

                }


                ParamValueCopyDirectWithValue(statementIndex, paramIndex, field, indexIncoming, inValue);


                if (field.NotNullFlag && ObjHelper.ValueNullOrDbNull(inValue))
                {
                    if (field.OutDbType=="int")
                    {
                        ParamValueCopyDirectWithValue(statementIndex, paramIndex, field, indexIncoming, 0);
                    }
                    else
                        ParamValueCopyDirectWithValue(statementIndex, paramIndex, field, indexIncoming, "");

                    // TODO: Handle more types than just string and integer
                }
            }
            return 1;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails")]
        protected bool ParamValueLookupSelectFromMssql(object incomingValue,
                       int statementIndex,
                       int paramIndex,
                       IField field,
                       int indexIncoming)
        {
            // NULL case.
            if (ObjHelper.ValueNullOrDbNull(incomingValue))
                throw new FieldLookupException("Internal problem: paramValueLookupSelectFromMSSQL does not support incomingValue == null.","parameter incomingValue null");

            if (field.SelectCommand==null)
                throw new FieldLookupException("Internal problem: paramValueLookupSelectFromMSSQL called for a field that has no query", "SelectCommand null");


            try
            {
                using (var selectReader = field.SelectCommand.ExecuteReader())
                {
                    // If data exists already we can convert firebird data into sql data to satisfy relational dependency.
                    if (selectReader.Read())
                    {
                        Log.Write(LogLevel.LDebug, _mapping.FirebirdTableName + "." + field.InField + " Lookup SUCCESS");
                        _lookupSuccess++;
                        object existingIdentity = selectReader.GetValue(0);
                        if (existingIdentity==null)
                        {
                            Log.Write(LogLevel.LError, _mapping.FirebirdTableName + "." + field.InField+ " Fetching Lookup value from SelectReader failed.");
                        }
                        _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, existingIdentity);
                    }
                    else
                    {
                        Log.Write( LogLevel.LInfo, _mapping.FirebirdTableName + "." + field.InField + " Lookup in "+ field.SelectTable + " did not find any row for value "+StrHelpers.NameOfObjectStr(incomingValue));

                        // INSERT ROW because the SELECT above returned 0 rows.
                        if (!ObjHelper.ValueNullOrDbNullOrEmptyString(incomingValue))
                        {
                            _lookupFail++;
                            var newRowIdent = InsertRowAndFetchIdentity(incomingValue, field);
                            if (newRowIdent==null)
                            {
                                Log.Write(LogLevel.LError, _mapping.FirebirdTableName + "." + field.InField + " insertRowAndFetchIdentity failed.");
                            }
                            Log.Write(LogLevel.LFine, "Lookup setting parameter "+paramIndex.ToString()+" to newRowIdent:"+StrHelpers.NameOfObjectStr(newRowIdent)  );
                            _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, newRowIdent);
                        }
                        else
                        {
                            Log.Write(_mapping.FirebirdTableName + " : Null value case. param " + paramIndex.ToString());
                            _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, null);

                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {


                _dataAccessLayer.HandleExceptionLogging(statementIndex, _mapping, ex, field.SelectCommand); // creates some exception logging and also stores failed sql.


                if ((ex.Errors.Count != 1)||(ex.Errors[0].Number != SqlConsts.UniqueIndexValueConstraintViolation))
                {
                    if (ex.Number == SqlConsts.NullCheckViolation)
                        throw ex;
                    if (ex.Number == SqlConsts.InvalidColumnName)
                        throw ex;
                    if (ex.Number == SqlConsts.InsertIdentityNotAllowed)
                        throw ex; // fix: Add     "destintidentity": 1 to mapping.json
                    // internal failure probably, since the user can't fix a missing parameter.
                    if (ex.Number == SqlConsts.ParamValueMissing)
                        throw new FieldLookupException("Internal problem: Lookup failed, parameter value missing and no default provided for " + field.OutField, ex.Message);


                    string sql = "?";
                    string paramStr = "?";
                    if (field.SelectCommand != null)
                    {
                        if (field.SelectCommand.Parameters.Count > 0)
                        {
                            paramStr = _dataAccessLayer.GetParamValueAsStr(field.SelectCommand, 0);
                        }
                        sql = field.SelectCommand.CommandText.Replace("@VALUE", paramStr);
                    }

                    // One kind of problem here is that field.selectCommand.Parameters[0] type is int32, value is some integer like 8, but field in the query is a string
                        // SELECT TOP 1 [InternalRoleID] FROM [dbo].[Role] WHERE [Name] = @VALUE
                    if (ex.Number == SqlConsts.ConversionError)
                    {
                        string msg = _mapping.FirebirdTableName + ": Field-lookup resulted in a conversion error " +
                                     field.InField + "  sql: " + sql;
                        Log.Write(LogLevel.LError, msg);
                        throw new FieldLookupException(msg, ex.Message);
                    }
                    else
                    {   // TRANSLATE because lookup exception is not useful:
                        throw new FieldLookupException("Field lookup error in " + field.InField + " sql: " + sql,
                            ex.Message);
                        // OR PRESERVE STACK?
                        //rethrow; // PRESERVE STACK.

                    }


                }
            }
            return true;
        }

        protected int ParamValueWithLookup(int statementIndex, int paramIndex,
                            IField field)
        {

            int indexIncoming = field.QueryIndex;
            // Lookup Corresponding Lookup Code in SQL Server side, having same logical identity
            // on the firebird side.
            //string sqlCmdStr = "";
            object incomingValue = _dataAccessLayer.FirebirdReaderGetValue(indexIncoming);

            string columnType = _dataAccessLayer.FirebirdReaderGetColumnType(indexIncoming); // Firebird column types uppercase value like "INTEGER", NOT MSSQL types like "INT" or "BIGINT"

            string incomingValueEntityName = "";



            if (ObjHelper.ValueNullOrDbNull(incomingValue))
            {
                Log.Write(LogLevel.LWarning, _mapping.FirebirdTableName + ": lookup numeric value missing for " + field.InField);

                incomingValue = field.SelectDefaultValue;

                if ((incomingValue is string) && (columnType == "INTEGER")&&(field.ForeignIdentity))
                {
                    // skip the lookup, the default contains the implicit lookup value.
                    ParamValueWithLookupSelectCommandQuery(statementIndex, paramIndex, field, (string)incomingValue , indexIncoming);
                    return 1;
                }

                if  (ObjHelper.ValueNullOrDbNull(incomingValue))
                {
                    // no translate on NULL.
                    _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, incomingValue);
                    _owner.ChecksAfterParameterSetValue(this, CopyType.Lookup, paramIndex, field, indexIncoming, incomingValue);

                    return 0;
                }
            }

            // It's important that flags like field.foreignIdentity get set when we're parsing the Mapping.
            // If not, then the failure occurs later.

            // If incoming value is some integer like -1, meaningful only in firebird,
            // we need to translate the integer back to its meaning. Useful for Issuers,
            // Modalities, Resources, but not for Patient, Study.
            if ( field.ForeignIdentity) // was && (_mapping.identity == field.inField)
            {
                // Transform incoming value from Integer foreign key valid only on
                // source database into a human readable value valid on the destination database.
                // For example, translating GROUP or ROLE from one database to another.
                // On Firebird
                // select FIRST 1 ROLENAME from rolelist where ROLEID=-1
                // That might return a value like 'SYSTEM'.

                incomingValueEntityName = _dataAccessLayer.FirebirdLookupEntityName( _mapping, field, incomingValue);


                Log.Write(LogLevel.LFine, _mapping.FirebirdTableName + ": " + field.InField + " Foreign Identity Lookup of value " + incomingValue + " to name " + incomingValueEntityName);
                // Now fetch the foreign key value on MS SQL side for something with entity name
                // specified (ROLE=SYSTEM, GROUP=FRED, or whatever.)

                _dataAccessLayer.SetupFieldSelectCommandQuery(_mapping,
                    DataReaderQueryType.EntityNamed,
                    field,
                    incomingValueEntityName);





                  // read and get key value or auto-create key on destination MS-SQL:
                ParamValueLookupSelectFromMssql(incomingValue, statementIndex, paramIndex, field, indexIncoming);




                /*
                string incomingValueEntityName = _dataAccessLayer.FirebirdLookupEntityName(_mapping, field, incomingValue);
                Log.Write(LogLevel.LFine,
                    _mapping.FirebirdTableName + ": " + field.InField + " Foreign Identity Lookup of value " + incomingValue +
                    " to name " + incomingValueEntityName);
                ParamValueWithLookupSelectCommandQuery(paramIndex, field, incomingValueEntityName, indexIncoming);
                */
            }
            else if ((field.ForeignIdentity)&&(field.SelectField == null))
            {

                Log.Write(LogLevel.LFine, field.InField + " JSON Identity Lookup of " + incomingValue);

                _dataAccessLayer.SetupFieldSelectCommandQuery(_mapping,
                    DataReaderQueryType.JsonValue,
                    field,
                    incomingValue);



                ParamValueLookupSelectFromMssql(incomingValue, statementIndex, paramIndex, field, indexIncoming);



            }
            else
            {
                Log.Write(LogLevel.LFine, field.InField + " Simple Lookup of " + incomingValue);

                // Simple lookup.
                _dataAccessLayer.SetupFieldSelectCommandQuery(_mapping,
                    DataReaderQueryType.SimpleLookup,
                    field,
                    incomingValue);

                // read and get key value or auto-create key on destination MS-SQL:
                ParamValueLookupSelectFromMssql(incomingValue, statementIndex, paramIndex, field, indexIncoming);
            }

            _owner.ChecksAfterParameterSetValue(this, CopyType.Lookup, paramIndex, field, indexIncoming, incomingValue);

            return 1;
        }

        private void ParamValueWithLookupSelectCommandQuery(int statementIndex, int paramIndex, IField field,
            string entityName, int indexIncoming)
        {


            // Now fetch the foreign key value on MS SQL side for something with entity name
            // specified (ROLE=SYSTEM, GROUP=FRED, or whatever.)

            _dataAccessLayer.SetupFieldSelectCommandQuery(_mapping,
                DataReaderQueryType.EntityNamed,
                field,
                entityName);


            // read and get key value or auto-create key on destination MS-SQL:
            ParamValueLookupSelectFromMssql(entityName, statementIndex, paramIndex, field, indexIncoming);
        }
        /// <summary>
        /// Set a parameter field to the identity of a sub-inserted entity defined in subfields underneath this field.
        /// </summary>
        /// <param name="statementIndex">0 for the main insert, 1..n for some subfield identity insertion</param>
        /// <param name="paramIndex">0..n are the INSERT sql-parameter values named with an @ and the real field name</param>
        /// <param name="field">The field information from the export mapping</param>
        /// <returns></returns>
        protected void ParamValueSubFieldFunction(int statementIndex, int paramIndex, IField field)
        {
            // If we didn't call mapping.BuildSubInsertStatements() at some point prior to
            // reaching this point or if that code failed, then this code isn't going to
            // be able to continue. Instead of failing with extremely obscure SQL error
            // we should try to know that we're in this bad state and inform the internal
            // developer who is repairing it.
            if (_subInsertStatementConfigured <= 0)
                throw new WriterFailException("Internal failure: ParamValueSubFieldFunction called before mapping.BuildSubInsertStatements() or mapping.BuildSubInsertStatements() failed.");


            // If the incoming statementIndex is 0, then the sub-entity parameters will be 1, and so on.



            // When moving from a firebird STUDY which links directly to PATIENT to a
            // MS SQL model where the PATIENT is linked via an IMAGESERVICEREQUEST to a STUDY,
            // we have to create new identity object and then insert the identity of that sub-object.

            /* field example from Mapping.json for PATIENTSTUDY:
                  [
                    "+", "[InternalImagingServiceRequestId]", "[phi].[ImagingServiceRequest]",
                    [
                      [ "INTERNALPATIENTID", "[InternalPatientId]", "[phi].[Patient]", "[InternalPatientID]", "int" ],
                      [ "ACCESSIONNUMBER", "[AccessionNumber]" ]
                    ]
                  ]

            */
            int subStatementIndex = statementIndex + 1;

            // Get RamSoft.FirebirdToSqlServer.Model.ColumnInfo, list of string DataTypeName, string Name.
            //var sqlColumnsData = _dataAccessLayer.GetSqlColumnInfo(field.SelectTable);

            // _dataAccessLayer.SetupInsertSql() on the second layer INSERT is called _dataAccessLayer.SetupBeforeSubFieldInsert
            // TODO: Rethink the naming of data access layer interfaced methods.
            _dataAccessLayer.SetupBeforeSubFieldInsert(subStatementIndex, field); // Sets up INSERT statement for statement index other than zero, from a field and its subfield declarations.
            // loop over subfields
            int subFieldCount = field.GetSubFieldCount();
            int nullFieldValueCounter = 0;
            for (int subFieldIndex = 0; subFieldIndex < subFieldCount; subFieldIndex++)
            {
                var subField = field.GetSubField(subFieldIndex);
                // do stuff...
                ExecuteOneRowField(subStatementIndex,subField,subFieldIndex,ref nullFieldValueCounter);

            }
            var identity = _dataAccessLayer.InsertSqlExecute( subStatementIndex);
            _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, identity);




        }
        protected bool ParamValueSpecialFunction(int statementIndex, int paramIndex, IField field)
        {
           // Sometimes we get data from a source OTHER than the firebird table, or we have a literal value,
            // in that case, it's okay to get here, otherwise it's not okay.
            // field.infield contains the primary expression to be evaluated.
            //object value;
            if (!TryEvaluateSpecialValue(statementIndex, paramIndex,field))
            {
                throw new WriterFailException("Internal failure: unable to determine value/meaning of " + StrHelpers.NameOfObjectStr(field.InField) + " during Execute");
            }

            //_dataAccessLayer.InsertSqlParamValue(paramIndex, value); <-- inside the TryEval...

            return true;
        }


        public const int ExecuteOneRowSkip = -1;
        public const int ExecuteOneRowAbort = -2;

        protected void ExecuteOneRowField(int statementIndex, IField field, int paramIndex, ref int nullFieldValueCounter)
        {

            int indexIncoming = field.QueryIndex;
            int firebirdFieldCount = _dataAccessLayer.FirebirdReaderGetFieldCount();
            if (firebirdFieldCount <= 0)
            {
                throw new WriterFailException(
                    "Internal failure: _dataAccessLayer.FirebirdReaderGetFieldCount returned invalid field count");
            }


            if (field.InField == "*")
            {
                string str = GenerateExtJsonStr();
                _dataAccessLayer.InsertSqlParamValue(statementIndex, paramIndex, str);
                Log.Write(LogLevel.LFine, "Generated JSON:" + str);

            }
            else if (field.InField == "+")
            {
                ParamValueSubFieldFunction(statementIndex, paramIndex, field);
            }
            else
            if (field.SpecialFunctionDetect())
            {
                ParamValueSpecialFunction(statementIndex, paramIndex, field);

            }
            else
            if ((indexIncoming >= 0) && (indexIncoming < firebirdFieldCount))
            {
                // if the field.index value is in the range of values that
                // exist inside _fbReader's fields then it's a field to field
                // copy or a transform from a field to a field.
                if (String.IsNullOrEmpty(field.SelectTable) && String.IsNullOrEmpty(field.SelectField))
                {  // simple case
                    ParamValueCopyDirect(statementIndex, paramIndex, field);
                }
                else
                {  // lots of complex lookup and transform cases.
                    ParamValueWithLookup(statementIndex, paramIndex, field);

                };


                if (_dataAccessLayer.GetInsertSqlParamValueIsNull(statementIndex, paramIndex))
                {
                    // TODO: Actual null value handler logic. (Maybe have default values on some fields?)
                    nullFieldValueCounter++;
                    if (paramIndex == 0)
                    {
                        throw new WriterFailException("Internal failure: Unexpectedly NULL parameter value INSERT " + _mapping.GetMicrosoftSqlTableName());


                    }
                }


            } /* if */
              // expected: infield=EMPLOYERNAME, outfield=EmployerName, param=@EmployerName
              //  Log.Write( "@"+field.outField + " = " + StrHelpers.NameOfObjectStr(_sqlCommandIns.Parameters[paramIndex].Value));
        }
        /// <summary>
        ///  Executes a single row insert, or returns a special code if a skip or abort condition prevented it.
        /// </summary>
        /// <returns>Row number value from 0..n, or special negative flags like ExecuteOneRowSkip or ExecuteOneRowAbort </returns>
        protected int ExecuteOneRow()
        {
            const int baseStatementIndex = 0;
            int nullFieldValueCounter = 0;

            int rowCounter = 0;

            int paramCount = _dataAccessLayer.GetInsertSqlParamCount(baseStatementIndex);



            _owner.ChecksBeforeExecuteOneRow(paramCount); // validate that parameter count and firebird field count are acceptable.

            // begin parameter loop. we have to set the values.
            for (int paramIndex = 0; paramIndex < paramCount; paramIndex++)
            {
                var field = _mapping.GetSelectedField(paramIndex);
                ExecuteOneRowField(baseStatementIndex,  field, paramIndex, ref nullFieldValueCounter);
            } /* for parameter loop */



            try
            {

                var checkBeforeInsertResult =  _owner.ChecksBeforeInsertIdentity(this,  _mapping, _dataAccessLayer);


                if (checkBeforeInsertResult == CheckBeforeInsertAction.Skip)
                {
                    LastIdentity = _dataAccessLayer.GetLastIdentity();
                    return ExecuteOneRowSkip;
                }
                else if (checkBeforeInsertResult == CheckBeforeInsertAction.Abort)
                    return ExecuteOneRowAbort;


                object newIdentity = _dataAccessLayer.InsertSqlExecute(0); //  execute SQL INSERT statement. Main one is statementIndex 0, secondary ones are 1,2,3...

                if (newIdentity != null)
                {
                    rowCounter++; // success
                    if ((rowCounter % 1000) == 0)
                    {
                        Log.Write(LogLevel.LFine, "WRITER OK: " + rowCounter.ToString() + " items moved from " +
                            _mapping.FirebirdTableName);
                    }

                    // Owner may want to be notified of identities created.
                    // Owner might want to generate some sample data for manual verification.
                    _owner.ChecksAfterInsertIdentity(this, _mapping, _dataAccessLayer, newIdentity);



                    LastIdentity = newIdentity;

                    _dataAccessLayer.Commit(baseStatementIndex);



                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // Goal here is to log enough information to help us figure out whether code failed
                // or whether configuration (Mapping.json) is wrong.
                bool shouldRaise =  _dataAccessLayer.HandleExceptionLogging( baseStatementIndex, _mapping, ex, null);
                if (shouldRaise)
                {  // A constraint uniqueness failure is continuable because it means the entity was already transferred probably.
                    // A constraint on FOREIGN KEY means we didn't do our foreign-key-re-Mapping right.
                    // In the case of a pre-populated table like tzdb.Country, we don't migrate ISO country codes. That stuff is static.
                    // Every site will have a pre-populated and hopefully complete ISO country code list.
                    throw;
                }
                else
                {
                    UniqueConstraintSkipCount++;  // TODO: Log the existing identity/key values somewhere?
                }
            }//end-catch.

            //To-do: Progress callback every 100 rows?


            return rowCounter;
        }




        // Execute: apply mapping, return inserted/modified row count.
        public int Execute()
        {
            const int statementIndex = 0;
            int readCounter = 0;
            int rowCounter = 0;

            if (_mapping == null)
            {
                throw new WriterFailException("Internal failure: mapping information missing");
            }


            int mappedFieldCount = _mapping.GetSelectedFieldsCount();
            if (mappedFieldCount<1)
            {
                throw new WriterFailException("Internal failure: mapping information contains no field mappings");
            }


            _dataAccessLayer.SetupBeforeExecute(statementIndex, _mapping);




            while ((_dataAccessLayer.FirebirdRead()) && (rowCounter < _maxRowCount) && (readCounter < _maxRowCount))
            {
                readCounter++;
                _owner.SetContextRow(readCounter);

                int execResult = ExecuteOneRow();
                if (execResult > 0)
                {
                    rowCounter += execResult;
                }
                else
                {   // in this case LastIdentity should be set to existing item.
                    rowCounter++; // found existing item, or found a fault that would prevent inserting a new item. Want to return value greater than zero to prevent default insertion
                }


            }

           Log.Write(LogLevel.LFine, _mapping.FirebirdTableName+ "LOOKUP SUCCESS COUNTER " + _lookupSuccess.ToString());
           Log.Write(LogLevel.LFine, _mapping.FirebirdTableName + "LOOKUP FAIL COUNTER: " + _lookupFail.ToString());



            return rowCounter;
        }


    }


    // end namespace.
}
