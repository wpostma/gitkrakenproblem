﻿using System;
using System.Collections.Generic;
using System.Threading;
using RamSoft.FirebirdToSqlServer.Interface;

// ConversionWorker


namespace RamSoft.FirebirdToSqlServer.ConversionEngine
{

    // TODO: Refactor ConversionWorker using tasks.

    public class WorkerCompletedArgs : EventArgs
    {
        public Exception WorkerException;

        public  int TotalCount { get; set; }

        public WorkerCompletedArgs(Exception ex, int totalCount)
        {
            TotalCount = totalCount;
            WorkerException = ex;
        }
    }

    /// <summary>
    /// This worker provides one thread context in which all our data conversion work occurs.
    ///
    /// Initially only one worker thread will be associated with a complete job, and the goal here is to avoid
    /// blocking main UI thread 0, as well as making the job interruptible via a cancel button in the user interface.
    ///
    /// Eventually it would be nice if we could split the jobs up and run multiple workers.
    /// Figuring out how to do that might be a hard problem.
    /// </summary>
    public class ConversionWorker
    {
        private readonly Thread _thread;
        private readonly IEnumerable<string> _tables;
        private readonly IConversion _conversion;
        private readonly List<int> _resultsList;
        private int _totalCount;
        private Exception _ex; // will be null if the worker did not fault, which is the Normal+Good case.

        public event EventHandler<WorkerCompletedArgs> WorkerCompletedEvent;  // Fired many times. Single line status text.


        protected void Execute()
        {
            try
            {
                // ReSharper disable once LoopCanBePartlyConvertedToQuery
                foreach (string table in _tables)
                {
                    // NOTE: conversion engine will raise an exception to abort itself
                    // in a clean fashion, if we call _conversion.Cancel
                    int aresult = _conversion.DoConvert(table);
                    _totalCount += aresult;
                    _resultsList.Add(aresult);

                }
            }
            catch (Exception ex)
            {

                System.Diagnostics.Debug.WriteLine("ERROR: ConversionWorker: Background operation aborted. ");
                System.Diagnostics.Debug.WriteLine(ex.Message);
                _ex = ex;
            }
            // tell main thread we're finished.
            // ReSharper disable once InvertIf
            if (WorkerCompletedEvent != null)
            {
                var args = new WorkerCompletedArgs(_ex,_totalCount);
                WorkerCompletedEvent(this, args);
            }

        }

        public bool WaitCompleted(int timeout) // returns -1 if not finished yet, otherwise returns a total number of rows converted for all tables.
        {
            if (!_thread.Join(timeout))
                return false;
            else
                return true;
        }

        public Exception GetException()
        {
            return _ex;
        }

        public void Start()
        {
            _thread.Start();
        }

        public void Cancel() // don't use Thread.Abort, it's evil, kind of like TThread.Terminate.
        {
            _conversion.Cancel();
        }


        public ConversionWorker(IConversion Conversion, IEnumerable<string> tables)
        {
            _tables = tables;
            _conversion = Conversion;
            _thread = new Thread(Execute) {IsBackground = true};
            _resultsList = new List<int>();

        }

    }
}
