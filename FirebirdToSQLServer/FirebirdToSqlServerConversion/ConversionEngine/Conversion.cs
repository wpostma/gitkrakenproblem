﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Newtonsoft.Json.Linq;
using RamSoft.FirebirdToSqlServer.Exceptions;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;
using RamSoft.FirebirdToSqlServer.Util;
//############################################### Type Equivalences, local for this unit ########################################
using JsonTableDictionary =
    System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, object>>;


// RamSoft Firebird to SQL Server - Conversion object
//----------------------------------------------------------------------
//
// The conversion object takes connection strings for firebird and SQL,
// and manages the lifetime and initialization of the writer, and makes the
// writer do its stuff.
//
//
//    _conversion = new Conversion(
//                                mappingJsonFileNameToBeRead,
//                                new DataAccesslayerFactory(fbConnStr,sqlConnStr),
//                                maxrowcount);
//
//   SetupUI(/*IEnumerable<string>:*/_conversion.Tables); // get the list of tables out.
//
//  Before using, set options, wire up Events:
//     _conversion.SetSampleOutputCounter(10);
//     _conversion.StatusEvent += StatusEvent;
//     _conversion.SampleOutputEvent += SampleOutputEvent;
//
//  To make it do its job, run DoConvert:
//         _conversion.DoConvert(tableName);
//
//  Normally we would want to run conversion in its own interruptible thread,
//  so see ConversionWorker which has this responsibility.
//
//----------------------------------------------------------------------

namespace RamSoft.FirebirdToSqlServer.ConversionEngine
{

    public class Lookups : ILookups
    {
        private readonly Dictionary<string, object> _lookups;

        public Lookups()
        {
            _lookups = new Dictionary<string, object>();
        }

        public bool TryGetValue(string keyName, out object value)
        {
            return _lookups.TryGetValue(keyName, out value);
        }

        public void Add(string keyName, object value)
        {
            _lookups.Add(keyName, value);
        }



    }

    public class Conversion : IExportWriterOwner, IConversion

    {

        /// <summary>
        ///  An abstraction so we can provide different existing-identity-lookup strategies, rather than just hard coding the dictionary in here and having it have the same lifetime as this object
        /// </summary>
        protected ILookups _lookups;

        /// <summary>
        ///  Statistics so we can detect problems, bugs.
        /// </summary>
        protected volatile int _lookupSuccessCounter = 0;
        /// <summary>
        ///  Statistics so we can detect problems, bugs.
        /// </summary>
        protected volatile int _lookupDefaultsCounter = 0;

        /// <summary>
        /// during exception handling, we grab and store the sql for later use.
        /// </summary>
        private string _lastFailedSqlStatement;

        /// <summary>
        /// max rows PER TABLE to be converted.
        /// </summary>
        private readonly int _cfgMaxRowCount;

        /// <summary>
        /// Data access layer factory, also is the thing that creates FB and SQL reader and command objects used by the conversion logic.
        /// </summary>
        private readonly IDataAccessLayerFactory _dalFactory;

        /// <summary>
        /// Single global shared JSON configuration
        /// </summary>
        private readonly JObject _mappingDictionary;
        /// <summary>
        ///  Per table expansion on the mappings.
        /// </summary>
        private readonly Dictionary<string, ExportMapping> _mappings;

        private volatile bool _cancel;

        // context data.
        protected string Context;
        protected int ContextRow;

        // PUBLIC-PROPERTIES:
        /// <summary>
        ///  Turn on the check for duplicates before inserting an identity.
        /// </summary>
        public bool EnableDuplicateChecks { get; set; } = true;



        public Conversion(string mappingFileName,
            IDataAccessLayerFactory dalFactory,
            ILookups lookups,
            int maxRowCount)
        {
            _dalFactory = dalFactory;
            _lookups = lookups;

            // Load JSON into a JObject which we will treat like a dictionary.
            // It must contain a mapping from firebird table names to sql server table names,
            // so we don't have to maintain that in our binary code.
            _mappingDictionary = JsonHelpers.LoadJson(mappingFileName);
            _mappings = new Dictionary<string, ExportMapping>();


            // getting data out example; firebird table USERLIST maps to ms sql table [dbo].[User]
            // JObject item = (JObject)_MappingDictionary["USERLIST"];
            // string dest = (string)item["destination"];
            // Debug.WriteLine(dest); // expected: dest== "[dbo].[User]"

            // LINQ is so cool.
            Tables = _mappingDictionary.Properties().
                Where(p => string.CompareOrdinal(p.Name.Substring(0, 2), "__") != 0). // Skip items that start with __
                Select(p => p.Name).ToList();

            Tables.Sort();

            _cfgMaxRowCount = maxRowCount;
        }

        public int GetLookupSuccessCounter()
        {
            return _lookupSuccessCounter;

        }

        public int GetLookupDefaultsCounter()
        {
            return _lookupDefaultsCounter;

        }

        public string ServerUrl { get; set; }

        public int SampleOutputCounter { get; set; }


        public object InsertRowAndFetchIdentity(string firebirdTableName, object incomingValue,
            IField field)
        {
            if (string.IsNullOrEmpty(firebirdTableName))
                throw new DataConversionFailureException("InsertRowAndFetchIdentity:parameter firebirdTableName missing");
            if (string.IsNullOrEmpty(firebirdTableName))
                throw new DataConversionFailureException("InsertRowAndFetchIdentity:parameter firebirdTableName missing");




            /*
            If we needed to have multiple types of lookups to different entities, we could do that by
            differentiating the lookup keys for two or more cases....
                char identityFlag = '.';
                if (field.ForeignIdentity)
                {
                    identityFlag = '&';
                }
                else
                {
                    identityFlag = '#';
                };
            */

            // lookupKey: [table].[field].'PATIENTID1234567' or
            //            [table].[field].1234567
            string lookupKey = StrHelpers.AddBrackets(field.SelectTable) + '.' + field.GetSelectFieldActual()+'.'+StrHelpers.NameOfObjectStr(incomingValue);
            object lookupValue;
            if (_lookups.TryGetValue(lookupKey, out lookupValue)) // This is some external Dictionary that might be in memory or on disk, we don't want to know.
            {
                _lookupSuccessCounter++;
                return lookupValue;
            }

            // invoked by the mapping.

            // How do we find existing rows?
            //select JSON_VALUE(ExtJSON, '$.INTERNALPATIENTID')  internalpatientid from phi.patient
            // where internalpatientid = :internalpatientid


            Log.Write(LogLevel.LInfo, "Dependent foreign key insert on table " +
                                   firebirdTableName + " " +
                                   field.InField + " = " +
                                   StrHelpers.NameOfObjectStr(incomingValue));

            var mapping = GetOrCreateMapping(firebirdTableName);

            if (mapping.IsFaulted())
            {
                throw new DataConversionFailureException("Mapping JSON configuration section for firebird table "+firebirdTableName+" is missing or invalid. " + lookupKey );

            }
            /* Find a particular Issuer by issuer name, or something like that
               which may already have data in firebird, and move that data row over.
               Due to our recursive cleverness, this could reach in and grab other
               entities that are required to move this data over.
            */

            /* singleValue = the values we want to insert when creating a new pro-forma object to match SQL relational foreign key requirements*/
            var single = new SingleDefaultValue();

            single.Value = incomingValue; // This might be a FK value ONLY VALID IN FIREBIRD!

            if (!field.ForeignIdentity)
            {
                if (incomingValue != null)
                {
                    single.DefaultValue = incomingValue;
                }
                else
                {
                    single.DefaultValue = field.SelectDefaultValue;
                }
            }


            // "SELECT * FROM USERLIST WHERE <single.ValueField> = @SINGLEVALUE"
            if (field.InField[0] == '@')
            {
                single.ValueField = mapping.GetFirebirdIdentityFieldListStr(firebirdTableName);
            }
            else
            {
                single.ValueField = field.InField; // This might have an @ prefix meaning, this is dereferenced.
            }
            single.SelectTable = field.SelectTable;
            // in the case where the above has an @ prefix, this value is required.


            single.IdentityFlag = field.ForeignIdentity;

            /* Each SQL writer is used with using syntax because it gets a fresh MS SQL connection,
               and must be disposed to avoid resource leaks.

               We also can't share connections because we are in the middle of doing various read operations,
               we can't reuse the connection for a write operation, according to ADO.Net connection sharing
               rules.
            */

            //using (var dataAccessLayer = _dalFactory.CreateDataAccessLayer())
            var dataAccessLayer = _dalFactory.CreateDataAccessLayer();
            try
            {
                //  new ExportWriter or a subclass thereof.
                var writer = _dalFactory.CreateExportWriter(
                    this,
                    dataAccessLayer,
                    mapping,
                    /*_cfgMaxRowCount*/1,
                    single
                    );


                // TODO: Make writer NOT generate duplicate rows!
                // TODO: Make writer generate ETL-history data.
                int modifiedDataCount = writer.Execute();


                if (modifiedDataCount <= 0)
                {
                    // Insert row anyways.
                    Log.Write(LogLevel.LInfo,"Dependent insert  "+ lookupKey + " could not find any data in firebird, using DEFAULT values only.");
                    _lookupDefaultsCounter++;
                    writer.WriteRowWithDefaults();
                }
                ;

                // add to lookup dictionary.
                _lookups.Add(lookupKey,writer.LastIdentity);

                return writer.LastIdentity;
            }
            finally
            {
                _dalFactory.FinalizeDataAccessLayer(dataAccessLayer);

            }
        }

        public void SetContextRow(int rowNumber)
        {
            ContextRow = rowNumber;
        }

        // Validation Checkpoint (STUB) methods. If something is bad, throw an exception or log something.

        public void ChecksBeforeExecuteOneRow(int paramCount)
            // validate that parameter count and firebird field count are acceptable.
        {
            if (_cancel)
            {
                throw new DataConversionFailureException("User Cancelled");
            }
            //System.Threading.Thread.Sleep(5000); // test feature.


            //Debug.Assert(paramCount > 0);
            //Debug.Assert(firebirdFieldCount > 0);
        }


        public void ChecksAfterParameterSetValue(object context, CopyType type, int paramIndex,
            IField field, int indexIncoming, object value)
        {
            // future: do something.

        }


        public CheckBeforeInsertAction ChecksBeforeInsertIdentity(object context, IMapping mapping,
            IDataAccessLayer dal)
        {
            // These hard coded tests should be replaced by some flexible rules that we insert at the conversion layer.

            CheckAssert(context != null, "Insertion of identity cannot proceed without a context");
            CheckAssert(dal != null, "Insertion of identity cannot proceed without a data access layer");

            if (EnableDuplicateChecks && dal.IdentityAlreadyExists(mapping))
                return CheckBeforeInsertAction.Skip;


            //var writer = context as ExportWriter;
            //mapping = dal.GetMapping();
            //var tableName = mapping.FirebirdTableName;
            // debug helper code
            //if (tableName == "ROLELIST")
            // {
            //   CheckAssert(paramCount >= 2, "Role insert requires 2 or more fields");
            // }

            /* todo: write some checks in a subclass of this writer.*/
            return CheckBeforeInsertAction.Continue;

        }


        public void ChecksAfterInsertIdentity(object context, IMapping mapping,
            IDataAccessLayer dal, object newIdentity)
        {
            /* todo: write some checks in a subclass of this writer.*/

            if (SampleOutputCounter > 0)
            {
                string sql = dal.GetLastInsertSql(0);
                SetSampleOutput(0, sql);
            }
        }


        public string GetFailedSql()
        {
            return _lastFailedSqlStatement;
        }

        public void Cancel()
        {
            _cancel = true;
        }

        public List<string> Tables { get; set; }

        public event EventHandler<StatusEventArgs> StatusEvent; // Fired many times. Single line status text.

        public event EventHandler<StatusEventArgs> SampleOutputEvent;
        // Fired at end of conversion, contains a sample of the output.

        public void SetSampleOutputCounter(int acount)
        {
            SampleOutputCounter = acount;
        }

        /// <summary>
        ///    IConversion.GetDependenciesFor returns list of tables that should be converted before the main table provided,
        ///    If an in-order transformation starting with lower database tables, then higher tables, is required.
        /// </summary>
        public List<string> GetDependenciesFor(string tableName)
        {
            try
            {
                var dict = _mappingDictionary[tableName];
                if (dict == null)
                    throw new DataConversionFailureException("No dependencies data available for " + tableName);

                var alist = dict["depends"]?.ToObject<List<string>>();
                if (alist == null)
                    return new List<string>(); // empty list.

                    //throw new DataConversionFailureException("No dependencies data available for " + tableName);

                // it's okay to say that this list is empty.
                return alist;
            }
            catch (Newtonsoft.Json.JsonSerializationException ex)
            {
                // expected "depends": [ "a","b","c" ],
                // actual "depends": "notalist",
                // result: JSON serialization exception.
                // converted to  DataConversionFailureException
                throw new DataConversionFailureException("dependency data invalid for " + tableName, ex);

            }
        }

        /// <summary>
        ///     IConversion.DoConvert is the main execution loop of Conversion moves rows from Firebird to SQL Server.
        /// </summary>
        /// <param name="tableName"></param>
        public int DoConvert(string tableName)
        {
            // _context and _contextRow are used so that SetStatus values can have a context.
            // the person receiving the message needs some idea "where this message came from".
            // in this case, what table we're converting and what our writer's read-counter is
            // gives us a bit of a location.
            Context = tableName;
            ContextRow = 0;

            SetStatus(0, "Please wait...");


            if (tableName == "*")
            {
                throw new DataConversionFailureException("Wildcard conversion tool feature not designed yet");
            }

            // here's our key: Transform the rows to documents, with a prefix, and a primary key value.
            // The prefix is how we know the TYPE of the document.

            // DataTableToSparseDictionary -> Do not include nulls.
            // DataTableToDictionary -> Include nulls.
            // DataTableToSparseDictionaryUID -> Do not include nulls, generate UID
            //var dict = DataTableToSparseDictionaryUID(StudyAccess, "RAMSOFT.AUDIT.STUDYACCESS.",  "ENTRYID");

            var sw = new Stopwatch();
            sw.Start();


            var mapping = GetOrCreateMapping(tableName);

            var modifiedDataCount = 0;

            // instead of calling new DataAccessLayer(GetFirebirdConnection(), GetMssqlConnection())
            // directly, we put the construction into the factory. However the factory had to return
            // a concrete type so that we can get IDisposable+Using statement to work properly, so
            // we stopped using a concrete type, and switched to try..finally, and disposal is a dalfactory
            // concern now.
            //
            //using (var dataAccessLayer = _dalFactory.CreateDataAccessLayer()) <-- limits us so we can't mock or stub pr abstract the data access layer as a pure interface
            var dataAccessLayer = _dalFactory.CreateDataAccessLayer();
            try
            {
                if (dataAccessLayer == null)
                    throw new DataConversionNoOperationException("Data access layer factory failed to create dal");

                //  new ExportWriter or subclass thereof
                var writer = _dalFactory.CreateExportWriter( /*owner=*/this,
                    dataAccessLayer,
                    mapping,
                    _cfgMaxRowCount);

                if (writer == null)
                    throw new DataConversionNoOperationException("Data access layer factory failed to create writer");


                SetStatus(0, "data transferring for firebird table " + mapping.FirebirdTableName);
                // TODO: Make writer NOT ever generate duplicate rows!  (We have some of this done, but there is more that we could do, to guarantee this by design.)
                // TODO: Make writer generate ETL-history data.

                try
                {

                    modifiedDataCount = writer.Execute(); // during this loop a callback will increment _contextRow.
                }
                catch (Exception)
                {
                    _lastFailedSqlStatement = dataAccessLayer.GetFailedSql();
                    //TODO: dataAccessLayer.GetFailedSql() is not always returning the failed SQL!

                    if (string.IsNullOrEmpty(_lastFailedSqlStatement))
                    {
                        Log.Write(LogLevel.LError, $"-- failure in DoConvert({tableName}), no query available.");
                    }
                    else
                    {
                        Log.Write(LogLevel.LError, $"-- failure in DoConvert({tableName}), for query ");
                        Log.Write(LogLevel.LError, _lastFailedSqlStatement);
                    }

                    throw;
                }

                sw.Stop();

                if (modifiedDataCount <= 0)
                {
                    throw new DataConversionNoOperationException("No data has been transferred from Firebird to MS SQL");
                }

                SetStatus(0, "data row transfer count: " + modifiedDataCount +
                             " elapsed:" + sw.ElapsedMilliseconds);
            }
            finally
            {
                _dalFactory.FinalizeDataAccessLayer(dataAccessLayer); // using try..finally because we want to use an interface IDataAccessLayer here and a type can't be IDisposable and IDataAccessLayer.

            }

            return modifiedDataCount;


        }

        protected string GetContext()
        {
            return Context + '.' + ContextRow;
        }

        private void SetStatus(int errorCode, string statusMessage)
        {
            if (StatusEvent != null)
            {
                var args = new StatusEventArgs(errorCode, statusMessage, GetContext());
                StatusEvent(this, args);
            }
        }

        private void SetSampleOutput(int errorCode, string output)
        {
            if (SampleOutputCounter > 0)
            {
                SampleOutputCounter--;
                if (SampleOutputEvent != null)
                {
                    var args = new StatusEventArgs(errorCode, output, GetContext());
                    SampleOutputEvent(this, args);
                }
            }
        }


        private int ExecuteCommand(FbConnection connection, FbTransaction transaction, string commandSql)
        {
            return _dalFactory.ExecuteCommand(connection, transaction, commandSql);
        }

        private DbDataReader ExecuteQuery(FbConnection connection, FbTransaction transaction, string querySql)
        {
            return _dalFactory.ExecuteQuery(connection, transaction, querySql);
        }




        // we want json fields to be in lowercase.
        // we want the primary key from firebird to become _fbidentity in the converted record.
        private string Normalize(string value, string id)
        {
            if (value.ToUpper() == id.ToUpper())
                return "_fbidentity";
            return value.ToLower();
        }


        // Helper used when converting DataRow to a Dictionary, which we then can convert to JSON, used if you want to include nulls.
        private JsonTableDictionary DataTableToDictionary(DataTable dt, string prefix, string id)
        {
            var cols = dt.Columns.Cast<DataColumn>().Where(c => c.ColumnName != id);
            return dt.Rows.Cast<DataRow>()
                .ToDictionary(r => prefix + r[id].ToString(),
                    r => cols.ToDictionary(c => Normalize(c.ColumnName, id), c => r[c.ColumnName]));
        }


        // Helper used when converting DataRow to a Dictionary, which we then can convert to JSON, used if you want to exclude nulls.
        private JsonTableDictionary DataTableToSparseDictionary(DataTable dt, string prefix, string id)
        {
            var cols = dt.Columns.Cast<DataColumn>(); // .Where(c => c.ColumnName != id);
            return dt.Rows.Cast<DataRow>()
                .ToDictionary(r => prefix + r[id].ToString(),
                    r => cols.Where(c => !Convert.IsDBNull(r[c.ColumnName])).ToDictionary
                        (
                            c => Normalize(c.ColumnName, id), c => r[c.ColumnName]
                        )
                );
        }


        private string GenerateUid()
        {
            var id = Guid.NewGuid();
            return id.ToString().Replace("-", "");
        }

        // DataTableToSparseDictionaryUID : Dictionary Key is a UID.
        private JsonTableDictionary DataTableToSparseDictionaryUid(DataTable dt, string prefix, string id)
        {
            var cols = dt.Columns.Cast<DataColumn>(); // .Where(c => c.ColumnName != id);
            return dt.Rows.Cast<DataRow>()
                .ToDictionary(r => prefix + GenerateUid(),
                    r => cols.Where(c => !Convert.IsDBNull(r[c.ColumnName])).ToDictionary
                        (
                            c => Normalize(c.ColumnName, id), c => r[c.ColumnName]
                        )
                );
        }


        /// <summary>
        ///     Create if we haven't got an entity in memory for a given table name, or else return existing mapping.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public ExportMapping GetOrCreateMapping(string tableName)
        {
            try
            {
                var mapping = _mappings[tableName]; // makes it fast to keep a library of these around.
                return mapping;
            }
            catch (KeyNotFoundException e)
            {
                Log.Write(LogLevel.LFine, "KeyNotFoundException " + tableName + " / " + e.Message);

                // Assumption: _MappingDictionary["EMPLOYER"] exists and contains enough data to create a mapping.
                // If it doesn't, this class is expected to blow up with a friendly message in an exception at some
                // point, or else the writer  is expected to blow up while executing this mapping, with some idea
                // for the user about how to fix the mapping json file.
                var mapping = new ExportMapping(tableName, _mappingDictionary);
                _mappings[tableName] = mapping;
                return mapping;
            }
        }

        protected void CheckAssert(bool cond, string amessage)
        {
            if (!cond)
                throw new DataConversionFailureException("Check failed: " + amessage);
        }
    }
}