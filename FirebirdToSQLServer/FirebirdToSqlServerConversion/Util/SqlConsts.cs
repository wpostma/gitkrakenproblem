namespace RamSoft.FirebirdToSqlServer.Util
{
    /// <summary>
    /// These are Error Code values from MS SQL exceptions, found when unpacking SqlException.Errors
    /// </summary>
    public class SqlConsts
    {
        /*
          select  error,description
            from master.dbo.sysmessages
            WHERE (error> N1)AND(error < N2)
            AND msglangid=1033  -- 1033 = en/US locale

            */
        public const int InvalidColumnName = 207;
        public const int ConversionError = 245;

        public const int NullCheckViolation = 515; // not null field cannot have null.
        public const int ImplicitConversionTypeViolation = 518; // try to assign a varchar into a bit field or something like that.
        public const int ExplicitConversionTypeViolation = 529; // try to cast something to something and it can't be done.
        public const int InsertIdentityNotAllowed = 544; // something is wrong, boss.
        public const int ConstraintViolation = 547; // constraint like uniqueness or foreign key violated
        public const int UniqueIndexValueConstraintViolation = 2601;
        public const int PrimaryKeyConstraintViolation = 2627;
        public const int ParamValueMissing = 8178;
    }
}