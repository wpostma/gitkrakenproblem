﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using FirebirdToSQLServerTests.Fixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Exceptions;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;

namespace RamSoft.FirebirdToSqlServer
{
   

    [TestClass]
    public class ConversionTests
    {
        private const int MaxRowCountForUnitConversionTest = 99999999;
        protected Conversion Conv; // Test item.
        public FakeDataAccessLayer FakeDal = new FakeDataAccessLayer();


        //public Mock<IDataAccessLayerFactory> MockDalf = new Mock<IDataAccessLayerFactory>();  // supports IDataAccessLayerFactory
        //public Mock<IDataAccessLayer> MockDal = new Mock<IDataAccessLayer>();// can't test against IDataAccessLayer as DataAccessLayer is IDisposable.

        public FakeDataAccessLayerFactory FakeDalf = new FakeDataAccessLayerFactory();

        // Data Access Layer Requires Some Plumbing, like Firebird and SQL Data readers.
        public Mock<IDataReader> MockFbReader = new Mock<IDataReader>(); // mock IDataReader
        public Mock<IColumnInfo> MockSqlColumnInfo = new Mock<IColumnInfo>(); //  mock IColumnInfo

        // Items under test:
        protected ExportWriter Writer = null;


        protected void ListCheck(List<string> aexpected, List<string> aoutput, string msg)
        {
            Assert.IsTrue(aoutput != null, msg);
            Assert.IsTrue(aexpected != null, msg);
            Assert.AreEqual(aexpected.Count, aoutput.Count, msg);

            Assert.IsTrue(aoutput.SequenceEqual(aexpected), msg);
        }

        // nUnit [SetUp] -> MSTEST [SetupInitialize]
        [TestInitialize]
        public void SetUp()
        {
            // firebird column info reader with a few fields,  FACILITYNAME, ISSUEROFPATIENTID
            MockFbReader.Setup(t => t.FieldCount).Returns(3);
            MockFbReader.Setup(t => t.GetName(0)).Returns("INTERNALPATIENTID");
            MockFbReader.Setup(t => t.GetFieldType(0)).Returns(typeof (int));
            MockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("int");

            MockFbReader.Setup(t => t.GetName(1)).Returns("ADDRESS");
            MockFbReader.Setup(t => t.GetFieldType(1)).Returns(typeof (string));
            MockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("varchar");

            MockFbReader.Setup(t => t.GetName(3)).Returns("LOOKUP");
            MockFbReader.Setup(t => t.GetFieldType(3)).Returns(typeof (string));
            MockFbReader.Setup(t => t.GetDataTypeName(3)).Returns("varchar");


            MockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(4);

            MockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("InternalPatientId"); // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("int");

            MockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("Address");
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("nvarchar");

            MockSqlColumnInfo.Setup(t => t.GetName(2)).Returns("LastUpdateUserID");
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("int");

            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            MockSqlColumnInfo.Setup(t => t.GetName(3)).Returns("ExtJSON"); // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(3)).Returns("nvarchar");


            //var dalf = new DataAccessLayerFactory(fbConnStr, msSqlConnStr); // would require real dbs.
        }

        public IDataAccessLayerFactory GetDalf()
        {
            //return  MockDalf.Object;
            return FakeDalf;
        }

        public ILookups GetLookups()
        {
            return new Lookups();
        }

        private Conversion GetFirebirdToSqlServerConversion()
        {
            return new Conversion(
                "..\\..\\InvalidMapping.json",
                GetDalf(),
                GetLookups(),
                MaxRowCountForUnitConversionTest);
        }

        // nUnit [TearDown] -> MSTEST [TestCleanup]

        [TestCleanup]
        public void TearDown()
        {
        }


        [TestMethod]
        [TestCategory("Convert")]
        public void ConvertGetDependenciesForTableIsListOfStrings()
        {
            var conv = GetFirebirdToSqlServerConversion();

            var list = conv.GetDependenciesFor("APPOINTMENTS");
            var expected = new List<string> {"FACILITIES", "MODALITIES", "RESOURCES", "PATIENTSTUDY"};

            ListCheck(expected, list, "Wrong results for Conversion.GetDepenciesFor(tableName)");
        }


        [TestMethod]
        [TestCategory("Convert.Fail")]
        [ExpectedException(typeof (DataConversionFailureException))]
        public void ConvertGetDependenciesForNonExistantTableThrowsException()
        {
            var conv = GetFirebirdToSqlServerConversion();

            var list = conv.GetDependenciesFor("DOESNOTEXIST");
        }


        [TestMethod]
        [TestCategory("Convert.Fail")]
        [ExpectedException(typeof (DataConversionFailureException))]
        public void ConvertGetDependenciesForExistingInvalidTableThrowsException1()
        {
            var conv = GetFirebirdToSqlServerConversion();

            /* var list = */
            conv.GetDependenciesFor("INVALIDMAPPING1");
        }

        [TestMethod]
        [TestCategory("Convert.Fail")]
        [ExpectedException(typeof (DataConversionFailureException))]
        public void ConvertGetDependenciesForExistingInvalidTableThrowsException2()
        {
            var conv = GetFirebirdToSqlServerConversion();

            conv.GetDependenciesFor("INVALIDMAPPING2");
        }


        [TestMethod]
        [TestCategory("Convert")]
        public void ConvertGetDependenciesForEmptyTableDictIsEmptyList()
        {
            // read EMPTYTABLEDICT success check.
            var conv = GetFirebirdToSqlServerConversion();
            var list = conv.GetDependenciesFor("EMPTYTABLEDICT");
            Assert.IsTrue(list.Count == 0, "Expect empty list return for EMPTYTABLEDICT");
        }


        [TestMethod]
        [TestCategory("Convert.Fail")]
        [ExpectedException(typeof (MappingFailureException))]
        public void ConvertDoConvertFailureForEmptyTableDictThrowsMappingFailure()
        {
            // read EMPTYTABLEDICT should through mapping failure exception because key and oldkey are missing.
            var conv = GetFirebirdToSqlServerConversion();
            conv.DoConvert("EMPTYTABLEDICT");
        }

        [TestMethod]
        [TestCategory("Convert")]
        [ExpectedException(typeof (DataConversionFailureException))]
        public void ConvertInsertRowAndFetchIdentityShouldFail()
        {
            // check that we throw DataConversionFailureException
            // with message Mapping JSON configuration section for firebird table BADTABLENAME is missing or invalid.
            var conv = GetFirebirdToSqlServerConversion();
            var x = 3;
            var map = conv.GetOrCreateMapping("CACHEDISSUEROFPATIENTIDS");
                // our input file is called invalid, but this particular map is valid.
            map.MapFields(MockFbReader.Object, MockSqlColumnInfo.Object, null);
            //map.BuildStatement(StatementType.Update);
            Assert.IsTrue(map.GetAllFieldsCount() > 0, "test requires a valid table mapping to exist for this test case");
            conv.InsertRowAndFetchIdentity("BADTABLENAME", x, map.GetAllField(0));
        }
    }
}