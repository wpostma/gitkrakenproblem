using System;
using System.Collections.Generic;
using System.Data.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RamSoft.FirebirdToSqlServer.Exceptions;

namespace RamSoft.FirebirdToSqlServer.Util
{
    public class JsonHelpers
    {


        /// <summary>
        /// Load a JSON file from disk and return as a JObject, assuming that the outer JSON syntax is JSON dictionary-like entity.
        /// </summary>
        public static JObject LoadJson(string filename)
        {
            using (System.IO.StreamReader file = System.IO.File.OpenText(filename))
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("read JSON file "+ filename+" length "+file.BaseStream.Length);
#endif
                try
                {

                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        return (JObject) JToken.ReadFrom(reader);
                    }
                }
                catch (System.InvalidCastException)
                {

                    throw new DataConversionFailureException(
                        "Check that JSON file exists and that the top level is in dictionary { \"a\":\"b\", \"c\": \"d\" } syntax");
                }

            }
        }

        /// <summary>
        /// Check type of JValue is Integer
        /// </summary>
        /// <param name="avalue">A single json value</param>
        /// <returns></returns>
        static public bool IntegerLikeType(JValue avalue)
        {
            return avalue.Type == JTokenType.Integer;
        }

        /// <summary>
        /// Check type of JValue is String
        /// </summary>
        /// <param name="avalue">A single json value</param>
        /// <returns></returns>
        static public bool StringLikeType(JValue avalue)
        {
            return avalue.Type == JTokenType.String;
        }

        /// <summary>
        ///  Serialize a DataReader's current row as a dictionary, useful when we want to then convert the dictionary to JSON. Provide list of columns.
        /// </summary>
        /// <param name="cols">which columns do we want</param>
        /// <param name="reader">which dataset reader do we want to grab data from</param>
        /// <returns>Dictionary</returns>
        static public Dictionary<string, object> SerializeRowPartial(IEnumerable<string> cols, DbDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                var value = reader[col];
                if ((value != null) && (value != DBNull.Value))
                {
                    result.Add(col, value);
                }
            }
            return result;
        }

        /// <summary>
        ///  Serialize a DataReader's current row as a dictionary, useful when we want to then convert the dictionary to JSON, automatically uses all columns.
        /// </summary>
        /// <param name="cols">which columns do we want</param>
        /// <param name="reader">which dataset reader do we want to grab data from</param>
        /// <returns>Dictionary</returns>

        static public Dictionary<string, object> SerializeRowFull(DbDataReader reader)
        {
            var cols = new List<string>();
            for (var i = 0; i < reader.FieldCount; i++)
                cols.Add(reader.GetName(i));

            return SerializeRowPartial(cols, reader);
        }



    }
}