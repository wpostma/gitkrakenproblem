﻿using System;

// RamSoft Firebird to SQL Server - Data Access Layer classes
//----------------------------------------------------------------------
//
//

namespace RamSoft.FirebirdToSqlServer.Model
{
    /*
      Concrete production (real) data access layer. Integration testable,
      but since the only trait worth testing is that this layer reads a physical
      db and writes a physical db, unit testing this layer is pointless, so it
      is integration testable instead.
    */


    [Serializable]
    public class DataAccessLayerException : ApplicationException
    {
        public DataAccessLayerException()
        {
        }

        public DataAccessLayerException(string message, Exception originalException) : base(message, originalException)
        {
            //
        }
    }

    // class
} // ns