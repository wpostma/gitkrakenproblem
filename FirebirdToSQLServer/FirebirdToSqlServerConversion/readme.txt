This utility is to convert SQL data from Firebird Database to Microsoft SQL 2016 schema, and is
specifically designed for the requirements of RamSoft inc and our Radiology RIS/PACS database
schemas.

It is recommended to make an MS SQL database backup so you can revert to before that backup and re-run this
tool as many times as you need to find and fix problems with the mapping.json.

TODO: Support command line parameters
  /BATCH - don't prompt
  /PROGRESS - show progress bar only but hide the user interface (GUI).
  /INTERACTIVE - interactive mode with lots of prompts and options
  /FBCONNECTION - specify Firebird connection
  /SQLCONNECTION - specify MS SQL connection string
  /TABLE - specify table or tables to convert
  /TASK - specify some other task (transform/duplicate data during import)
  /NOWRITE - just report on what data would be written, don't actually do the write.
  /MAPPING - specify path and filename for the mapping configuration
  /LOG - write to log file.

  Notes on Data Duplication
  --------------------------

  * If this tool is to be useful in production some day, it has to be capable of detecting and preventing duplicates.

  * If this tool is to be maximally useful in development, it is better if it is capable of detecting duplicates, but also
  capable of intentionally creating duplicates, sometimes with changes.  For instance, suppose we have a Scheduler data-set
  from a customer.  We could anonymize patient names but copy their data including appointments, multiple times, with time
  shifting. Then we can create populations of test data that mimic real data, and be able to test a system under arbitrary
  dataset sizes.   Say we want to test a 50-modality 50-room mega-clinic, and populate a two week calendar with 50 rooms worth
  of data.  Using a 10-room 10-modality incoming set of data, imported cleverly five times into a set of different resources
  and people, we would have a good simulation of a real world clinic with 50 modalities (say 25 Digital X-Ray machines,
   10 CT machines, and so on).




TODO: Issuer -> AssigningAuthority


  "CACHEDISSUEROFPATIENTIDS": {
    "destination": "[dbo].[AssigningAuthority]",
    "destintidentity": 1,
    "identity": "ISSUEROFPATIENTID",
    "oldkey": [ "varchar", "ISSUEROFPATIENTID" ],
    "newkey": [ "int", "InternalAssigningAuthorityID" ],

    "fields": [
      [ "ISSUEROFPATIENTID", "Issuer" ],
      [ 1, "[IsActive]" ],
      [ "*", "[ExtJSON]" ]
    ]
  },

  "CACHEDISSUEROFPATIENTIDS": {
    "destination": "[dbo].[AssigningAuthority]",
    "destintidentity": 1,
    "identity": "ISSUEROFPATIENTID",
    "oldkey": [ "varchar", "ISSUEROFPATIENTID" ],
    "newkey": [ "int", "InternalAssigningAuthorityID" ],

    "fields": [
      [ "ISSUEROFPATIENTID", "Name" ],
      [ 1, "[IsActive]" ],
      [ "*", "[ExtJSON]" ]
    ]
  },


    "APPOINTMENTS": {
    "destination": "[dbo].[ReservedTime]",
    "destintidentity": 1,
    "query": [
      "select A.appointmentid, A.starttime claimed, cast(A.starttime as date) startdate, ",
      "  cast(A.finishtime as date) enddate, cast(A.starttime as time) starttime, ",
      "  cast(A.finishtime as time) endtime,   A.RESOURCEID,    A.INTERNALSTUDYID, ",
      "  A.APPOINTMENTSTATE,   A.LASTUPDATEUSERID,  ",
      "  case BLOCKEDREASON when '' then ",
      "  COALESCE (P.FAMILYNAME || ' ' || P.GIVENNAME || ' : ' || S.STUDYDESCRIPTION,'UNKNOWN')  ",
      "else      BLOCKEDREASON   end DISPLAY ",
      "from appointments A ",
      "left join patientstudy S on A.INTERNALSTUDYId=S.internalstudyid ",
      "left join patientmodule P on P.INTERNALPATIENTID=S.INTERNALPATIENTID "
    ],
    "depends": [ "FACILITIES", "MODALITIES", "RESOURCES", "PATIENTSTUDY" ],
    "key": [ "int", "APPOINTMENTID", "[InternalReservedTimeId]" ],
    "fields": [
      [ "INTERNALSTUDYID", "InternalStudyId", "[phi].[Study]", "[InternalStudyId]" ],
      [ "claimed", "[Claimed]" ],
      [ "DISPLAY", "[Display]" ],
      [ 0, "[IsAppointmentAllowed]" ],
      [ 1, "[IsActive]" ],
      [ "*", "[ExtJSON]" ],
      [ "RESOURCEID", "^[dbo].[ReservedTimeResource].[InternalResourceId]" ]
    ]
  },


  "PATIENTSTUDY": {
    "destination": "[phi].[Study]",
    "destintidentity": 1,
    "identity": "STUDYINSTANCEUID",
    "depends": [ "CACHEDISSUEROFPATIENTIDS", "STUDYSTATUSVALUES", "FACILITIES", "MODALITIES", "RESOURCES", "PATIENTMODULE" ],
    "key": [ "int", "INTERNALSTUDYID", "[InternalStudyId]" ],
    "existing": "select [InternalStudyId] from [phi].[Study] WHERE [StudyUID] = @StudyUID",
    "fields": [
      [ "STUDYINSTANCEUID", "StudyUID" ],

      [ "STUDYID", "StudyId" ],
      [ "STUDYDESCRIPTION", "Description" ],
      [ "REQPROCEDUREID", "RequestedProcedureID" ],
      [ "INTERNALSTUDYID", "ForeignDBStudyID" ],
      [ "PRIORITY", "PRIORITYVALUE" ],
      [ "HL7UPDATED", "IsHL7Updated" ],
      [ "STATUSORDER", "StatusValue" ],
      [ "STUDYTIMESTAMP", "LastUpdateUTC" ],
      [ "STUDYDATETIME", "StudyDateTimeUTC" ],
      [ "+STUDYDATETIME", "StartDateTime" ],
      [ "SCHEDULEDMODALITY", "ModalityCode" ],
      [ "*", "ExtJSON" ],


      [ "+", "[InternalImagingServiceRequestId]", "[phi].[ImagingServiceRequest]" ],
      [ "INTERNALPATIENTID", "^[phi].[ImagingServiceRequest].[InternalPatientId]", "[phi].[Patient]", "[InternalPatientID]", "int" ],
      [ "ACCESSIONNUMBER", "^[phi].[ImagingServiceRequest].[AccessionNumber]" ]

    ]
  },