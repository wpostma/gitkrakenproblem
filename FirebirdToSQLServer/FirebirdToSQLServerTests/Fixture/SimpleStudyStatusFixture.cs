namespace FirebirdToSQLServerTests.Fixture
{
    internal class SimpleStudyStatusFixture : BaseFixture
    {
        // The key entity is an empty list in this case. This is a special syntax case that means that the first field is the key map,
        // and the first field values are copied without any translation.  So STATUSORDER int 100 on source becomes StatusValue int 100 on destination.
        public const string StudyStatus1 =
            @"{

                      ""STUDYSTATUSVALUES"": {
                          ""destination"": ""[code].[Status]"",
                          ""key"": [  ],
                          ""fields"": [
                            [ ""STATUSORDER"",""[StatusValue]""],
                            [ ""STATUS"", ""[Status]"" ],
                            [ 0, ""[Favorite]"" ],
                            [ ""*"", ""[ExtJSON]"" ]
                          ]

                        },
                    }";

        // a procedurally generated lookup string that lets us find the facility name in firebird
        // from its internalfacilityid internal PK.
        public const string FirebirdFacilityIdentityLookupSql1 =
            "SELECT FIRST 1 FACILITYNAME FROM FACILITIES WHERE INTERNALFACILITYID = @VALUE";


        public SimpleStudyStatusFixture() : base(StudyStatus1, "STUDYSTATUSVALUES")
        {
            // firebird column info reader with a few fields,  FACILITYNAME, ISSUEROFPATIENTID
            MockFbReader.Setup(t => t.FieldCount).Returns(2);
            MockFbReader.Setup(t => t.GetName(0)).Returns("STATUSORDER"); // PK
            MockFbReader.Setup(t => t.GetName(1)).Returns("STATUS");
            MockFbReader.Setup(t => t.GetFieldType(0)).Returns(typeof (int));
            MockFbReader.Setup(t => t.GetFieldType(1)).Returns(typeof (string));
            MockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("int");
            MockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("varchar");


            MockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(3);
            MockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("StatusValue"); // PK
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("int");
            MockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("Status");
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("nvarchar");

            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            MockSqlColumnInfo.Setup(t => t.GetName(2)).Returns("ExtJSON"); // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("nvarchar");
        }
    }
}