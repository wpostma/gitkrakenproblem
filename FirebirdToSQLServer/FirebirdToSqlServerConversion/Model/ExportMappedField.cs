﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using RamSoft.FirebirdToSqlServer.Exceptions;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Util;

//----------------------------------------------------------------------
// See also ExportWriter.cs
//----------------------------------------------------------------------


// ReSharper disable always ConvertPropertyToExpressionBody

namespace RamSoft.FirebirdToSqlServer.Model
{
    ///<summary> RamSoft Firebird to SQL Server Export Mapping Field Model
    ///</summary>
    ///<remarks>
    /// This is used from inside the main level Conversion layer, which is used inside a .net form or a console
    /// runner application.  This data/model object describes the mapping from one firebird table's fields to one or more destination
    /// MS SQL tables and their fields. ExportMapping reads this information from a Mapping.json file.
    ///
    /// var mapping = new ExportMapping("TABLENAME");
    ///
    /// Note: This class must avoid depending on concrete clients, we should not see the following here
    ///  using FirebirdSql.Data.FirebirdClient;
    ///  using System.Data.SqlClient;
    ///</remarks>
    public class ExportMappedField : IField
    {
        private string _outfield;

        private List<IField> _subFields;

        /// <summary>
        /// what item index in the unfiltered column list is this.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// what item in the incoming firebird select * query is this.
        /// </summary>
        public int QueryIndex { get; set; }

        /// <summary>
        /// What is the name of the field in the incoming firebird database
        /// </summary>
        public string InField { get; set; }

        /// <summary>
        /// What is the column type for the field in the incoming firebird database
        /// </summary>
        public string InDbType { get; set; }


        /// <summary>
        /// What is the column name for the field in the outgoing MS SQL database
        /// </summary>
        public string OutField {
            get
            {
                return _outfield;
            }
            set
            {
                _outfield = value;
#if DEBUG
                if (value == "[Favorite]")
                    throw new MappingFailureException("[Favorite] not allowed");
#endif

            }
        }

        /// <summary>
        /// Mostly zero. In special cases, where sub field insertions are happening, how many subfields are beneath this field in the mapping info
        /// </summary>
        public int GetSubFieldCount()
        {
            if (_subFields != null)
            {
                return _subFields.Count;
            }
            else
            {
                return 0;
            }
        }

        ///<summary>INSERT INTO table (col1,col2) VALUES (@col1,@col2) for a subfield insertion. See Mapping.json PATIENTSTUDY for example.</summary>
        public string SubFieldInsertSQL { get; set; }

        /// <summary>
        /// Get previously generated sub-field-INSERT sql statement if it exists, otherwise nothing. This is not the main insert statement.
        /// </summary>
        /// <returns>INSERT statement or nothing</returns>
        public string GetSubFieldInsertSQL()
        {
            return SubFieldInsertSQL;
        }

        public void SetSubFieldInsertSQL(string value)
        {
            SubFieldInsertSQL = value;
        }


        public void SubFieldsClear()
        {
            // lazy create this infrequently used sub-field map.
            _subFields?.Clear();

        }

        public IField GetSubField(int index)
        {
            return _subFields?[index];
        }


        public void SubFieldAdd(IField field)
        {
            // lazy create this infrequently used subField list
            if (_subFields == null)
            {
                _subFields = new List<IField>();
            }
            _subFields.Add(field);
        }


        public string OutDbType { get; set; }

        /*public string OutDbType
        {
            get { return OutDbType; }
            set
            {
                if (value == null)
                    throw new MappingFailureException("OutDbType cannot be null");
                if (value.Contains(":"))
                    throw new MappingFailureException("OutDbType cannot contain colon");
                OutDbType = value;
            }
        }*/

        public bool InsertOutField { get; set; }
        // only true on fields that are not auto-generated, for example IDENTITY(1,1) fields.

        public bool UpdateSetField { get; set; }

        public bool NotNullFlag { get; set; }
        // field declared as "|[Foo]" means it can't be null, but perhaps can be empty string instead.

        public bool MasterDetailFlag { get; set; } // ^[dbo].[SecondaryTable].[Field] syntax flag.

        public bool SecondaryFlag { get; set; } // secondary copy "+FIELDNAME" in the Mapping.json.

        public bool Identity { get; set; } // This is the current primary key and it is set as IDENTITY(1,1)

        public bool ForeignIdentity { get; set; }
        // This is a foreign key linking this row to another entity which is set as IDENTITY(1,1)

        public bool InsertExtJson { get; set; }
        public bool PrimaryKeyFlag { get; set; }


        // optional items
        public string SelectTable { get; set; }
        public string SelectField { get; set; }

        public string GetSelectFieldActual()
        {
            var selField = StrHelpers.AddBrackets(SelectField);
            if (string.IsNullOrEmpty(selField))
            {
                //   throw new DataAccessLayerException(
                //      "FB Lookup Failed. Select Field value is missing from lookup for field" + field.InField +
                //      " lookup in table " + field.SelectTable, null);
                selField = StrHelpers.AddBrackets(OutField);
            }
            return selField;
        }


        public object SelectDefaultValue { get; set; }
        public string SelectFieldType { get; set; }

        // runtime objects (optional, and interface-typed for unit testability)

        public IDbCommand SelectCommand { get; set; } // expected SQL Server SqlCommand
        public IDbCommand SelectFbCommand { get; set; } // expected Firebird FbCommand

        protected string FmtDefault(string v)
        {
            if (string.IsNullOrEmpty(v))
            {
                return "?";
            }
            return v;
        }

        public override string ToString()
        {
            //returns "FIREBIRDFIELD int => SqlServerField smallint"
            // flags are fetched separately as flagStr();
            return FmtDefault(InField) + " " +
                   FmtDefault(InDbType) + " => " +
                   FmtDefault(OutField) + " " +
                   FmtDefault(OutDbType);
        }


        // helper state functions
        public bool SpecialFunctionDetect()
        {
            return !string.IsNullOrEmpty(InField)
                   && ((InField[0] == '#') || (InField[0] == '$') || (InField[0] == '@')
                       );
        }

        public string GetSubFieldNames()
        {
            // empty string or [F1], [F2] string
            if (GetSubFieldCount() == 0)
            {
                return string.Empty;
            }
            else
            {
                var subFieldOutList = from s in _subFields select StrHelpers.AddBrackets(s.OutField);
                var res = string.Join(", ", subFieldOutList);
                return res;
            }

        }

        public string GetSubFieldParamNames()
        {
            // empty string or @F1, @F2
            if (GetSubFieldCount() == 0)
            {
                return string.Empty;
            }
            else
            {
                var subParamNameOutList = from s in _subFields select "@"+s.OutField;
                var res = string.Join(", ", subParamNameOutList);
                return res;
            }
        }

    }
}