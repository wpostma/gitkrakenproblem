﻿
using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Diagnostics;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Exceptions;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Util;

// RamSoft Firebird to SQL Server - Main form.
//----------------------------------------------------------------------
//
// This tool is used to take a Firebird SQL Query and convert result set data into a new
// SQL schema redesigned for Microsoft SQL Server 2016.  This utility is based on the code
// for a similar tool for Firebird-to-Couchbase conversion.
//
// See Conversion.cs for the high level conversion class.
// 
// See readme.txt for notes.
//
// TODO:  Make it configurable.

namespace RamSoft.FirebirdToSqlServer
{
    public partial class MainForm : Form
    {
        private readonly IConversion _conversion;
        private ConversionWorker _worker;

        public void StatusEvent(object sender, StatusEventArgs args)
        {
            BeginInvoke((Action)(() =>
            {
                toolStripStatusLabel1.Text = args.Message;
                statusStrip1.Refresh();
            }));

        }

        public void SampleOutputEvent(object sender, StatusEventArgs args)
        {
            // This could be called by a background thread.  This lambda (closure) is deferred so we don't
            // block the background thread, nor do we want to touch UI control state from a background thread
            // directly .
            BeginInvoke((Action)(() =>
            {
                textBox1.AppendText("\n--------OUTPUT:");
                textBox1.AppendText(args.Message);
                int len = args.Message.Length;
                if ((len > 0) && args.Message[len - 1] != '\n')
                    textBox1.AppendText("\n");
                textBox1.Refresh();
            }));
        }

        protected void Log(string message)
        {
    
            // This could be called by a background thread.  This lambda (closure) is deferred so we don't
            // block the background thread, nor do we want to touch UI control state from a background thread
            // directly .
            BeginInvoke((Action) (() =>
            {
                Debug.WriteLine(message);
                textBox1.AppendText(message);
                int len = message.Length;
                if ( (len > 0) &&  message[len-1] != '\n')
                   textBox1.AppendText("\n");

            }));
            
        }
        //public delegate void logCallback(LogLevel level, string message);
        //protected void LogCallback(LogLevel level, string message)
        
        protected void LogEvent(object sender, LogEventArgs args)
        {
            LogLevel logLevel = args.LogLogLevel;
            string message = args.Message;

            if (logLevel >= LogLevel.LInfo)
            {
                var msg = Util.Log.GetPrefix(logLevel) + ':' + message; 
                Log(msg + "\n");
            }

            if (logLevel == LogLevel.LError)
            {
                TreeAdd(TreeType.Error, message);
            }
            else
            if (logLevel == LogLevel.LWarning)
            {
                
                TreeAdd( TreeType.Warning, message);

            }
            else if (logLevel == LogLevel.LInfo)
            {
                TreeAdd(TreeType.Info , message);

            }

        }

        public string GetUserSelectedFirebirdConnectionStr()
        {
            string userValue = textBoxFB.Text;
            if (!string.IsNullOrEmpty(userValue))
            {
                return string.Format("initial catalog={0};user id=SYSDBA;password=$db@dM1n;data source=localhost",
                    userValue);
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetUserSelectedMicrosoftSqlConnectionStr()
        {
            string userValue = textBoxSQLDB.Text;
            if (!string.IsNullOrEmpty(userValue))
            {
                return string.Format("Data Source=(local);Initial Catalog={0};Integrated Security=True",
                    userValue);
            }
            else
            {
                return string.Empty;
            }
        }

        public MainForm()
        {
            InitializeComponent();
           
            //FirebirdToSqlServer.Log.LogCallback = LogCallback;
            Util.Log.LogEvent += LogEvent;

            // TODO: Make this UI configurable for firebird database source and MS SQL database destination.
            // Make all these come from command line and or put some UI into the application to find an
            // MS SQL server instance or connect to a specific firebird instance, or read 
            // environment variables, or both...
            
            string mappingFileName = "..\\..\\Mapping.json"; // assuming executable is in subdir /Aaa/Bbb/ relative to the json source 
            string fbConnStr;
            string sqlConnStr;

            pictureBox1.Image = Properties.Resources.FirebirdToSQLServerApp;

            fbConnStr = GetUserSelectedFirebirdConnectionStr();
            sqlConnStr = GetUserSelectedMicrosoftSqlConnectionStr();


            if (string.IsNullOrEmpty(fbConnStr))
            {
                // fallback: Use configuration manager when user didn't enter a value.    
                var fcon = ConfigurationManager.ConnectionStrings["FirebirdToSqlServerTools.Properties.Settings.firebird"];
                fbConnStr = fcon.ConnectionString;

                if (string.IsNullOrEmpty((fbConnStr)))
                    throw new ConfigurationErrorsException("Firebird connection information not provided by user or found in App.config");

            }


            if (string.IsNullOrEmpty(sqlConnStr))
            {

                var scon = ConfigurationManager.ConnectionStrings["FirebirdToSqlServerTools.Properties.Settings.mssql"];
                if (scon != null)
                {
                    sqlConnStr = scon.ConnectionString;
                }
                if (string.IsNullOrEmpty((sqlConnStr)))
                    throw new ConfigurationErrorsException("MS SQL connection information not provided by user or found in App.config");
            }
            
            textBoxFB.SetCue("Enter c:\\path\\to\\pacs.fdb");

            textBoxSQLDB.SetCue("Enter the DBNAME on default instance");



            /* Limit number of rows that can be transferred via App.Config setting:
<appSettings>
    <add key="maxrowcount" value="100000000" />
</appSettings>
*/
            int maxrowcount = 0;
            string appCfgStrMaxRowCount = ConfigurationManager.AppSettings["maxrowcount"];
            if (appCfgStrMaxRowCount != null)
            {
                maxrowcount = Int32.Parse(appCfgStrMaxRowCount);
            }
            if (maxrowcount <= 0)
            {
                maxrowcount = Int32.MaxValue;
            }

            var lookups = new Lookups();
            // TODO:load existing entities into lookups?

            IDataAccessLayerFactory dalFactory = new DataAccessLayerFactory(fbConnStr,sqlConnStr);
                
            _conversion = new Conversion(
                                mappingFileName, 
                                dalFactory,
                                lookups,
                                maxrowcount);
            // Conversion contains a writer.

            checkedListBoxTables.Items.Clear();
            foreach (var item in _conversion.Tables)
            {
                checkedListBoxTables.Items.Add(item);
            }


            _conversion.SetSampleOutputCounter(10);

            _conversion.StatusEvent += StatusEvent;
            _conversion.SampleOutputEvent += SampleOutputEvent;

        }

        private void TreeAdd(TreeType type, string message, Exception ex = null)
        {
            BeginInvoke((Action)(() =>
            {
                treeView1.BeginUpdate();
                try
                {
                    var node = treeView1.Nodes.Add(message);
                    switch (type)
                    {
                        case TreeType.Table:
                            node.ImageIndex = 0;
                            break;
                        case TreeType.Error:
                            node.ImageIndex = 1;
                            break;
                        case TreeType.Warning:
                            node.ImageIndex = 2;
                            break;
                        case TreeType.Success:
                            node.ImageIndex = 3;
                            break;
                        case TreeType.Info:
                            node.ImageIndex = 4;
                            break;
                        case TreeType.Failure :
                            node.ImageIndex = 5;
                            break;
                        default:
                            node.ImageIndex = -1;
                            break;
                    }
                    node.SelectedImageIndex = node.ImageIndex;
                    if (ex != null)
                    {
                        var childnode = node.Nodes.Add(ex.Message);
                        childnode.ImageIndex = 5;
                        childnode.SelectedImageIndex = 5;

                        if (ex.InnerException != null)
                        {
                            var childnode2 = childnode.Nodes.Add(ex.InnerException.Message);
                            childnode2.ImageIndex = 5;
                            childnode2.SelectedImageIndex = 5;

                        }
                    }



                }
                finally
                {
                    treeView1.EndUpdate();

                }
                

            })); //end-invoke.

        }

        private void WorkerCompletedEventHandler(object sender, WorkerCompletedArgs e)
        {
            // worker object fires this event in a background thread.
            BeginInvoke((Action)(() =>
            {

                statusStrip1.Refresh();
                button1.Enabled = true;
                button2.Enabled = false;
                Exception ex = e.WorkerException;

                if (ex != null)
                {
                    string sql = _conversion.GetFailedSql();
                    if (!string.IsNullOrEmpty((sql)))
                        Log("SQL:" + sql);
                }

                if (ex is DataConversionNoOperationException)
                {
                    Log("No operation. Source table may be empty  or all data may already be migrated. " + ex.Message);
                    TreeAdd(TreeType.Warning, "No data moved.");
                }
                else if (ex is MappingFailureException)
                {
                    Log("NOTE: Mapping.json file may be incorrect. Please repair and try again.");
                    TreeAdd(TreeType.Failure, "Mapping.json configuration problem", ex);
                }
                else if (ex is FieldLookupException)
                {
                    TreeAdd(TreeType.Failure, "Field Lookup Failure", ex);

                }
                else 
                {
                    if (ex == null)
                    {
                        TreeAdd(TreeType.Success, "SUCCESS. "+e.TotalCount.ToString()+" rows.");
                    }
                    else
                        TreeAdd(TreeType.Failure, "Internal Failure",ex);

                }

            }));//end-invoke.




        }

        private void button1_Click(object sender, EventArgs e)
        {


            treeView1.BeginUpdate();
            treeView1.Nodes.Clear();
            treeView1.EndUpdate();



            toolStripStatusLabel1.Text = "Please wait...";
                toolStripStatusLabel1.BackColor = Color.Beige;
                statusStrip1.Refresh();

                button1.Enabled = false;

                // direct:
                //foreach (string tableName in checkedListBoxTables.CheckedItems)
                //{
                //    _conversion.DoConvert(tableName); // run the conversion.
                //}
                // 

                // threaded:
                var coll = checkedListBoxTables.CheckedItems.Cast<string>().ToList();
                    // convert collection to IEnumerable<string>
                _worker = new ConversionWorker(_conversion, coll);
                _worker.WorkerCompletedEvent += WorkerCompletedEventHandler;
                _worker.Start();

                button2.Enabled = true;


            // thread continues asynchronously, we expect WorkerCompletedEventHandler to fire when it completes.


        }

        private bool _bFirst;

        private void MainForm_Activated(object sender, EventArgs e)
        {
            try
            {
                if (!_bFirst)
                {
                    _bFirst = true;
                    checkedListBoxTables.Focus();
                    checkedListBoxTables.SelectedItem = 0;
                }
            }
            catch (ApplicationException ex)
            {
                //ignore.
                Log("Mainform_activated: "+ex.Message);
            }

        }

        private void RefreshItems()
        {

            StringBuilder sb = new StringBuilder();
            foreach (var item in checkedListBoxTables.CheckedItems)
            {
                sb.Append(item.ToString() + ", \n");

            }

            textBox1.Text = sb.ToString();
        }

        private void checkedListBoxTables_Click(object sender, EventArgs e)
        {

         //   this.BeginInvoke(new MethodInvoker(() => RefreshItems() ));

            
        }

        private void checkedListBoxTables_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            // BeginInvoke is much more elegant than PostMessage(this.Handle, WM_USERMESSAGE,0,0);
            // Technically this is an On Before Item Check State Change. The state change hasn't
            // taken effect, so this event can be used to block the change.  Then for an After 
            // Item Check change effect, use BeginInvoke.
            BeginInvoke(new MethodInvoker(() => RefreshItems()));
        }

        private void checkedListBoxTables_SelectedIndexChanged(object sender, EventArgs e)
        {
          //   this.BeginInvoke(new MethodInvoker(() => RefreshItems()));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _worker.Cancel();
        }
    }
}
