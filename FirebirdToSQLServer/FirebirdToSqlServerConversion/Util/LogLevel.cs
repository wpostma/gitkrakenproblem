namespace RamSoft.FirebirdToSqlServer.Util
{
    /// <summary>
    /// Enum used by minimalist logging class.
    /// </summary>
    public enum LogLevel
    {
        LFine, LDebug, LInfo, LWarning, LError
    }
}