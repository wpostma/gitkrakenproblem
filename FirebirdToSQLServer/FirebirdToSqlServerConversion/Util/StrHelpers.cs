using System;

namespace RamSoft.FirebirdToSqlServer.Util
{
    [Serializable()]
    public class StrHelpers
    {

        /// <summary>
        ///  Case Insensitive String Comparison, and Non-Nullity in a single operation.
        /// </summary>
        /// <param name="str1">Left side string to compare</param>
        /// <param name="str2">Right side string to compare</param>
        /// <returns>True if both inputs are non-null, and they are the same except for ignore case.</returns>
        static public bool SameText(string str1, string str2)
        {
            if ((str1 == null) && (str2 == null))
                return false;
            else
                return String.Equals(str1, str2, StringComparison.OrdinalIgnoreCase);
        }

        static public int DiffText(string str1, string str2)
        {
            bool isSame = SameText(str1, str2);
            if (isSame)
                return -1;
            // find first difference:
            try
            {
                string str1upper = str1.ToUpper();
                string str2upper = str2.ToUpper();
                int lmax = str1upper.Length;
                if (str2upper.Length < lmax)
                {
                    lmax = str2upper.Length;
                }
                for (int i = 0; i < lmax; i++)
                {
                    char c1 = str1upper[i];
                    char c2 = str2upper[i];
                    if (c1 != c2)
                        return i;
                }
                return lmax;

            }
            catch (Exception)
            {

                throw;
            }
        }
        static public string RemoveBrackets(string instring)
        {
            if (instring == null)
                return "";

            if (instring.Contains(" "))
            {
                throw new StringHelperFailure("Unable to remove brackets from a field name containing a space");
            }

            return instring.Replace("[", "").Replace("]", "");
        }

        static public string NameOfObjectStr(object value)
        {
            if ((value == null)||(value == DBNull.Value))
            {
                return "NULL";
            }
            else if (value is char)
            {
                return "'" + value.ToString() + "'";
            }
            else if (value is bool)
            {
                bool b = (bool) value;
                if (b)
                    return "/*True*/ 1";
                else
                    return "/*False*/ 0";

            }
            else if (value is TimeSpan)
            {
                return "'" + value.ToString() + "'";
            }
            else if (value is DateTime)
            {
                return "'"+((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss")+"'";
            }
            else if (value is String)
            {
                return '\'' + ((string)value) + '\''; // single quotes so we can output syntactically valid SQL when an insert fails.
            }
            else
                return value.ToString();
        }
        static public string AddBrackets(string instring)
        {
            if (instring == null)
            {
                return "";
            }
            else if (instring.Contains(" "))
            {
                throw new StringHelperFailure("Unable to add brackets to a field name containing a space");
            }

            if (instring.Contains("["))
            {
                return instring;
            }
            else
            {
                return "[" + instring.Replace(".", "].[") + "]";
            };
        }

    }
}