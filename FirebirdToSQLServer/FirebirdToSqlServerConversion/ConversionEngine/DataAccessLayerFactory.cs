using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using FirebirdSql.Data.FirebirdClient;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;

namespace RamSoft.FirebirdToSqlServer.ConversionEngine
{
    public class DataAccessLayerFactory : IDataAccessLayerFactory, IDisposable, IColumnInfoCache
    {
        private readonly string _fbConnStr;
        private readonly string _msSqlConnStr;
        private FbConnection _fbConnection;
        private Dictionary<string, ColumnInfo> _sqlFieldLookup;
        private SqlConnection _sqlConnectionForInfo; // ONLY USED FOR GetInfoxxx calls.

        public DataAccessLayerFactory(string fbConnStr, string msSqlConnStr)
        {
            _fbConnStr = fbConnStr;
            _msSqlConnStr = msSqlConnStr;
            _sqlFieldLookup = new Dictionary<string, ColumnInfo>();

        }

        public int ExecuteCommand( /*FBConnection*/ DbConnection connection, /*FbTransaction*/ DbTransaction transaction, string commandSql)
        {
            var cmd = new FbCommand(commandSql)
            {
                Connection = connection as FbConnection,
                Transaction = transaction as FbTransaction
            };
            return cmd.ExecuteNonQuery();
        }

        public DbDataReader ExecuteQuery( /*FbConnection*/ DbConnection connection,  /*FbTransaction*/ DbTransaction transaction, string querySql)
        {
            var qry = new FbCommand(querySql)
            {
                Connection = connection as FbConnection,
                Transaction = transaction as FbTransaction
            };
            return qry.ExecuteReader(); // the command generates an FbDataReader.
        }

        public virtual IDataAccessLayer CreateDataAccessLayer()
        {
            return new DataAccessLayer(GetFirebirdConnection() as FbConnection,
                GetMssqlConnection() as SqlConnection,
                this
                );
        }

        public void FinalizeDataAccessLayer(IDataAccessLayer dal)
        {
            (dal as IDisposable)?.Dispose();

        }

        public ExportWriter CreateExportWriter(
            IExportWriterOwner owner,
            IDataAccessLayer dal,
            IMapping mapping,
            int maxRowCount,
            // Optional special case fields:
            SingleDefaultValue single = null)
        {
            return new ExportWriter(owner,
                dal,
                mapping,
                maxRowCount,
                single);
        }


        // This is the Firebird connection logic.
        public DbConnection GetFirebirdConnection()
        {
            // real type: FbConnection

            if (_fbConnection != null)
            {
                return _fbConnection;
            }
            string connectStr = _fbConnStr;
            _fbConnection = new FbConnection(connectStr);
            _fbConnection.Open();
            return _fbConnection;
        }


        // Every MS SQL READER and WRITER gets its own connection. Otherwise we end up in too much pain.
        // We can share one connection for all the GetInfoForTable calls.
        public DbConnection GetMssqlConnection()
        {
            // real type: SqlConnection

            // NOTE: WE CAN NOT SHARE SQL CONNECTIONS IN THIS APP!
            var sqlConnection = new SqlConnection(_msSqlConnStr);
            sqlConnection.Open(); // throws Sql Client exception on failure.
            return sqlConnection;
        }

        public void Dispose()
        {

            Dispose(false);

        }

        private void Dispose(bool managed)
        {
            // data access layer factory disposes one firebird shared, and one ms sql shared connection,
            // but many other MS SQL connections are created at least one per DataAccessLayer/Writer pair.

            _fbConnection?.Dispose();
            _fbConnection = null;
            _sqlConnectionForInfo?.Dispose();
            _sqlConnectionForInfo = null;

        }


        public IDataAccessLayer CreateDataAccessLayerIntf()
        {
            return new DataAccessLayer(GetFirebirdConnection() as FbConnection,
                GetMssqlConnection() as SqlConnection,
                this);
        }

        public IColumnInfo GetInfoForTable(string tableName)
        {
            ColumnInfo value;
            if (_sqlFieldLookup.TryGetValue(tableName, out value))
            {
                return value; // each time we hit this path we save a pointless repetition of an SQL query.
            }
            else
            {
                if (_sqlConnectionForInfo == null)
                {
                    _sqlConnectionForInfo = GetMssqlConnection() as SqlConnection;
                }

                value = new ColumnInfo();
                using (SqlCommand sqlColumns = new SqlCommand("SELECT TOP 0 * from " + tableName + " -- GetInfoForTable ", _sqlConnectionForInfo))
                {
                    SqlDataReader sqlColumnsReader = sqlColumns.ExecuteReader();
                    for (int i = 0; i < sqlColumnsReader.FieldCount; i++)
                    {
                        value.AddColumnInfoItem(sqlColumnsReader.GetName(i), sqlColumnsReader.GetDataTypeName(i));

                    }
                    sqlColumnsReader.Close();
                } // using
                _sqlFieldLookup.Add(tableName, value); // remember for later.
                return value;
            } //else
        } //GetInfoForTable
    }
}