﻿using Moq;
using Newtonsoft.Json.Linq;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;

namespace FirebirdToSQLServerTests.Fixture
{
    internal class PatientStudyWriterFixture
    {

        public JObject Mapdict;
        public ExportMapping Mapping;


        public Mock<IExportWriterOwner> Owner { get; set; }

        //public Mock<IDataAccessLayer> Dal { get; set; }  // tried mocking it, and it got awkward fast.

        public FakeDataAccessLayer Dal { get; set; }


        public ExportWriter Writer { get; set; }

        // This string defines enough stuff to handle cases that were found and covered using the real patient and patient+study basics.
        // This string represents the state of Mapping.JSON before the IHE MIMA refactor
        private const string PatientStudyBeforeIHEMima1 =
           @"{
              ""ROLELIST"": {
                ""destination"": ""[dbo].[Role]"",
                ""identity"": ""ROLENAME"",
                ""key"": [ ""int"", ""ROLEID"", ""[InternalRoleId]"" ],
                ""fields"": [
                  [ ""ROLENAME"", ""[NAME]"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              },

              ""GROUPLIST"": {

                ""destination"": ""[dbo].[Group]"",
                ""identity"": ""GROUPNAME"",
                ""key"": [ ""int"", ""GROUPID"", ""InternalGroupId"" ],
                ""depends"": [ ""ROLELIST"" ],
                ""fields"": [
                  [ ""GROUPNAME"", ""[GroupName]"" ],
                  [ ""ROLEID"", ""[InternalRoleID]"", ""[dbo].[Role]"", ""[Name]"", ""SYSTEM"", ""int"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              },

              ""USERLIST"": {
                ""destination"": ""[dbo].[User]"",
                ""identity"": ""USERNAME"",
                ""key"": [ ""int"", ""USERID"", ""[InternalUserID]"" ],
                ""depends"": [ ""EMPLOYER"", ""FACILITIES"", ""GROUPLIST"", ""PHYSICIANSPECIALTYTYPE"", ""ROLELIST"" ],
                ""fields"": [
                  [ ""USERNAME"", ""[UserName]"" ],
                  [ 0, ""[IsActive]"" ],
                  [ ""GROUPID"", ""[InternalGroupID]"", ""[dbo].[Group]"", ""[GroupName]"", ""SYSTEM"", ""int"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              },

              ""PATIENTSTUDY"": {
                ""destination"": ""[phi].[Study]"",
                ""destintidentity"": 1,
                ""identity"": ""STUDYINSTANCEUID"",
                ""depends"": [ ""STUDYSTATUSVALUES"", ""FACILITIES"", ""MODALITIES"", ""RESOURCES"", ""PATIENTMODULE"" ],
                ""key"": [ ""int"", ""INTERNALSTUDYID"", ""[InternalStudyId]"" ],
                ""fields"": [
                  [ ""STUDYINSTANCEUID"", ""StudyUID"" ],
                  [ ""INTERNALPATIENTID"", ""InternalPatientId"", ""[phi].[Patient]"", ""[InternalPatientID]"", ""int"" ],
                  [ ""STUDYDESCRIPTION"", ""Description"" ],
                  [ ""STUDYID"", ""StudyId"" ],
                  [ ""ACCESSIONNUMBER"", ""AccessionNumber"" ],
                  [ ""REQPROCEDUREID"", ""RequestedProcedureID"" ],
                  [ ""INTERNALSTUDYID"", ""ForeignDBStudyID"" ],
                  [ ""PRIORITY"", ""PRIORITYVALUE"" ],
                  [ ""HL7UPDATED"", ""IsHL7Updated"" ],
                  [ ""STATUSORDER"", ""StatusValue"" ],
                  [ ""STUDYTIMESTAMP"", ""LastUpdateUTC"" ],
                  [ ""STUDYDATETIME"", ""StudyDateTimeUTC"" ],
                  [ ""+STUDYDATETIME"", ""StartDateTime"" ],
                  [ ""SCHEDULEDMODALITY"", ""ModalityCode"" ],
                  [ ""*"", ""ExtJSON"" ]
                ]
              },

              ""PATIENTMODULE"": {
                ""destination"": ""[phi].[Patient]"",
                ""identity"": [ ""PATIENTID"", ""ISSUEROFPATIENTID"" ],
                ""depends"": [ ""CACHEDISSUEROFPATIENTIDS"" ],
                ""key"": [ ""int"", ""INTERNALPATIENTID"", ""InternalPatientId"" ],
                ""existing"": ""select [InternalPatientID] from [phi].[Patient] WHERE [InternalIssuerID] = @InternalIssuerID AND [PatientID] = @PATIENTID"",
                ""fields"": [
                  [ ""PATIENTID"", ""[PatientID]"" ],
                  [ ""ISSUEROFPATIENTID"", ""InternalIssuerID"", ""[dbo].[Issuer]"", ""[Issuer]"", ""DEFAULT"", ""int"" ],
                  [ ""$DICOMNAME"", ""PatientName"" ],
                  [ ""PATIENTTIMESTAMP"", ""Timestamp"" ],
                  [ ""$GUID_GENERATE()"", ""PatientGUID"" ],
                  [ ""+ISSUEROFPATIENTID"", ""Issuer"" ],
                  [ ""COUNTRY"", ""COUNTRYCODE"" ],
                  [ ""HOMEPHONE"", ""PHONEHOME"" ],
                  [ ""BUSINESSPHONE"", ""PHONEBUSINESS"" ],
                  [ ""@LASTUPDATEUSER"", ""LastUpdateUserID"", ""[dbo].[User]"", ""[UserName]"", ""SYSTEM"", ""int"" ],
                  [ 1, ""IsActive"" ],
                  [ ""EMAIL"", ""EmailAddress"" ],
                  [ ""*"", ""[ExtJSON]"" ]
                ]
              },


              ""FACILITIES"": {
                ""destination"": ""[dbo].[Facility]"",
                ""identity"": ""FACILITYNAME"",
                ""key"": [ ""int"", ""INTERNALFACILITYID"", ""InternalFacilityId"" ],
                ""depends"": [ ""CACHEDISSUEROFPATIENTIDS"" ],
                ""fields"": [
                  [ ""FACILITYNAME"", ""[FacilityName]"" ],
                  [ ""ISSUEROFPATIENTID"", ""[InternalIssuerID]"", ""[dbo].[Issuer]"", ""[Issuer]"", ""DEFAULT"" ],
                  [ ""IMAGING"", ""[IsImaging]"" ],
                  [ ""REFERRING"", ""[IsReferring]"" ],
                  [ ""MAMMOTRACKING"", ""[IsMammoTracking]"" ],
                  [ ""COUNTRY"", ""[CountryCode]"" ],
                  [ ""EMAIL"", ""[EmailAddress"" ],
                  [ ""CONTACTPHONE"", ""[PhoneNumber]"" ],
                  [ ""FAXNUMBER"", ""[FaxNumber]"" ],
                  [ ""*"", ""[ExtJSON]"" ]

                ]
              },

              ""CACHEDISSUEROFPATIENTIDS"": {
                ""destination"": ""[dbo].[Issuer]"",
                ""destintidentity"": 1,
                ""identity"": ""ISSUEROFPATIENTID"",
                ""oldkey"": [ ""varchar"", ""ISSUEROFPATIENTID"" ],
                ""newkey"": [ ""int"", ""InternalIssuerID"" ],

                ""fields"": [
                  [ ""ISSUEROFPATIENTID"", ""Issuer"" ],
                  [ 1, ""[IsActive]"" ],
                  [ ""*"", ""[ExtJSON]"" ]
                ]
              }
        }";

        // fields: [
        //       [ InField, OutField, SelectTable, SelectField, SelectDefaultValue, SelectFieldType],
        // ]


        // a procedurally generated lookup string that lets us find the facility group in firebird by PK
        public const string FirebirdStudyIdLookupSql1 =
            "SELECT FIRST 1 INTERNALSTUDYID FROM PATIENTSTUDY WHERE STUDYID = @VALUE";


        public PatientStudyWriterFixture(string tableName = "PATIENTSTUDY", string jsonsnippet = PatientStudyBeforeIHEMima1)
        {
            Owner = new Mock<IExportWriterOwner>();
            Dal = new FakeDataAccessLayer();



            // firebird column info reader with a few fields,  FACILITYNAME, ISSUEROFPATIENTID
            Dal.MockFbReader.Setup(t => t.FieldCount).Returns(5);

            Dal.MockFbReader.Setup(t => t.GetName(0)).Returns("STUDYINSTANCEUID");
            Dal.MockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("varchar");

            Dal.MockFbReader.Setup(t => t.GetName(1)).Returns("INTERNALPATIENTID");
            Dal.MockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("int");

            Dal.MockFbReader.Setup(t => t.GetName(2)).Returns("STUDYDESCRIPTION");
            Dal.MockFbReader.Setup(t => t.GetDataTypeName(2)).Returns("varchar");

            Dal.MockFbReader.Setup(t => t.GetName(3)).Returns("STUDYID");
            Dal.MockFbReader.Setup(t => t.GetDataTypeName(3)).Returns("varchar");

            Dal.MockFbReader.Setup(t => t.GetName(4)).Returns("STATUSORDER");
            Dal.MockFbReader.Setup(t => t.GetDataTypeName(4)).Returns("int");

            // Fake syntax can be much more compact than the Moq syntax. For data table fakery beats mockery.
            Dal.FakeColumnInfo.AddColumnInfoItem("StudyUID","nvarchar" );
            Dal.FakeColumnInfo.AddColumnInfoItem("InternalPatientId", "int");
            Dal.FakeColumnInfo.AddColumnInfoItem("Description", "nvarchar");
            Dal.FakeColumnInfo.AddColumnInfoItem("StudyID", "nvarchar");
            Dal.FakeColumnInfo.AddColumnInfoItem("StatusValue", "int");
            Dal.FakeColumnInfo.AddColumnInfoItem("ExtJSON", "nvarchar"); // Allows [ "*" => "ExtJSON" ] to work

            /*
            Dal.MockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(6);

            Dal.MockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("StudyUID");
            Dal.MockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("nvarchar");

            Dal.MockSqlColumnInfo.Setup(t => t.GetName(1)).Returns("InternalPatientId");
            Dal.MockSqlColumnInfo.Setup(t => t.GetDataTypeName(1)).Returns("int");

            Dal.MockSqlColumnInfo.Setup(t => t.GetName(2)).Returns("Description");
            Dal.MockSqlColumnInfo.Setup(t => t.GetDataTypeName(2)).Returns("nvarchar");

            Dal.MockSqlColumnInfo.Setup(t => t.GetName(3)).Returns("StudyID");
            Dal.MockSqlColumnInfo.Setup(t => t.GetDataTypeName(3)).Returns("nvarchar");

            Dal.MockSqlColumnInfo.Setup(t => t.GetName(4)).Returns("StatusValue");
            Dal.MockSqlColumnInfo.Setup(t => t.GetDataTypeName(4)).Returns("int");


            // ExtJson : Allows [ "*" => "ExtJSON" ] to work
            Dal.MockSqlColumnInfo.Setup(t => t.GetName(5)).Returns("ExtJSON");
            Dal.MockSqlColumnInfo.Setup(t => t.GetDataTypeName(5)).Returns("nvarchar");
            */

            Mapdict = JObject.Parse(jsonsnippet); // will raise  Newtonsoft.Json.JsonReaderException if invalid.
            Mapping = new ExportMapping(tableName, Mapdict);

        }

        public void SetupWriter(int maxRowCount = 100, SingleDefaultValue single = null)
        {






            // This would blow up if no column reader is returned
            Writer = new  SemiFakeExportWriter(Owner.Object, Dal, Mapping,  maxRowCount, single);
        }
    }
}