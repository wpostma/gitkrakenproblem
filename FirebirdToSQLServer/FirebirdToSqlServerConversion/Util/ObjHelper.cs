﻿using System;

namespace RamSoft.FirebirdToSqlServer.Util
{

    /// <summary>
    /// Some static methods for checking object meets some criteria or not
    /// </summary>
    public class ObjHelper
    {
        public static bool ValueNullOrDbNull(object value)
        {
            return (value == DBNull.Value) || (value == null);
        }

        public static bool ValueNullOrDbNullOrEmptyString(object value)
        {
            if (value == null)
            {
                return true;
            }
            else if (value is string)
            {
                return ((string) value) == string.Empty;
            }
            else
              return (value == DBNull.Value);
        }
    }

}
