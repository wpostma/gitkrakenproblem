namespace FirebirdToSQLServerTests.Fixture
{
    internal class MasterDetailFixture : BaseFixture
    {
        private const string NormalizeCase1 = @"{
            ""FIREBIRDTABLENAME"": {
                        ""destination"": ""[dbo].[Table]"",
                        ""key"": [ ""int"", ""FBPRIMARYKEY"", ""[InternalTableId]"" ],
                        ""fields"": [
                          [ ""IN1"", ""[Out1]"" ],
                          [ ""anotherinternaltableid"", ""^[dbo].[AnotherTable].[AnotherInternalTableId]"" ]
                        ]
               }
            }";


        public MasterDetailFixture() : base(NormalizeCase1, "FIREBIRDTABLENAME")
        {
            MockReadersSetup();
        }

        private void MockReadersSetup()
        {
            // firebird column info reader with a few fields,  FACILITYNAME, ISSUEROFPATIENTID
            MockFbReader.Setup(t => t.FieldCount).Returns(3);

            MockFbReader.Setup(t => t.GetName(0)).Returns("IN1");
            MockFbReader.Setup(t => t.GetFieldType(0)).Returns(typeof (string));
            MockFbReader.Setup(t => t.GetDataTypeName(0)).Returns("varchar");

            MockFbReader.Setup(t => t.GetName(1)).Returns("FBPRIMARYKEY");
            MockFbReader.Setup(t => t.GetFieldType(1)).Returns(typeof (int));
            MockFbReader.Setup(t => t.GetDataTypeName(1)).Returns("int");

            MockFbReader.Setup(t => t.GetName(2)).Returns("anotherinternaltableid");
            MockFbReader.Setup(t => t.GetFieldType(2)).Returns(typeof (int));
            MockFbReader.Setup(t => t.GetDataTypeName(2)).Returns("int");

            MockSqlColumnInfo.Setup(t => t.GetFieldCount()).Returns(1);
            MockSqlColumnInfo.Setup(t => t.GetName(0)).Returns("Out1"); // NOT in the same order as above!!
            MockSqlColumnInfo.Setup(t => t.GetDataTypeName(0)).Returns("nvarchar");
        }
    }
}