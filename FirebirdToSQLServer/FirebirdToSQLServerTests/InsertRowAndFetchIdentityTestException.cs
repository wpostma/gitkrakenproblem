using System;
using RamSoft.FirebirdToSqlServer.Interface;

namespace Ramsoft.TestFixtures
{
    [Serializable]
    internal class InsertRowAndFetchIdentityTestException : ApplicationException
    {
        public IField Field;


        public string FirebirdTableName;
        public object IncomingValue;

        public InsertRowAndFetchIdentityTestException(string message,
            string afirebirdTableName,
            object anincomingValue,
            IField afield) : base(message)
        {
            FirebirdTableName = afirebirdTableName;
            Field = afield;
            IncomingValue = anincomingValue;
        }
    }
}