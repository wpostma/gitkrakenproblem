﻿using System;
using System.ComponentModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RamSoft.FirebirdToSqlServer.Util;

// unit under test: FirebirdToSqlHelpers

namespace RamSoft.FirebirdToSqlServer
{
    [TestClass]
    [Category("QuickTests")]
    public class HelpersTests
    {
        [TestMethod]
        [TestCategory("Helpers.More")]
        public void HelperDiff()
        {
            Assert.AreEqual(1, StrHelpers.DiffText("ab", "a"));
            Assert.AreEqual(1, StrHelpers.DiffText("a", "ab"));
            Assert.AreEqual(-1, StrHelpers.DiffText("aa", "aa"));
            Assert.AreEqual(-1, StrHelpers.DiffText("aa", "AA"));
            Assert.AreEqual(0, StrHelpers.DiffText("ba", "aa"));
            Assert.AreEqual(2, StrHelpers.DiffText("cc", "ccd"));
            Assert.AreEqual(2, StrHelpers.DiffText("cc", "CCd"));

            Assert.AreEqual(-1, StrHelpers.DiffText("select [Foo] FROM [Bar]", "SELECT [FOO] from [Bar]"));
        }

        [TestMethod]
        [TestCategory("Helpers.More")]
        public void HelpersObjNullMethod()
        {
            Assert.IsTrue(ObjHelper.ValueNullOrDbNull(null));

            Assert.IsTrue(ObjHelper.ValueNullOrDbNull(DBNull.Value));

            object n = 0;
            object m = "foo";

            Assert.IsFalse(ObjHelper.ValueNullOrDbNull(n));
            Assert.IsFalse(ObjHelper.ValueNullOrDbNull(m));


            Assert.IsTrue(ObjHelper.ValueNullOrDbNullOrEmptyString(DBNull.Value));

            Assert.IsTrue(ObjHelper.ValueNullOrDbNullOrEmptyString(""));

            Assert.IsFalse(ObjHelper.ValueNullOrDbNullOrEmptyString(1));
            Assert.IsFalse(ObjHelper.ValueNullOrDbNullOrEmptyString("A"));
            Assert.IsFalse(ObjHelper.ValueNullOrDbNullOrEmptyString(3.3));
        }


        [TestMethod]
        [TestCategory("Helpers.Dicom")]
        public void HelpersDicomNameHelper1()
        {
            // empty class, property assignment, 5 elements
            var h1 = new DicomPersonNameHelper();

            h1.FamilyName = "POSTMA";
            h1.GivenName = "WARREN";
            h1.MiddleName = "ARTHUR";
            h1.NamePrefix = "MR";
            h1.NameSuffix = "ESQ";

            var caretDelimitedOutputFormat = "POSTMA^WARREN^ARTHUR^MR^ESQ"; // caret delimited
            string s1 = h1.FormattedString();
            Assert.AreEqual(s1, caretDelimitedOutputFormat);
            string s2 = h1.ToString();
            Assert.AreEqual(s1, caretDelimitedOutputFormat);

            h1.MiddleName = "FRED";
            var caretDelimitedOutputFormat3 = "POSTMA^WARREN^FRED^MR^ESQ";
            string s3 = h1.FormattedString();
            Assert.IsTrue(s3 == caretDelimitedOutputFormat3,
                "Invalid output of formattedString, not in expected Dicom PN format with caret delimiters");
        } //t

        [TestMethod]
        [TestCategory("Helpers.Dicom")]
        public void HelpersDicomNameHelper2()
        {
            // four elements, provided as constructor element
            var h1 = new DicomPersonNameHelper("POSTMA", "WARREN", "ARTHUR", "MR");


            Assert.IsTrue(h1.NameSuffix == string.Empty,
                "we want default values like nameSuffix when not provided, to be String.Empty not Null"); //

            var caretDelimitedOutputFormat = "POSTMA^WARREN^ARTHUR^MR";
            string s1 = h1.FormattedString();
            Assert.AreEqual(s1, caretDelimitedOutputFormat);
            string s2 = h1.ToString();
            Assert.AreEqual(s1, caretDelimitedOutputFormat);
        }

        [TestMethod]
        [TestCategory("Helpers.Dicom")]
        public void HelpersDicomNameHelper3()
        {
            // four elements, provided as constructor element
            const string originalDicomCaretDelimitedValue = "POSTMA^WARREN^ARTHUR^MR";
            var h1 = new DicomPersonNameHelper(originalDicomCaretDelimitedValue);

            Assert.AreEqual(h1.FamilyName, "POSTMA");
            Assert.AreEqual(h1.GivenName, "WARREN");
            Assert.AreEqual(h1.MiddleName, "ARTHUR");
            Assert.AreEqual(h1.NamePrefix, "MR");
            Assert.AreEqual(h1.NameSuffix, "");


            string s1 = h1.FormattedString();
            Assert.AreEqual(s1, originalDicomCaretDelimitedValue);
            string s2 = h1.ToString();
            Assert.AreEqual(s1, originalDicomCaretDelimitedValue);
        }

        [TestMethod]
        [TestCategory("Helpers.More")]
        public void HelpersStrHelperSameText()
        {
            // TRUE cases
            Assert.IsTrue(StrHelpers.SameText("", ""), "check for empty string equality");

            Assert.IsTrue(StrHelpers.SameText("a", "A"), "case insensitivity check for single character");

            Assert.IsTrue(StrHelpers.SameText("abcdefg", "AbCdEfG"),
                "case insensitivity check for short sequence of characters");

            Assert.IsTrue(StrHelpers.SameText("Warren was here", "WARREN WAS HERE"),
                "case insensitivity check for short sequence of characters with spaces");


            // FALSE cases

            Assert.IsFalse(StrHelpers.SameText("Warren was here", "WARREN\u00a0WAS\u00a0HERE"),
                "unicode non-breaking space should not be computed as equal to space");

            Assert.IsFalse(StrHelpers.SameText("ABC", "DEF"), "content different simple case");

            Assert.IsFalse(StrHelpers.SameText("ABC", null), "content different null rvalue case ");

            Assert.IsFalse(StrHelpers.SameText(null, "ABC"), "content different null lvalue case ");

            // Maybe a surprising choice. Is a null LVALUE equal to a null RVALUE?  No, not for this helper.
            // This helper is for keyword recognition. If the rvalue is missing, no matter what the lvalue
            // input we want false returned.
            Assert.IsFalse(StrHelpers.SameText(null, null), "in error-input case, always return false");
        }

        [TestMethod]
        [TestCategory("Helpers.More")]
        public void HelpersStrHelperDebugObjectStr()
        {
            Assert.AreEqual("NULL", StrHelpers.NameOfObjectStr(null));
            Assert.AreEqual("NULL", StrHelpers.NameOfObjectStr(DBNull.Value));
            Assert.AreEqual("''", StrHelpers.NameOfObjectStr(""));
            Assert.AreEqual("'x'", StrHelpers.NameOfObjectStr("x"));
            Assert.AreEqual("1", StrHelpers.NameOfObjectStr(1));
            Assert.AreEqual("0", StrHelpers.NameOfObjectStr(0));
            Assert.AreEqual("/*False*/ 0", StrHelpers.NameOfObjectStr(false));
            Assert.AreEqual("/*True*/ 1", StrHelpers.NameOfObjectStr(true));
            var dt = new DateTime(2015, 1, 18);
            string dts = StrHelpers.NameOfObjectStr(dt);
            Assert.AreEqual("'2015-01-18 00:00:00'", dts);

            var ts = new TimeSpan(0, 3, 15, 23, 0);
            string tss = StrHelpers.NameOfObjectStr(ts);
            Assert.AreEqual("'03:15:23'", tss);


            Assert.AreEqual("3", StrHelpers.NameOfObjectStr(3.0));
            Assert.AreEqual("-1", StrHelpers.NameOfObjectStr(-1));
            Assert.AreEqual("SomeObjectTest!", StrHelpers.NameOfObjectStr(new SomeObject()));
            Assert.AreEqual("'a'", StrHelpers.NameOfObjectStr('a'));
        }

        private class SomeObject
        {
            public override string ToString()
            {
                return "SomeObjectTest!";
            }
        }
    } // class
} //ns