using System;

namespace RamSoft.FirebirdToSqlServer.Util
{
    /// <summary>
    // Something small that prevents me depending everywhere on any specific Logging framework.
    /// </summary>
    public class Log
    {
        public static event EventHandler<LogEventArgs> LogEvent;

        public static int LFineCount = 0; // even finer than debug is FINE.
        public static int LDebugCount = 0;
        public static int LInfoCount = 0;
        public static int LWarningCount = 0;
        public static int LErrorCount = 0;



        public static LogLevel MinLogLogLevel = LogLevel.LDebug;

        public static string GetPrefix(LogLevel logLevel)
        {
            switch (logLevel)
            {

                case LogLevel.LFine:
                    return "[FINE]";


                case LogLevel.LDebug:
                    return "[DEBUG]";
                case LogLevel.LInfo:
                    return "[INFO]";
                case LogLevel.LWarning:
                    return "[WARN]";
                case LogLevel.LError:
                    return "[ERROR]";

            }
            return "[UNKNOWN]";
        }

        private static void BumpCount(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.LFine:
                    LFineCount++;
                    break;
                case LogLevel.LDebug:
                    LDebugCount++;
                    break;
                case LogLevel.LInfo:
                    LInfoCount++;
                    break;
                case LogLevel.LWarning:
                    LWarningCount++;
                    break;
                case LogLevel.LError:
                    LErrorCount++;
                    break;

            }
        }


        public static void Write(string message)
        {
            //Debug.WriteLine(message);
            Write(LogLevel.LDebug, message);
        }

        public static void Write(LogLevel logLevel, string message)
        {
            if (logLevel >= MinLogLogLevel)
            {
                BumpCount(logLevel);


            }

            if (LogEvent != null)
            {
                var args = new LogEventArgs(logLevel, message);
                LogEvent(null, args);
            }
        }

    }
}