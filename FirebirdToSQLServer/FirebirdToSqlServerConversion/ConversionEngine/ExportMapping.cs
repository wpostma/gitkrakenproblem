using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using RamSoft.FirebirdToSqlServer.Exceptions;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;
using RamSoft.FirebirdToSqlServer.Util;
using StatementType = RamSoft.FirebirdToSqlServer.Interface.StatementType;

namespace RamSoft.FirebirdToSqlServer.ConversionEngine
{
    public class ExportMapping : IMapping
    {
        private bool _fault; // true means mapping is unworkable.
        private int _foreignIdentityCounter;
        private string _inkeyfieldname;
        private string _inkeyfieldtype;
        private bool _mappedFields; // did we call MapFields?
        private string _outkeyfieldname;
        private string _outkeyfieldtype;
        private int _severeErrorCount; // >0 means mapping is probably not working.
        private int _minorErrorCount; // >0 means please examine because mapping could be better.

        protected object ActiveItem;
        protected bool ExtJson;
        protected List<IField> FieldsMap;

        protected List<IField> FieldsMap2;
        //subset of fieldsMap.  Populated by BuildStatement, which is called by ExportWriter constructor.

        protected JObject MappingDictionary;
        // shared global MUTABLE dictionary. initial contents read by the outer class (Conversion).

        override public string ToString()
        {
            // ReSharper disable once RedundantArgumentDefaultValue
            var fields = GetMicrosoftSqlSelectFieldNames(true);

            return "{ Mapping " + FirebirdTableName + " => " + GetMicrosoftSqlTableName()+" ; "+FieldsMap.Count+":"+FieldsMap2.Count+" "+fields+" }";

        }

        protected Dictionary<string, string> ReverseLookupTableNames;

        public ExportMapping(string aFirebirdTableName, JObject mappingDictionary)
        {
            // construct objects or set object defaults:
            _fault = false;
            FirebirdTableName = aFirebirdTableName;
            FieldsMap = new List<IField>();
            FieldsMap2 = new List<IField>(); // filtered content from _fieldsMap.
            MappingDictionary = mappingDictionary;
            ReverseLookupTableNames = new Dictionary<string, string>();
            // build some reverse-order mapping stuff that we always need, once, at the start, instead of doing something slow or ugly later.
            BuildReverseTableNameMap();
            ReadTableDictionaryOrSetFaultBit();
        }

        private void ReadTableDictionaryOrSetFaultBit()
        {
            // There are lots of things in the table dictionary we are not
            // going to check here, but one we ARE going to check is the query
            // object. If the whole table dictionary is missing, we set
            // an error condition (_fault)
            var tableitem = (JObject) MappingDictionary[FirebirdTableName];
            if (tableitem != null)
            {
                var query = tableitem["query"];
                if (query != null)
                {
                    if (query.Type == JTokenType.Array)
                    {
                        // multi-line list of JSON strings lets us write arbitrarily long SQL queries.
                        var array = (JArray) query;
                        var list = array.ToObject<List<string>>();
                        CustomFbQuery = string.Join("\n", list);
                    }
                    else
                    {
                        CustomFbQuery = (string) query;
                    }
                }
            }
            else
            {
                _fault = true;
            }
        }

        /// <summary>
        /// one time precalculation of a reverse table name map, called from constructor.
        /// </summary>
        private void BuildReverseTableNameMap()
        {
            //.
            var tableNames = MappingDictionary.Properties().Select(p => p.Name).
                Where(name => string.CompareOrdinal(name.Substring(0, 2), "__") != 0).ToArray();
            // Skip items that start with __
            foreach (string value in tableNames)
            {
                try
                {
                    var item = (JObject) MappingDictionary[value];
                    if (item != null)
                    {
                        var dest = (string) item["destination"];
                        if (dest != null)
                        {
                            ReverseLookupTableNames[StrHelpers.AddBrackets(dest)] = value;
                        }
                    }
                    else
                        Log.Write("Missing table element " + value);
                }
                catch (ArgumentException ex)
                {
                    Log.Write(LogLevel.LInfo, "Argument exception:" + ex.Message);
                    Log.Write(LogLevel.LInfo, "Skipped table element " + value);
                }
            }
        }

        public string CustomFbQuery { get; set; }
        //public string identity { get; set; } // what field contains the most important NAME or UID for this entity.

        public bool Destintidentity { get; set; }

        public string FirebirdTableName { get; set; }

        public int GetForeignIdentityCounter()
        {
            return _foreignIdentityCounter;
        }


        public string Inkeyfieldtype
        {

            get { return _inkeyfieldtype; }
        }

        public string Inkeyfieldname
        {
            get { return _inkeyfieldname; }
        }

        public string Outkeyfieldtype
        {
            get { return _outkeyfieldtype; }
        }

        public string Outkeyfieldname
        {
            get { return _outkeyfieldname; }
        }

        public int GetAllFieldsCount()
        {
            return FieldsMap.Count;
        }

        public IField GetAllField(int idx)
        {
            return FieldsMap[idx] as IField;
        }

        public int GetSelectedFieldsCount()
        {
            return FieldsMap2.Count;
        }

        public void ResetFlags()
        {
            _fault = false;
            _severeErrorCount = 0;
            _foreignIdentityCounter = 0;
            ExtJson = false;
        }

        public JObject GetTableDictionary(string firebirdTableName)
        {
            return (JObject) MappingDictionary[firebirdTableName];
        }



        public string GetFieldLookupFbIdentityMapSql(string selectForMssqlTable)
        {
            // IMapping.GetFieldLookupFbIdentityMapSql
            // if selectForMssqlTable = "[dbo].[Role]"
            // this should get "SELECT FIRST 1 ROLENAME FROM ROLELIST WHERE ROLEID = @VALUE"
            // which is a FIREBIRD query.  It is done this way because it is useful to the data access layer
            // FirebirdLookupEntityName(IMapping mapping,...) method.

            string fbTableName = ReverseLookupTableName(selectForMssqlTable);
            var fbTableDictionary = GetTableDictionary(fbTableName);
            string fbTableEntityFieldList = GetFirebirdIdentityFieldListFromDictStr(fbTableDictionary);


            if (fbTableEntityFieldList == null)
                return "";
            if (fbTableEntityFieldList[0] == '$')
                return "";


            var oldkey = (JArray) fbTableDictionary["oldkey"];
            var newkey = (JArray) fbTableDictionary["key"];
            string fbKey;
            if (oldkey != null)
            {
                fbKey = (string) oldkey[1];
            }
            else
            {
                fbKey = (string) newkey[1];
            }


            string fbSql = "SELECT FIRST 1 " + fbTableEntityFieldList +
                           " FROM " + fbTableName +
                           " WHERE " +
                           fbKey + " = @VALUE";
            return fbSql;
        }

        /// <summary>
        ///     return Firebird table name that is to be mapped to mssql.
        /// </summary>
        /// <param name="mssqlTableName"></param>
        /// <returns></returns>
        public string ReverseLookupTableName(string mssqlTableName)
        {
            string aname = "";
            try
            {
                aname = StrHelpers.AddBrackets(mssqlTableName);
                return ReverseLookupTableNames[aname];
            }
            catch (Exception)
            {
                throw new MappingFailureException("Unable to do a reverse lookup from "+aname);
            }

        }

        private string GetOutputFieldNameForInputFieldName(string fbfield)
        {
            foreach(var afield in FieldsMap)
            {
                if (afield.InField == fbfield)
                {
                    // match found, but might not contain enough data to let us build a query.
                    if (string.IsNullOrEmpty(afield.OutField))
                        break; // fail.

                    return afield.OutField;
                }
            }
            return ""; // give up. parent function GetDefaultExistingItemQuery should also return empty string.
        }

        public string GetDefaultExistingItemQuery(string afirebirdtablename )
        {
            // the initial list of fields here is a list of FIREBIRD side values.
            var dict = (JObject)MappingDictionary[afirebirdtablename];
            var fields = GetFirebirdIdentityFieldListFromDict(dict);
            List<string> whereClauseItems = new List<string>();

            foreach (var fbfield in fields)
            {
                var sqlField = GetOutputFieldNameForInputFieldName(fbfield);
                if (string.IsNullOrEmpty(sqlField))
                    return string.Empty; //give up.

                whereClauseItems.Add( StrHelpers.AddBrackets( sqlField) + " = @" + sqlField + " ");

            }
            // not possible to build a query with no fields.
            if (whereClauseItems.Count == 0)
                return string.Empty;

            if (string.IsNullOrEmpty(_outkeyfieldname))
                return string.Empty;

            // possible to build a query.
            // TODO: Maybe we only want to fetch InternalTableId IDENTITY(1,1) value not *. Or maybe we want to fetch * so we can create and dump a string. Not sure.

            return "SELECT " + _outkeyfieldname + " FROM " + GetMicrosoftSqlTableName() + " WHERE " + string.Join(" AND ", whereClauseItems);

        }

        public string GetExistingItemQuery(string afirebirdtablename)
        {
            // IMapping.GetExistingItemQuery  will fetch a custom query if present in mapping.json
            // or generate a default.

            var dict = (JObject) MappingDictionary[afirebirdtablename];
            if (dict == null)
                return string.Empty;

            var existing = dict["existing"];
            if (existing == null)
            {
                existing = GetDefaultExistingItemQuery(afirebirdtablename);
            }

            string avalue = (string)existing;
            if ((avalue==null)|| (avalue == "0"))
            {
                return string.Empty; // User explicitly disabled.
            }
            return avalue;
        }

        public string GetFirebirdIdentityFieldListFromDictStr(JObject dict)
        {
            var list = GetFirebirdIdentityFieldListFromDict(dict);
            return string.Join(", ", list);
        }

        public List<string> GetFirebirdIdentityFieldListFromDict(JObject dict)

        {
            var identity = dict["identity"];
            if (identity == null)
            {
                // fallback.
                var list = new List<string> {(string) dict["fields"][0][0]};
                return list;
            }
            if (identity.Type == JTokenType.String)
            {
                var list = new List<string> {(string) identity};
                return list;
            }
            if (identity.Type == JTokenType.Array)
            {
                var identityArray = (JArray) identity;
                // I somehow want to write identityArray.ToList<string> but should
                // write ToObject<List<string>>
                var items = identityArray.ToObject<List<string>>();
                return items;
            }
            throw new MappingFailureException("identity entry is of an invalid type");
        }


        public string GetFirebirdIdentityFieldListStr(string afirebirdtablename)
        {
            var dict = (JObject) MappingDictionary[afirebirdtablename];
            if (dict == null)
                return "";
            // table name not found, no point exploding. we should probably print a serious error and skip.

            return GetFirebirdIdentityFieldListFromDictStr(dict);
        }


        public bool GetKeyFieldInfo(string afirebirdtablename,
            out string inkeyfieldtype,
            out string inkeyfieldname,
            out string outkeyfieldtype,
            out string outkeyfieldname)
        {
            var dict = (JObject) MappingDictionary[afirebirdtablename];
            inkeyfieldtype = null;
            inkeyfieldname = null;
            outkeyfieldtype = null;
            outkeyfieldname = null;
            if (dict == null)
                return false;
            // table name not found, no point exploding. we should probably print a serious error and skip.


            ParseKeyInfo(FirebirdTableName, dict, out inkeyfieldtype, out inkeyfieldname, out outkeyfieldtype,
                out outkeyfieldname);

            return true;
        }


        public IField GetSelectedField(int idx)
        {
            return FieldsMap2[idx] as IField;
        }

        public bool IsFaulted()
        {
            return _fault;
        }

        public int GetSevereErrorCount()
        {
            return _severeErrorCount;
        }

        public int GetMinorErrorCount()
        {
            return _minorErrorCount;

        }

        public string GetMicrosoftSqlTableName() // get "destination".
        {
            if (MappingDictionary != null)
            {
                var tableitem = (JObject) MappingDictionary[FirebirdTableName];
                return (string) tableitem["destination"];
            }
            throw new MappingFailureException("mappingDictionary field is missing");
        }

        public bool GetMicrosoftSqlTableIntIdentity() // get "destintidentity".
        {
            if (MappingDictionary != null)
            {
                var tableitem = (JObject) MappingDictionary[FirebirdTableName];
                var dest = tableitem["destintidentity"];
                if (dest != null)
                {
                    return dest.Value<int>() > 0;
                }
                return false;
            }
            throw new MappingFailureException("mappingDictionary field is missing");
        }

        public string GetMicrosoftSqlUpdateColumns()
        {
            var output = new List<string>();
            foreach (var field in FieldsMap2)
            {
                if (field.InsertOutField && field.UpdateSetField)
                {
                    output.Add(StrHelpers.AddBrackets(field.OutField) + " = @" + field.OutField);
                }
            }
            if (output.Count > 0)
            {
                return string.Join(", ", output);
            }
            throw new MappingFailureException(FirebirdTableName + " has no updatable fields");
        }

        public string GetMicrosoftSqlUpdateWhereClause()
        {
            var output = new List<string>();
            foreach (var field in FieldsMap2)
            {
                if (field.InsertOutField && !field.UpdateSetField)
                {
                    output.Add("(" + StrHelpers.AddBrackets(field.OutField) + " = @" + field.OutField + " )");
                }
            }
            if (output.Count > 0)
            {
                return string.Join(" AND\n", output);
            }
            throw new MappingFailureException(FirebirdTableName + " has no WHERE-condition fields");
        }

        // return destination field names in comma separated list format suitable for SELECT statement.
        // typical usage stack:
        //   ExportMapping.getMicrosoftSQLFieldNames(AddBrackets, prefix)
        //   ExportMapping.BuildStatement(StatementType )
        //   ExportWriter.setupMapping(mapping, single)
        //   ExportWriter.ExportWriter(owner, fbConnection, sqlConnection, mapping, maxRowCount, single)
        //   Conversion.DoConvert(string tableName)
        public string GetMicrosoftSqlSelectFieldNames(bool addBrackets = true, string prefix = "  ")
        {
            var sb = new StringBuilder();
            for (var n = 0; n < FieldsMap2.Count; n++)
            {
                var item = FieldsMap2[n];
                string outfield = item.OutField;
                if (outfield == null)
                    throw new MappingFailureException(FirebirdTableName + " item " + item.InField +
                                                      " outField element is null.");
                if (outfield == string.Empty)
                    throw new MappingFailureException(FirebirdTableName + " item " + item.InField +
                                                      "outField element is empty string.");


                if (!addBrackets)
                {
                    outfield = StrHelpers.RemoveBrackets(outfield);
                }

                if (prefix != null)
                {
                    sb.Append(prefix);
                }
                if (addBrackets && (outfield[0] != '['))
                    sb.Append("[");

                sb.Append(outfield);

                if (addBrackets && (outfield[0] != '['))
                    sb.Append("]");

                if (n < FieldsMap2.Count - 1)
                {
                    sb.Append(", \n");
                }
            }
            return sb.ToString();
        }

        // return comma separated @PARAMETER sequence with parameter name matching field name.
        public string GetMicrosoftSqlValueNames()
        {
            string fieldNames = GetMicrosoftSqlSelectFieldNames( /*no brackets please*/false, /*with prefix*/ "  @");

            if (fieldNames.Contains("["))
            {
                throw new MappingFailureException("getMicrosoftSQLValueNames should not have brackets");
            }
            return fieldNames;
        }

        protected void FieldsMapAdd(IField item)
        {
            if ((item.OutField == null) && item.InsertOutField)
            {
                Log.Write(LogLevel.LError,
                    "Internal problem: Field " + item.InField + " has no out field, but has insertOutField flag set.");
            }
            FieldsMap.Add(item);
            item.Index = FieldsMap.Count - 1;
            //item.queryIndex = -1;
        }


        /// <summary>
        ///   This seems like a horrible and confusing piece of code.
        /// </summary>
        public int FindOutOrInFieldIndex(string fieldName)
        {
            int i;
            string fieldNameNoBrackets = StrHelpers.RemoveBrackets(fieldName);
            for (i = 0; i < FieldsMap.Count; i++)
            {
                if (StrHelpers.SameText(FieldsMap[i].OutField, fieldNameNoBrackets)) // explicit rename handling.
                    return i;

                if (StrHelpers.SameText(FieldsMap[i].InField, fieldNameNoBrackets))
                    // same name handling without any rules. TODO: Make a way to disable this rule if it hurts us.
                    return i;
            }
            return -1; // not found.
        }

        public int FindInFieldIndex(string inFieldName)
        {
            int i;
            string fieldNameNoBrackets = StrHelpers.RemoveBrackets(inFieldName);
            for (i = 0; i < FieldsMap.Count; i++)
            {
                if (StrHelpers.SameText(FieldsMap[i].InField, fieldNameNoBrackets))
                    // same name handling without any rules. TODO: Make a way to disable this rule if it hurts us.
                    return i;
            }
            return -1; // not found.
        }


        public IField FindOutField(string outFieldName)
        {
            string fieldNameNoBrackets = StrHelpers.RemoveBrackets(outFieldName);
            for (int i = 0; i < FieldsMap.Count; i++)
            {
                var afield = FieldsMap[i];
                if (StrHelpers.SameText(afield.OutField, fieldNameNoBrackets)) // explicit rename handling.
                    return afield;

                for (int j = 0; j < afield.GetSubFieldCount(); j++)
                {
                    var subfield = afield.GetSubField(j);
                    if (StrHelpers.SameText(subfield.OutField, fieldNameNoBrackets)) // explicit rename handling.
                        return subfield;
                }

            }
            return null; // not found!
        }




        /// <summary>
        ///     If I pass in [ABC] or abc, and there is an outField value named Abc in
        ///     _fieldsMap, then find it, and return index, otherwise return -1
        /// </summary>
        public int FindOutgoingField(string fieldName)
        {
            int i;
            string fieldNameNoBrackets = StrHelpers.RemoveBrackets(fieldName);
            for (i = 0; i < FieldsMap.Count; i++)
            {
                if (StrHelpers.SameText(FieldsMap[i].OutField, fieldNameNoBrackets))
                    return i;
            }
            return -1; // not found.
        }

        protected int FindColumnInQueryFields(string fieldName, IDataReader fbReader)
        {
            for (var n = 0; n < fbReader.FieldCount; n++)
            {
                if (fbReader.GetName(n) == fieldName)
                    return n;
            }
            return -1; // not found.
        }

        public string GetOutputInserted()
        {
            return "OUTPUT INSERTED." + _outkeyfieldname;
        }

        protected bool FieldLooksImportant(string fieldName)
        {
            return (fieldName != "*") &&
                   !StrHelpers.SameText(StrHelpers.AddBrackets(fieldName), StrHelpers.AddBrackets(Outkeyfieldname));
        }


        /// <summary>
        ///  MapSqlColumnTypes sets the OutDbType in our mapping, and also adds magic items to the list.
        /// </summary>
        /// <param name="sqlColumnsReader"></param>
        /// <returns></returns>
        protected int MapSqlColumnTypes(IColumnInfo sqlColumnsData) // expected SqlDataReader or Mock
        {
            var mapFieldCount = 0;
            var skipFieldCount = 0;
            string outfield;
            if (sqlColumnsData.GetFieldCount() == 0)
                throw new MappingFailureException("SQL column reader has no fields.");

            Log.Write(FirebirdTableName + ": mapSqlColumnTypes for " + sqlColumnsData.GetFieldCount() +
                      " ms sql fields to  " + FieldsMap.Count + " fieldmap entries");


            for (var n = 0; n < sqlColumnsData.GetFieldCount(); n++)
            {
                string outExpression = sqlColumnsData.GetName(n);
                outfield = StrHelpers.RemoveBrackets(outExpression);
                string dataType = sqlColumnsData.GetDataTypeName(n);
                if (dataType == null)
                {
                    throw new MappingFailureException("SQL column reader returned a null data type name");
                    // If this happens when unit testing, your Mock is incomplete.
                }
                //       if (dataType == "bit")
                //       {
                //           Log.Write(outname + " is bit");
                //       }

                if (StrHelpers.SameText(outfield, "ExtJSON"))
                {
                    if (!ExtJson)
                    {
                        ExtJson = true;
                        var item = new ExportMappedField();
                        item.InField = "*";
                        // get all the values from incoming side and shove them into extjson. Clean it up later.
                        item.OutField = "ExtJSON";
                        item.InsertExtJson = true;
                        item.InsertOutField = true;
                        item.OutDbType = "JSON";
                        FieldsMapAdd(item);
                        Log.Write(FirebirdTableName + ":ExtJSON add as fieldsMap item " + item.Index);
                    }
                    else
                    {
                        Log.Write(FirebirdTableName + ":ExtJSON skip");
                    }
                }
                if (StrHelpers.SameText(outfield, _outkeyfieldname))
                {
                    Log.Write(FirebirdTableName + ": SKIP IDENTITY FIELD " + _outkeyfieldname);
                }
                else
                {
                    // Explicit case matches first.
                    var afield = FindOutField(outfield);
                    if (afield != null)
                    {
                        //Log.Write( LogLevel.lFine, firebirdTableName + ": "+outfield+" found!" );

                        afield.OutDbType = dataType; // .NET FieldType name like Int32
                        afield.InsertOutField = true;
                        mapFieldCount++;
                    }
                    else
                    {
                        var aindex = FindOutOrInFieldIndex(outfield); // magic fallback.
                        if (aindex >= 0)
                        {
                            afield = GetAllField(aindex);
                            afield.OutDbType = dataType; // .NET FieldType name like Int32
                            afield.InsertOutField = true;
                            mapFieldCount++;
                            if (!string.IsNullOrEmpty(outfield))
                            {
                                afield.OutField = outfield;

                            }
                            else
                            {
                                Log.Write(LogLevel.LError, "Field " + afield.InField + " has no output field");
                            }


                        }
                        else
                        {
                            if (FieldLooksImportant(outfield))
                            {
                                Log.Write(LogLevel.LWarning,
                                    FirebirdTableName + ": MS SQL field " + GetMicrosoftSqlTableName() + "." +
                                    outExpression +
                                    " not mapped to an incoming field. ");
                                _minorErrorCount++;
                            }
                            //_fieldsMap[index].insertOutField = false;
                            skipFieldCount++;
                        }
                    }
                }
            }
            if (mapFieldCount == 0)
            {
                throw new MappingFailureException("No fields mapped for " + FirebirdTableName + " from Mapping.json");
            }
            if (skipFieldCount > 0)
            {
                Log.Write(LogLevel.LWarning,
                    FirebirdTableName + ": " + skipFieldCount + " fields not mapped to MS SQL table " +
                    GetMicrosoftSqlTableName());
            }

            return skipFieldCount;
        }

        //  MapSecondaryFieldSubItemArray is a helper to  MapLookupFields
        protected void MapSecondaryFieldSubItemArray(IField field, JArray fielditem, int idx)
        {
            JArray subarray = (JArray)fielditem[idx];
            field.SubFieldsClear();

            foreach (var subfielditem in subarray)
            {
                var subfield = new ExportMappedField();
                JArray subfielditemarray = (JArray)subfielditem;
                //subfield.ParentField = field;

                // TODO: This logic is wildly incomplete. I need to refactor to avoid repeating huge piles of stuff.
                // I don't want to write ^ and @ and | handling multiple times.
                subfield.InField = StrHelpers.RemoveBrackets( (string)subfielditemarray[0]); // lvalue : firebird incoming field
                subfield.OutField = StrHelpers.RemoveBrackets( (string)subfielditemarray[1]); // rvalue : mssql outgoing field.
                subfield.SecondaryFlag = true; // all subfields are also secondary.
                subfield.InDbType = "?";
                subfield.OutDbType = "?";
                // later we need to populate subfield.InDbType,OutDbType.

                MapLookupFields(subfield, subfielditemarray);


                if (subfielditem.Count() > 2)
                    subfield.SelectTable = (string)subfielditem[2]; // lookup table (optional).
                field.SubFieldAdd(subfield);



            }
            // List<ExportMappedField> _subFields;

        }

        // MapLookupFields is a helper to BuildStatement, which calls it from several places
        // the part of the field def that contains lookup information is parsed here.
        // "fields": [
        //   [ "@LASTUPDATEUSER", "LastUpdateUserID", "[dbo].[User]", "[UserName]", "SYSTEM", "int" ],
        // ]
        // the table, field in that table, default value, and out-side of the identity MS SQL type
        // are specified like this:
        //   "[dbo].[User]", "[UserName]", "SYSTEM", "int"

        protected void MapLookupFields(IField field, JArray fielditem)
        {
            if (fielditem.Count > 2)
            {
                if (fielditem[2] is JArray)
                    throw new MappingFailureException("array not expected in " + field.InField+" third position");

                field.SelectTable = (string) fielditem[2]; // ie "[dbo].[User]"
            }
            if (fielditem.Count > 3)
            {
                if (fielditem[3] is JArray)
                {
                    // sub-item insertion
                    // [ "a", "b", "c", [ [ "d" ],[ "e" ] ] ]
                    if (field.SecondaryFlag)
                    {
                        MapSecondaryFieldSubItemArray(field, fielditem, 3);
                    }
                    else
                        throw new MappingFailureException("array not expected in "+field.InField+" fourth position");

                }
                else
                    field.SelectField = (string) fielditem[3]; // ie "[UserName]"
            }
            if (fielditem.Count > 4)
            {
                if (fielditem[4] is JArray)
                    throw new  MappingFailureException("array not expected in " + field.InField+" fifth position");

                field.SelectDefaultValue = (string) fielditem[4]; // ie "SYSTEM"
            }
            if (fielditem.Count > 5)
            {
                if (fielditem[5] is JArray)
                    throw new  MappingFailureException("array not expected in " + field.InField+" sixth position");

                field.SelectFieldType = (string) fielditem[5]; // ie "int"

                // may also be foreign key. there might be cases we have to detect here where we turn this off.
                // such as if on the firebird side, the incoming value is a name not a foreign key. This occurs
                // in dbo.Patient translations from firebird PATIENTMODULE where the last edit user is stored
                // as a user account name (varchar) instead of as an int identity.
                field.ForeignIdentity = true;

                _foreignIdentityCounter++;
            }
            if (fielditem.Count > 6)
            {
                throw new MappingFailureException("too many array elements in " + field.InField);
            }
        }

        public static void ParseKeyInfo(string firebirdTableName, JObject dict, out string inkeyfieldtype,
            out string inkeyfieldname, out string outkeyfieldtype, out string outkeyfieldname)
        {
            var keyinfoList = (JArray) dict["key"];
            if ((keyinfoList != null) && (keyinfoList.Count == 0))
            {
                // Mapping json contains "key": [],
                // that empty list means "no identity translation is done, the key values from firebird are copied over verbatim into MS SQL".
                // By convention field[0] in the fields array should be that key value.
                inkeyfieldtype = "*";  // don't care syntax.
                outkeyfieldtype = "*";
                inkeyfieldname = "*";
                outkeyfieldname = "*";


            }
            else if ((keyinfoList == null) || (keyinfoList.Count < 2))
            {
                // Mapping.json contains both an oldkey and newkey.
                var oldkeyinfoList = (JArray) dict["oldkey"];
                if ((oldkeyinfoList == null) || (oldkeyinfoList.Count < 2))
                    throw new MappingFailureException("Unable to read key or oldkey for " + firebirdTableName +
                                                      " from Mapping.json");


                var newkeyinfoList = (JArray) dict["newkey"];
                if ((newkeyinfoList == null) || (newkeyinfoList.Count < 2))
                    throw new MappingFailureException("Unable to read key or newkey for " + firebirdTableName +
                                                      " from Mapping.json");


                outkeyfieldtype = StrHelpers.RemoveBrackets((string) newkeyinfoList[0]); // expecting 'int' or 'char(2)'
                outkeyfieldname = StrHelpers.RemoveBrackets((string) newkeyinfoList[1]);
                // expecting primary key field name.
                inkeyfieldtype = (string) oldkeyinfoList[0]; // expecting primary key field name.
                inkeyfieldname = (string) oldkeyinfoList[1]; // expecting primary key field name.
            }
            else
            {
                // mapping.json contains "key": [ "fieldtype", "infieldname", "outfieldname" ]

                outkeyfieldtype = (string) keyinfoList[0]; // expecting 'int' or 'char(2)'
                inkeyfieldtype = (string) keyinfoList[0]; // expecting 'int' or 'char(2)'
                inkeyfieldname = (string) keyinfoList[1]; // expecting primary key field name.
                if (keyinfoList.Count > 2)
                {
                    outkeyfieldname = StrHelpers.RemoveBrackets((string) keyinfoList[2]);
                    // expecting primary key field name.
                }
                else
                {
                    outkeyfieldname = null;
                }
            }
        }

        public void MapFields(IDataReader fbReader,
            IColumnInfo sqlColumnsData,
            SingleDefaultValue single)
        {
            if (fbReader == null)
                throw new MappingFailureException("MapFields: fbReader is null");
            if (sqlColumnsData == null)
                throw new MappingFailureException("MapFields: sqlColumnsData is null");

            _mappedFields = true;

            var dict = (JObject) MappingDictionary[FirebirdTableName];
            if (dict == null)
                throw new MappingFailureException("Unable to read configuration for " + FirebirdTableName +
                                                  " from Mapping.json");

            ParseKeyInfo(FirebirdTableName, dict, out _inkeyfieldtype, out _inkeyfieldname, out _outkeyfieldtype,
                out _outkeyfieldname);


            var fieldinfoList = (JArray) dict["fields"];
            if ((fieldinfoList == null) || (fieldinfoList.Count < 1))
            {
                throw new MappingFailureException("Unable to read fields for " + FirebirdTableName +
                                                  " from Mapping.json");
            }
            FieldsMap.Clear();

            // DESTINATION INTEGER IDENTITY is a special flag that means that
            // when we create the MS SQL rows they have an IDENTITY(1,1)
            // auto -generated key.
            //            "CACHEDISSUEROFPATIENTIDS": {
            //                 "destination": "[dbo].[AssigningAuthority]",
            //                 "destintidentity": 1,
            bool destintidentity = GetMicrosoftSqlTableIntIdentity();

            // start by getting an item and putting it into fields map for each incoming field.
            // At some point in the design the goal here was to not populate the item.outField.
            // At some later point, it becomes a bad way to work, because we need the outField.
            // The question of whether or not to use the value in Mapping.json and put it right away into item.outfield is still an open question.
            for (var n = 0; n < fbReader.FieldCount; n++)
            {
                var item = new ExportMappedField();
                item.InField = fbReader.GetName(n);
                item.PrimaryKeyFlag = item.InField == _inkeyfieldname;
                item.Identity = item.PrimaryKeyFlag && !destintidentity;
                item.InsertOutField = false;
                item.InDbType = fbReader.GetDataTypeName(n);
                FieldsMapAdd(item);
            }


            // now fix up map based on the field map JSON configuration
            var mapFieldCount = 0;
            for (var n = 0; n < fieldinfoList.Count; n++)
            {
                mapFieldCount = FixupFieldInMapFields(fieldinfoList, mapFieldCount, n);
            }


            if (mapFieldCount == 0)
            {
                throw new MappingFailureException("Unable to read fields for " + FirebirdTableName +
                                                  " from Mapping.json");
            }


            /* map default identity value as a field, unless the identity is auto-generated. */
            if ((single != null)
                && !single.IdentityFlag
                && (single.DefaultValue != null)
                )
            {
                int fieldIndex = FindOutOrInFieldIndex(single.ValueField); // A bit of a crazy thing. TODO: Improve this?
                if (fieldIndex >= 0)

                {
                    var item = FieldsMap[fieldIndex];
                    if (!string.IsNullOrEmpty(item.OutField))
                    {
                        item.InsertOutField = true;
                        item.SelectDefaultValue = single.DefaultValue;
                        item.Identity = false;
                        // If insertOutField is true, it makes no difference unless identity is false
                        Log.Write(LogLevel.LInfo, FirebirdTableName +
                                               ": Forced-Assignment Identity for single row " +
                                               StrHelpers.NameOfObjectStr(single.DefaultValue)
                            );
                    }
                }
            }

            /* map column types, add ExtJSON item to output if it doesn't have it already. */
            MapSqlColumnTypes(sqlColumnsData);


            /* while copying from _fieldsMap to _fieldsMap2, check for uniqueness */
            bool duplicate;
            FieldsMap2.Clear();
            for (var n = 0; n < FieldsMap.Count; n++)
            {
                if (
                    //  ((_fieldsMap[n].inField[0] == '#')|| (_fieldsMap[n].inField[0] == '$'))
                    //    ||
                    FieldsMap[n].InsertOutField && !FieldsMap[n].Identity
                    )
                {
                    duplicate = false;
                    for (var m = 0; m < FieldsMap2.Count; m++)
                    {
                        if (FieldsMap2[m].OutField == FieldsMap[n].OutField)
                        {
                            duplicate = true;
                            Log.Write(LogLevel.LError,
                                FirebirdTableName + ": DUPLICATE FIELD OUT NAME: " + FieldsMap[n].OutField);
                            break;
                        }
                    }
                    if (!duplicate)
                    {
                        FieldsMap2.Add(FieldsMap[n]);
                    }
                }
            }

            Log.Write(LogLevel.LInfo, FirebirdTableName + " selected field count = " + FieldsMap2.Count);

            var fields = GetFirebirdIdentityFieldListFromDict(dict).ToDictionary(x => x, x => x);

            /* set QueryIndex, and UpdateSetField values*/
            for (var i = 0; i < GetAllFieldsCount(); i++)
            {
                var afield = GetAllField(i);
                afield.QueryIndex = FindColumnInQueryFields(afield.InField, fbReader);
                if ((afield.InField == null) || !fields.ContainsKey(afield.InField))
                {
                    // not an IDENTITY field, and not a foreign identity, and not a master detail.
                    afield.UpdateSetField = afield.InsertOutField &&
                                            string.IsNullOrEmpty(afield.SelectTable) &&
                                            !afield.ForeignIdentity &&
                                            !afield.MasterDetailFlag;
                }
            }
        }

        /// <summary>
        /// inner helper for MapFields
        /// </summary>
        /// <param name="fieldinfoList"></param>
        /// <param name="mapFieldCount"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        private int FixupFieldInMapFields(JArray fieldinfoList, int mapFieldCount, int n)
        {

            // A two element list contains lvalue,rvalue pairs.
            // A five element list contains lvalue,rvalue, selecttable, selectfield, defaultvalue
            // [ "ISSUEROFPATIENTID", "InternalAssigningAuthorityID", "[dbo].[AssigningAuthority]","[AssigningAuthority]", "DEFAULT" ]
            // if old field FK is string, and we're cleaning up our relational model,
            // we need to SELECT InternalAssigningAuthorityID from [dbo].[AssigningAuthority] where [AssigningAuthority] = {ISSUEROFPATIENTID}
            // and if that fails, we need to insert a value, with otherwise blank data
            // INSERT INTO [dbo].[AssigningAuthority] ( [AssigningAuthority] ) VALUES ( {assigning authority values} )
            var fielditem = (JArray)fieldinfoList[n];
            if (fielditem.Count < 2)
            {
                throw new MappingFailureException("Unable to read field " + n + " for " + FirebirdTableName +
                                                  " from Mapping.json");
            }
            string lvalue;
            if (fielditem[0].Type == JTokenType.Integer)
            {
                lvalue = "#" + (string)fielditem[0];
            }
            else
            {
                lvalue = (string)fielditem[0];
            }
            var rvalue = (string)fielditem[1];
            var requiredFlag = false;
            var masterDetailFlag = false;

            // Special Prefix Chars on RVALUEs:
            while (rvalue.Length > 0)
            {
                if (rvalue[0] == '^')
                {
                    masterDetailFlag = true;
                    rvalue = rvalue.Substring(1);
                }
                else if (rvalue[0] == '|')
                {
                    requiredFlag = true;
                    rvalue = rvalue.Substring(1);
                }
                else
                    break;
            }


            if (masterDetailFlag)
            {
                // case like [ "RESOURCEID", "^[dbo].[ReservedTimeResource].[InternalResourceId]" ]
                int idx = FindInFieldIndex(lvalue);
                if (idx < 0)
                    throw new MappingFailureException("Field " + lvalue + " not found in query of " +
                                                      FirebirdTableName + " from Mapping.json");
                var masterDetail = FieldsMap[idx];
                masterDetail.OutDbType = "MASTERDETAIL";
                masterDetail.OutField = rvalue; // in This case rvalue should have [] around everything.
                masterDetail.InsertExtJson = false;
                masterDetail.InsertOutField = false;
                // It's not a field in the INSERT statement, it becomes its own different insert statement.
                masterDetail.SecondaryFlag = true;
                masterDetail.NotNullFlag = requiredFlag;
                masterDetail.MasterDetailFlag = true;
                MapLookupFields(masterDetail, /*JArray*/ fielditem); // populate optional fielditem[2]..[5] values into masterDetail

                mapFieldCount++;
                Log.Write(LogLevel.LInfo, FirebirdTableName + " FIELD/MASTERDETAIL:  " + lvalue + " -> ^" + rvalue);
            }
            else
            {
                // lookup field by lvalue name.
                int idx = FindInFieldIndex(lvalue);
                if (idx >= 0)
                {
                    if (string.IsNullOrEmpty(rvalue))
                    {
                        throw new MappingFailureException("Unable to read field " + lvalue +
                                                          " output field value in " + FirebirdTableName +
                                                          " from Mapping.json");
                    }
                    FieldsMap[idx].OutField = StrHelpers.RemoveBrackets(rvalue);
                    FieldsMap[idx].InsertOutField = true;
                    FieldsMap[idx].NotNullFlag = requiredFlag; // If null, then change to empty string.
                    mapFieldCount++;

                    MapLookupFields(FieldsMap[idx], /*JArray*/ fielditem); // populate optional fielditem[2]..[5] values into FieldsMap[idx]


                    Log.Write(LogLevel.LInfo, FirebirdTableName + " FIELD/CLASSA:  " + lvalue + " -> " + rvalue);
                }
                else if (lvalue[0] == '+')
                {

                    lvalue = lvalue.Substring(1);
                    if (lvalue == string.Empty)
                    {
                        // + alone with no suffix means creation of a split of one incoming firebird row into two objects in the new hierarchy.
                        // If table CUSTOMER and ORDER in firebird became CUSTOMER, ORDERGROUP, ORDER with a new ORDERGROUP table being inserted,
                        // we would declare [ "+", "...." ] in our field list.

                        var secondaryItem = new ExportMappedField();
                        secondaryItem.InField = "+";
                        secondaryItem.InDbType = "split";
                        secondaryItem.OutField = StrHelpers.RemoveBrackets(rvalue);
                        secondaryItem.InsertExtJson = false;
                        secondaryItem.InsertOutField = true;
                        secondaryItem.SecondaryFlag = true;

                        MapLookupFields(secondaryItem, /*JArray*/ fielditem); // populate optional fielditem[2]..[5] values into secondaryItem

                        FieldsMapAdd(secondaryItem);
                        mapFieldCount++;
                        Log.Write(LogLevel.LInfo, FirebirdTableName + " FIELD/SPLIT:  " + lvalue + " -> " + rvalue);
                    }
                    else
                    { // +ABC means secondary additional re-mapping of a single incoming Firebird field into multiple MS SQL outgoing fields
                        idx = FindInFieldIndex(lvalue);
                        if (idx < 0)
                        {
                            Log.Write(LogLevel.LError,
                                FirebirdTableName + " FIELD/CLASSB MISSING:  " + lvalue + " -> " + rvalue);
                            _fault = true;
                            _severeErrorCount++;
                        }
                        else
                        {
                            var secondaryItem = new ExportMappedField();
                            secondaryItem.InField = lvalue;
                            secondaryItem.InDbType = FieldsMap[idx].InDbType;
                            secondaryItem.OutField = StrHelpers.RemoveBrackets(rvalue);
                            secondaryItem.InsertExtJson = false;
                            secondaryItem.InsertOutField = true;
                            secondaryItem.SecondaryFlag = true;


                            FieldsMapAdd(secondaryItem);
                            mapFieldCount++;
                            Log.Write(LogLevel.LInfo, FirebirdTableName + " FIELD/CLASSB:  " + lvalue + " -> " + rvalue);
                        }
                    }
                }
                else if ((lvalue[0] == '$') || (lvalue[0] == '#') || (lvalue[0] == '@'))
                {
                    // secondary functional field does NOT have to have any particular incoming query field
                    // match to be included in output so we directly set the insertOutField to true here.

                    var secondaryItem = new ExportMappedField();
                    secondaryItem.InField = lvalue;
                    if (lvalue[0] == '$')
                    {
                        secondaryItem.InDbType = "FUNCTION";
                    }
                    else if (lvalue[0] == '#')
                    {
                        secondaryItem.InDbType = "LITERAL";
                    }
                    else if (lvalue[0] == '@')
                    {
                        secondaryItem.InDbType = "DEREFERENCE";
                        // convert string value "USERID1" to its InternaluserId int primary key. meaning borrowed from Pascal @ take-address syntax.
                    }


                    // Todo: #-> Integer $->String? @->Dereferenced-Lookup.

                    secondaryItem.OutField = StrHelpers.RemoveBrackets(rvalue);
                    secondaryItem.OutDbType = "internal";
                    secondaryItem.InsertExtJson = false;
                    secondaryItem.InsertOutField = true;
                    secondaryItem.SecondaryFlag = true;


                    MapLookupFields(secondaryItem, /*JArray*/ fielditem);
                    // Right now these fields have no use except for the @ dereference-lookup case.


                    FieldsMapAdd(secondaryItem);
                    mapFieldCount++;
                    Log.Write(LogLevel.LInfo,
                        FirebirdTableName + ": FIELD/" + secondaryItem.InDbType + " " + lvalue + " -> " + rvalue);
                }
                else
                {
                    if (lvalue != "*")
                    {
                        Log.Write(LogLevel.LError, FirebirdTableName + "Did not find field " + lvalue);
                        _severeErrorCount++;
                    }
                }
            } // else lookup field by name

            return mapFieldCount;
        }

        /// <summary>
        ///  Generate valid SQL insert, no injection protection provided.
        /// </summary>
        /// <param name="fbtable">Goes into an SQL -- comment </param>
        /// <param name="tableName">MS SQL TABLE NAME</param>
        /// <param name="insertFields">FIELD1, FIELD2 string</param>
        /// <param name="inserted">optional OUTPUT INSERTED.fieldname or empty string</param>
        /// <param name="values">@FIELD1, @FIELD2 string</param>
        /// <returns></returns>
        private static string InsertStatement(string fbtable, string tableName, string insertFields, string inserted, string values)
        {
            string statement = $"-- RAMSOFT import {fbtable} table data \n" +
                        $"INSERT INTO {tableName}\n" +
                        $" ( \n {insertFields} \n ) \n" +
                        $"{inserted} \n" + //
                        $"VALUES \n( \n {values} \n)\n";
            return statement;
        }

        private static string UpdateStatement(string fbtable, string tableName, string updateFields, string whereClause)
        {
            string statement = $"-- RAMSOFT updated {fbtable} table data \n" +
                        $"UPDATE {tableName} \n SET \n {updateFields}" +
                        $"\nWHERE \n{whereClause}\n";
            return statement;
        }
        public string BuildStatement(StatementType statementType)
        {
            if (!_mappedFields)
            {
                throw new MappingFailureException("call MapFields before you call BuildStatement");
            }

            if (GetSelectedFieldsCount() == 0)
            {
                throw new MappingFailureException(
                    "MapFields was run but there are no output fields, so Mapping.json definition for this table is missing or incorrect");
            }
            string tableName = GetMicrosoftSqlTableName();
            if (string.IsNullOrEmpty(tableName))
            {
                throw new MappingFailureException("Unable to get SQL table");
            }

            string statement;
            switch (statementType)
            {
                case StatementType.Insert:

                    statement = InsertStatement( FirebirdTableName, tableName, GetMicrosoftSqlSelectFieldNames(), GetOutputInserted(), GetMicrosoftSqlValueNames());
                    break;

                case StatementType.Update:

                    // TODO: Actually USE the UPDATE statements. This stuff isn't used yet!

                    // UPDATE [dbo].[Facility]
                    // SET [ExtJSON] = @ExtJSON
                    // WHERE ([FacilityName] = @FacilityName )
                    //   AND ([InternalAssigningAuthorityID] = @InternalAssigningAuthorityID

                    statement =  UpdateStatement( FirebirdTableName, GetMicrosoftSqlTableName(),
                                GetMicrosoftSqlUpdateColumns(),
                                GetMicrosoftSqlUpdateWhereClause() );
                    break;

                default:
                    throw new NotImplementedException();
            }

            return statement;
        }


        public int BuildSubInsertStatements()
        {
            int subFieldCount = 0;
            //statement = InsertStatement(FirebirdTableName, tableName, GetMicrosoftSqlSelectFieldNames(), GetOutputInserted(), GetMicrosoftSqlValueNames());
            foreach (var field in FieldsMap2)
            {
                var insertFields = field.GetSubFieldNames(); // "[F1], [F2]";
                if (!string.IsNullOrEmpty(insertFields))
                {  // for each fields with subfields.

                    if (string.IsNullOrEmpty(insertFields))
                        throw new MappingFailureException("Internal: SubFieldNames missing or invalid");
                    var paramValues = field.GetSubFieldParamNames(); // "@F1, @F2";
                    if (string.IsNullOrEmpty(insertFields))
                        throw new MappingFailureException("Internal: SubFieldParamNames missing or invalid");
                    var outKeyFieldName = StrHelpers.AddBrackets(field.OutField);  // TODO: may need a way to specify a different name!
                    var inserted = "OUTPUT INSERTED."+outKeyFieldName;
                    var subFieldInsertSql = InsertStatement(FirebirdTableName, field.SelectTable, insertFields, inserted,
                        paramValues);
                    field.SetSubFieldInsertSQL(subFieldInsertSql);

                    subFieldCount++; //
                }
            }
            return subFieldCount;
        }

    }
}