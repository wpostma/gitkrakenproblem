﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace RamSoft.FirebirdToSqlServer.Exceptions
{


    [Serializable()]
    public class WriterFailException : ApplicationException
    {
        public WriterFailException() { }
        public WriterFailException(string message) : base(message) { }
    }

    [Serializable()]
    public class FieldLookupException : ApplicationException
    {
        public string InnerMessage;

        public FieldLookupException(string message, string sqlInnerMessage) : base(message)
        {
            InnerMessage = sqlInnerMessage;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {


            if (info == null)
                throw new ArgumentNullException(nameof(info));

            info.AddValue("InnerMessage", InnerMessage);
            base.GetObjectData(info, context);
        }

    }
}
