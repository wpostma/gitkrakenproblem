@echo off
echo ===== build FirebirdToSqlServerTools begins.
set HOMEDIR=%CD%
if not defined DevEnvDir ( call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat" )
echo DevEnvDir = %DevEnvDir%
mkdir .\FirebirdToSQLServer\packages 2>nul >nul
cd .\FirebirdToSQLServer\packages
echo  ========================== packages updated %DATE% ==========================  >package.log
nuget install "Newtonsoft.Json" >>package.log
if %ERRORLEVEL% neq 0 echo *** nuget-error newtonsoft: %ERRORLEVEL%
nuget install "Common.Logging" >>package.log
if %ERRORLEVEL% neq 0 echo *** nuget-error logging: %ERRORLEVEL%
nuget install "Common.Logging.Core" >>package.log
if %ERRORLEVEL% neq 0 echo *** nuget-error logging: %ERRORLEVEL%
nuget install "FirebirdSql.Data.FirebirdClient" >>package.log
if %ERRORLEVEL% neq 0 echo *** nuget-error firebird: %ERRORLEVEL%
nuget install "Moq" >>package.log
if %ERRORLEVEL% neq 0 echo *** nuget-error moq: %ERRORLEVEL%
REM /P:Configuration=Release OR /P:Configuration=Debug
cd %HOMEDIR%
echo on
msbuild  /P:Configuration=Release %HOMEDIR%\FirebirdToSqlServerTools.sln
@echo off
cd %HOMEDIR%