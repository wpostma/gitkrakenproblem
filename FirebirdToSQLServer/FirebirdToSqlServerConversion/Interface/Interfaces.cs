﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Newtonsoft.Json.Linq;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Model;
using JsonDictionary = System.Collections.Generic.Dictionary<string, object>;


//-------------------------------------------------------------------------------
// RamSoft Firebird to SQL Server  Interfaces, enums and Event Arg data objects
// used In Those Interfaces, for FB=>MSSQL data conversion.
//
// We abstract away the concrete types with Interfaces, so we can replace it via a mock or fake
// during unit testing, and also so we can see the contract between parts of our code.
//
// TODO: Write XML documentation comments for the interface methods.
//----------------------------------------------------------------------------------
// NOTICE:
// We don't want any concrete dependency on specific database clients in this unit.
//----------------------------------------------------------------------------------


namespace RamSoft.FirebirdToSqlServer.Interface
{
    ///<summary>Enum for Types of Indirections/Lookups/Transform Queries</summary>
    public enum DataReaderQueryType
    {
        EntityNamed,
        JsonValue,
        SimpleLookup
    }

    ///<summary>Enum to flag an SQL operation as an Insert or Update</summary>
    public enum StatementType
    {
        Insert,
        Update
    }

    ///<summary> C# Event data object for status events.</summary>
    public class StatusEventArgs : EventArgs
    {
        public StatusEventArgs(int error, string message, string context)
        {
            ErrorCode = error;
            Message = message;
            Context = context;
        }

        public int ErrorCode { get; set; }
        public string Message { get; set; }

        public string Context { get; set; }
    }




  /// <summary>
  /// IColumnInfo is an abstraction for data set column names.
  /// </summary>
    public interface IColumnInfo
    {


        int GetFieldCount();
        string GetName(int index);
        string GetDataTypeName(int index);
        //

    }

    /// <summary>
    /// IColumnInfoCache is an abstraction for data set column names fetching/caching service
    /// </summary>

    public interface IColumnInfoCache
    {
        IColumnInfo GetInfoForTable(string tableName);
    }


    /// <summary>
    /// IConversion: Firebird to SQL Conversion Application layer as abstract interface. Application layer needs to handle
    /// Status Events, and Output Events, and uses a Data Access Layer factory to create data access layers and writers, and execute a table
    /// conversion.
    /// </summary>
    ///
    public interface IConversion
    {
        List<string> Tables { get; set; }
        int DoConvert(string tableName);

        void SetSampleOutputCounter(int acount);

        event EventHandler<StatusEventArgs> StatusEvent; // Fired many times. Single line status text.

        event EventHandler<StatusEventArgs> SampleOutputEvent;
            // Fired at end of conversion, contains a sample of the output.

        string GetFailedSql();

        List<string> GetDependenciesFor(string tableName);

        void Cancel(); // should set a volatile bool, and inside the worker we should raise when it's set.
    }

    /// <summary>
    /// IDataAccessLayerFactory is a factory interface used to obtain connections to databases, and the top level Data Access Layer object, and
    /// its sub-level objects called Writers.  While I thought about splitting this into several smaller interfaces, I would only be increasing
    /// accidental complexity and not increasing the clarity, usefulness or correctness of this code. A time will probably come though when such a
    /// split would be a good idea.    At this point one Data Access Layer Factory has a lifetime approximately equal to the entire application
    /// lifetime and has no state that is specific to a single table or data transfer "job".
    /// </summary>
    public interface IDataAccessLayerFactory
    {
        DbConnection GetFirebirdConnection(); // realtype: FbConnection

        DbConnection GetMssqlConnection(); // realtype: SqlConnection

        int ExecuteCommand(DbConnection connection, DbTransaction transaction, string commandSql);

        DbDataReader ExecuteQuery(DbConnection connection, DbTransaction transaction, string querySql); // realtype: FbDataReader


        IDataAccessLayer CreateDataAccessLayer();

        void FinalizeDataAccessLayer(IDataAccessLayer dal);


        ExportWriter CreateExportWriter(
            IExportWriterOwner owner,
            IDataAccessLayer dal,
            IMapping mapping,
            int maxRowCount,
            // Optional special case fields:
            SingleDefaultValue single = null);

        //  SingleDefaultValue single = null
        // Optional special case fields:
        //  int maxRowCount,
        // ExportMapping mapping,

        // new ExportWriter(
    }

    /// <summary>
    /// IDataAccessLayer is an abstract DataAccessLayer interface for a data access layer concerned with
    /// executing Firebird queries and writing their results into Microsoft SQL Server tables.
    /// It lets you set up an SQL SERVER table, set up an INSERT statement against that table, and write
    /// to the parameters for that INSERT statement, and then execute the insert statement.
    /// </summary>
    public interface IDataAccessLayer
    {
        // commit
        bool Commit(int statementIndex);

        /// <summary>
        ///     SetupFieldSelectCommandQuery generates the query expression for an MS SQL lookup-of-a-foreign-key command.
        /// </summary>
        bool SetupFieldSelectCommandQuery(IMapping mapping, DataReaderQueryType aType,
            IField field, object incomingValue);

        /// <summary>
        ///  A writer will call this before we use the mapping passed in here, and before we execute any other insert
        ///  or parameter set actions.
        /// </summary>
        /// <param name="mapping"></param>
        /// <param name="single"></param>
        void SetupBeforeMapping(IMapping mapping, SingleDefaultValue single);

        /// <summary>
        ///   If we're inserting a row with defaults we may want to let the data access layer
        ///   initialize stuff for us.
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns>Parameter Count for the Insert</returns>
        int SetupBeforeWriteRowWithDefaults(IMapping mapping); // returns parameter count.

        /// <summary>
        ///   A regular row being inserted based on some row in firebird.
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns>Parameter Count</returns>
        int SetupBeforeExecute(int statementIndex, IMapping mapping); // returns parameter count.

        /// <summary>
        /// Create an SQL insert statement and return the index of that statement.
        /// </summary>
        /// <param name="insertSql"></param>
        /// <returns>statementIndex value, first time will be 0, second call will be 1</returns>
        int SetupInsertSql(string insertSql);


        /// <summary>
        /// MSSQL table get column information.
        /// Get all DataTypeName, string Name values for main MSQQL table if sqlTableName is null.
        /// If subfield is non null get the item for subField.SelectTable.
        /// </summary>
        /// <returns>Column (field) names and types</returns>
        IColumnInfo GetSqlColumnInfo(string sqlTableName = null);


        /// <summary>
        ///  Get a value from the incoming firebird row by column index
        /// </summary>
        /// <param name="indexIncoming">column index value starting at 0 and up</param>
        /// <returns>a value that could be a string or integer or date-time or something else that an SQL database field could contain</returns>
        object FirebirdReaderGetValue(int indexIncoming);

        /// <summary>
        ///  Firebird column types like "INTEGER", "TIMESTAMP", "VARCHAR(64)", "BLOB(BINARY)", or user
        ///  types like "T_YESNO" that is really a CHAR(1) with Y,N values, which maps to an MSSQL boolean "BIT".
        ///  Not MSSQL types like "INT" or "BIGINT" or "NVARCHAR(MAX)"
        /// </summary>
        /// <param name="indexIncoming">column index value starting at 0 and up</param>
        /// <returns></returns>
        string FirebirdReaderGetColumnType(int indexIncoming);

        string FirebirdReaderGetValueByName(string fieldname);
        JsonDictionary FirebirdReaderJsonSerializeRowFull();

        /// <summary>
        ///    The data access layer encapsulates the underlying dataset and its reader
        ///    and this method is to tell the dataset to advance the incoming Firebird
        ///    reader to the next row.
        /// </summary>
        /// <returns>Read success there is a Row if we return True</returns>
        bool FirebirdRead();


        ///<summary>
        /// The Writer class needs to set up mapping in method SetupMapping. For that it
        /// needs to access a Firebird database schema: get column reader.  It would probably be a
        /// good idea to do a tighter encapsulation of this  with a more specific adapter class but
        /// right now we're Leaking a general C# base data reader interface that is at least not
        /// specific to ANY one database type.
        ///</summary>
        ///<returns>An object we can use to get column (field) name and data type information </returns>
        IDataReader FirebirdColumnReader();


        /// <summary>
        ///  The Writer needs to know how many columns (fields) are in the incoming Firebird table.
        /// </summary>
        /// <returns>Column (field) count</returns>
        int FirebirdReaderGetFieldCount();

        /// <summary>
        ///  Transform incoming value from, among other things, an integer foreign key
        ///  valid only on the FB source into a name or identifier which is a string.
        /// </summary>
        /// <param name="mapping">entire set of mapping information about one firebird table conversion job</param>
        /// <param name="field">the field object we're doing the lookup for</param>
        /// <param name="incomingValue">Very often an integer but could be a two letter code like a state code</param>
        /// <returns>Some unique or semi-unique identifier</returns>
        string FirebirdLookupEntityName(IMapping mapping,
            IField field, object incomingValue);

        /// <summary>
        ///     If we want to log the data that we inserted the most useful format would
        ///     be as syntactically valid SQL insert statements that we could even write
        ///     into scripts, or, if it fails, to copy to clipboard and paste into SSMS
        ///     and try to puzzle out what's wrong and how to fix it.
        /// </summary>
        /// <param name="statementIndex">0 for main statement or 1,2,3, for some sub statement</param>
        /// <returns>INSERT statement or empty string</returns>
        string GetLastInsertSql(int statementIndex); // Get the last Insert statement, written out so it could be checked by an expert.

        /// <summary>
        ///   Similar to GetLastInsertSql but only the last failed statement.
        /// </summary>
        /// <returns>Some SQL statement, maybe insert or something else, or empty string</returns>
        string GetFailedSql(); // diagnostics and exception handling

        string GetParamValueAsStr(IDbCommand dbCommand, int index); // diagnostics and exception handling.

        // INSERT parameter-value-set methods, and execute and get identity (int).

        /// <summary>
        /// Sets up a parameter value so that we can later call InsertSqlExecute.
        /// </summary>
        /// <param name="statementIndex">0 means main insert statement</param>
        /// <param name="paramIndex"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool InsertSqlParamValue(int statementIndex, int paramIndex, object value);

        /// <summary>
        /// After InsertSqlParamValue has been called to set all parameters, actually do the SQL insertion.
        /// </summary>

        object InsertSqlExecute(int statementIndex); // returns IDENTITY(1,1) value on insert.

        ///<summary>
        ///  duplicate check before InsertSqlExecute: Does row which meets our primary identity rule (like patientID+accession) already exist in database?
        ///</summary>
        bool IdentityAlreadyExists(IMapping mapping);

        /// <summary>
        /// After  IdentityAlreadyExists, this holds last lookup value.
        /// </summary>
        /// <returns></returns>
        object GetLastIdentity();


        // INSERT parameter-get-info methods
        string GetInsertSqlParamTypeStr(int statementIndex, int paramIndex);
        bool GetInsertSqlParamValueIsNull(int statementIndex, int paramIndex);
        int GetInsertSqlParamCount(int statementIndex);
        string GetInsertSqlParamDebugStr(int statementIndex, int paramIndex);

        /// <summary>
        /// Get the number of null parameters in a given insert statement
        /// </summary>
        /// <param name="statementIndex">statementIndex=0 is the first and primary one. 1..n are secondary ones</param>
        /// <returns></returns>
        int GetInsertSqlParamValueNullCount(int statementIndex);

        /// <summary>
        /// Part of the implementation of a split from one row in firebird side to two or more joined rows in the MS SQL side is to set up a statementIndex>0, another INSERT statement.
        /// It should be safe to invoke this any number of times, and second and later invocations should guarantee they have no effect.
        /// </summary>
        /// <param name="statementIndex">A value greater than 0 is expected</param>
        /// <param name="field"></param>
        void SetupBeforeSubFieldInsert(int statementIndex, IField field);



        // Exception Logging Handlers

        /// <summary>
        /// HandleExceptionLogging should be invoked from a catch clause where we executed some data access layer action (query or command).
        /// It should do internal DAL error handling logic (like storing the failed SQL statement somewhere for reference) and return
        /// whether or not we should re-raise from here (aborting the entire action).
        /// </summary>
        /// <param name="mapping">A set of data to determine what table and fields are moved to what other table and fields</param>
        /// <param name="ex">The failure, probably an SqlException but the general type Exception is used here to avoid coupling</param>
        /// <param name="command">If null then the main data access layer INSERT query is assumed, otherwise the specific command we executed is passed in</param>
        /// <returns>shouldRaise, if true then raise ex, if false, then can decide yourself, or just continue</returns>
        bool HandleExceptionLogging(int statementIndex, IMapping mapping, Exception ex, IDbCommand command); // returns bool shouldRaise
    }

    ///<summary>
    /// Lookup key/value store service definition.
    /// </summary>
    public interface ILookups
    {
        bool TryGetValue(string keyName, out object value);

        void Add(string keyName, object value);
    }

    /// <summary>
    /// IField is an abstraction for an single Field element and its name on the incoming (firebird) and outgoing (ms sql) side.
    /// </summary>
    public interface IField
    {
        int Index { get; set; }
        int QueryIndex { get; set; }
        string InField { get; set; }
        string InDbType { get; set; }
        string OutField { get; set; }
        string OutDbType { get; set; }
        bool InsertOutField { get; set; }
        bool UpdateSetField { get; set; }
        bool NotNullFlag { get; set; }
        bool MasterDetailFlag { get; set; }
        bool SecondaryFlag { get; set; }
        bool Identity { get; set; }
        bool ForeignIdentity { get; set; }
        bool InsertExtJson { get; set; }
        bool PrimaryKeyFlag { get; set; }
        string SelectTable { get; set; }
        string SelectField { get; set; }
        object SelectDefaultValue { get; set; }
        string SelectFieldType { get; set; }
        IDbCommand SelectCommand { get; set; }
        IDbCommand SelectFbCommand { get; set; }
        int GetSubFieldCount();
        void SubFieldsClear();
        IField GetSubField(int index);
        void SubFieldAdd(IField field);

        string GetSubFieldInsertSQL();
        void SetSubFieldInsertSQL(string value);

        string GetSelectFieldActual();
        string ToString();
        bool SpecialFunctionDetect();

        /// <summary>
        /// If zero subfields, return empty string, otherwise return string in form "[SubFieldName1], [SubFieldName2]"
        /// </summary>
        /// <returns>empty string or subfield list string</returns>
        string GetSubFieldNames();

        /// <summary>
        /// If zero subfields, return empty string, otherwise return string in form "@SubFieldName1, @SubFieldName2"
        /// </summary>
        /// <returns>empty string or subfield parameter name string</returns>
        string GetSubFieldParamNames();
    }

    // Both Writers and Data Access Layers need to know the interface IMapping to do their jobs.
    public interface IMapping
    {
        string ToString();
        string CustomFbQuery { get; set; }
        bool Destintidentity { get; set; }
        string FirebirdTableName { get; set; }
        string Inkeyfieldtype { get; }
        string Inkeyfieldname { get; }
        string Outkeyfieldtype { get; }
        string Outkeyfieldname { get; }
        int GetForeignIdentityCounter();
        int GetAllFieldsCount();
        IField GetAllField(int idx);
        int GetSelectedFieldsCount();
        void ResetFlags();
        JObject GetTableDictionary(string firebirdTableName);

        /// <summary>
        ///  For some MSSQL table name, provided return a firebird query, like SELECT TOP 1 PRIMARYKEYFIELD FROM
        ///  SOMEFIREBIRDTABLE WHERE IDENTITYFIELDSTR = @VALUE.  if selectForMssqlTable = "[dbo].[Role]" this should
        ///  return "SELECT FIRST 1 ROLENAME FROM ROLELIST WHERE ROLEID = @VALUE".
        ///  In an IMapping this method is required as part of the implementation of the data access layer's FK-lookup
        ///  features.
        /// </summary>
        /// <param name="selectForMssqlTable"></param>
        /// <returns></returns>
        string GetFieldLookupFbIdentityMapSql(string selectForMssqlTable);

        /// <summary>
        ///     return Firebird table name that is to be mapped to mssql.
        /// </summary>
        /// <param name="mssqlTableName"></param>
        /// <returns></returns>
        string ReverseLookupTableName(string mssqlTableName);

        string GetDefaultExistingItemQuery(string afirebirdtablename);

        /// <summary>
        ///   Get an MSSQL query for an existing item given a firebird table name.  Fetch the custom user written query if present in mapping.json
        ///    or generate a default. See "existing" elements in Mapping.json for examples, like this:
        ///   "PATIENTMODULE": { "existing": "select [InternalPatientID] from [phi].[Patient] WHERE ....", }....
        /// </summary>
        /// <param name="afirebirdtablename">Firebird table name like PATIENTMODULE</param>
        /// <returns>query in form SELECT [ID] from [MSSQLTABLE] where ....</returns>
        string GetExistingItemQuery(string afirebirdtablename);
        string GetFirebirdIdentityFieldListFromDictStr(JObject dict);
        List<string> GetFirebirdIdentityFieldListFromDict(JObject dict);
        string GetFirebirdIdentityFieldListStr(string afirebirdtablename);

        bool GetKeyFieldInfo(string afirebirdtablename,
            out string inkeyfieldtype,
            out string inkeyfieldname,
            out string outkeyfieldtype,
            out string outkeyfieldname);

        IField GetSelectedField(int idx);

        bool IsFaulted();
        int GetSevereErrorCount();
        int GetMinorErrorCount();

        string GetMicrosoftSqlTableName(); // get "destination".


        bool GetMicrosoftSqlTableIntIdentity(); // get "destintidentity".


        string GetMicrosoftSqlUpdateColumns();
        string GetMicrosoftSqlUpdateWhereClause();
        string GetMicrosoftSqlSelectFieldNames(bool addBrackets = true, string prefix = "  ");
        string GetMicrosoftSqlValueNames();

        /// <summary>
        /// Be careful with this confusing piece of code.  It searches output fields then input fields.
        /// </summary>
        int FindOutOrInFieldIndex(string fieldName);

        int FindInFieldIndex(string inFieldName);
        IField FindOutField(string outFieldName);

        /// <summary>
        ///     If I pass in [ABC] or abc, and there is an outField value named Abc in
        ///     _fieldsMap, then find it, and return index, otherwise return -1
        /// </summary>
        int FindOutgoingField(string fieldName);

        string GetOutputInserted();

        void MapFields(IDataReader fbReader,
            IColumnInfo sqlColumnsData,
            SingleDefaultValue single);

        string BuildStatement(StatementType statementType);

        int BuildSubInsertStatements();
    }



    public enum CheckBeforeInsertAction
    {
        Continue,
        Abort,
        Skip
    };
    // return  CheckBeforeInsertAction.Continue = default.

    /// <summary>
    /// This interface depends on base-interfaces that might be ExportMappedField and ExportMapping
    /// but for testability we make it use the base classes.
    /// </summary>
    public interface IExportWriterOwner
    {
        object InsertRowAndFetchIdentity(string firebirdTableName, object incomingValue, IField field);

        void SetContextRow(int rowNumber);


        // Validation Checkpoint (STUB) methods. If something is bad, throw an exception or log something.

        void ChecksBeforeExecuteOneRow(int paramCount); // validate that parameter count and firebird field count are acceptable.

        void ChecksAfterParameterSetValue(object context, CopyType type, int paramIndex, IField field, int indexIncoming, object value);

        /// <summary>
        ///  The writer must be able to ask some parent object to check for some case like, an existing item that should be skipped
        ///  or a fault condition that requires us to stop the entire conversion operation.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mapping"></param>
        /// <param name="dal"></param>
        /// <returns>enum value CheckBeforeInsertAction of Continue, Abort, or Skip</returns>
        CheckBeforeInsertAction ChecksBeforeInsertIdentity(object context, IMapping mapping, IDataAccessLayer dal);

        void ChecksAfterInsertIdentity(object context, IMapping mapping, IDataAccessLayer dal, object newIdentity);

    }

}