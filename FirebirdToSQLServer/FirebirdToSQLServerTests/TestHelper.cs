using System.Linq;

namespace Ramsoft.TestFixtures
{
    public class TestHelper
    {
        public static string StripCommentsAndWhiteSpace(string sqlExpression)
        {
            // strip first line if it starts with --
            string temp = sqlExpression.Trim();
            int dashPos = temp.IndexOf("--");
            if (dashPos == 0)
            {
                int nlPos = temp.IndexOf('\n');
                temp = temp.Substring(nlPos + 1); // strip first line.
            }

            // LINQ to the rescue for whitespace normalization.
            var list = temp.Replace('\n', ' ').
                Split(' ').
                Where(s => !string.IsNullOrWhiteSpace(s));
            return string.Join(" ", list);
        }
    }
}