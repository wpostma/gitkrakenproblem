﻿using System.Data;
using Moq;
using Newtonsoft.Json.Linq;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;

namespace FirebirdToSQLServerTests.Fixture
{
    public class BaseFixture
    {
        public JObject Mapdict;
        public ExportMapping Mapping;
        public Mock<IDataReader> MockFbReader; // supports IDataReader
        public Mock<IColumnInfo> MockSqlColumnInfo; // supports IDataReader


        public string TableName;

        public BaseFixture(string jsonsnippet, string tableName)
        {
            TableName = tableName;
            Mapdict = JObject.Parse(jsonsnippet); // will raise  Newtonsoft.Json.JsonReaderException if invalid.
            Mapping = new ExportMapping(tableName, Mapdict);


            // if we want to introduce the map to a reader, we need to mock the
            // data readers, for this type of test fixture.
            MockFbReader = new Mock<IDataReader>(); // supports IDataReader
            //MockSqlReader = new Mock<IDataReader>();  // supports IDataReader

            MockSqlColumnInfo = new Mock<IColumnInfo>(); // supports IDataReader
        }
    }


    // helpers
}