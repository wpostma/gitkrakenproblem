using System;

namespace RamSoft.FirebirdToSqlServer.Util
{
    public class LogEventArgs : EventArgs
    {
        public LogLevel LogLogLevel;

        public string Message;

        public object Context;

        public LogEventArgs(LogLevel logLevel, string message, object context  = null)
        {
            LogLogLevel = logLevel;
            Message = message;
            Context = context;
        }
    }
}