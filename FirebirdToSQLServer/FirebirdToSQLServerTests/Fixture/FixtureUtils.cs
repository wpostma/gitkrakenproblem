﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Moq;
using RamSoft.FirebirdToSqlServer.Interface;


namespace FirebirdToSQLServerTests.Fixture
{
    class FixtureUtils
    {

        public static Mock<IExportWriterOwner> MockOwner()
        {
            var mock = new Mock<IExportWriterOwner>();
            return mock;
        }


        public static Mock<IDataAccessLayer> MockDal(IDataReader fbreader, IColumnInfo sqlinfo)
        {
            var mock = new Mock<IDataAccessLayer>();
            if (fbreader != null)
            {
                mock.Setup(t => t.FirebirdColumnReader()).Returns(fbreader);
            }
            if (sqlinfo != null)
            {
                mock.Setup(t => t.GetSqlColumnInfo(null)).Returns(sqlinfo);
            }
            return mock;
        }
    }
}
