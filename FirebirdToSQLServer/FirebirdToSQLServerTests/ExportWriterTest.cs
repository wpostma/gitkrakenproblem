﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FirebirdToSQLServerTests.Fixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using Moq;
using Newtonsoft.Json.Linq;
using Ramsoft.TestFixtures;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Util;

namespace FirebirdToSQLServerTests
{
    /// <summary>
    /// Tests for ExportWriterTest that do not depend on any real data layer (minimal mocked data layer).
    ///  We can check that the main loop, startup and shutdown of the writer behave as expected.
    /// </summary>
    [TestClass]

    public class ExportWriterTest
    {


        public ExportWriterTest()
        {
            // TODO: Add constructor logic here

        }


        private const string FmtTemplate1 = @"{{
                      ""{0}"": {{
                        ""destination"": ""{1}"",
                        ""oldkey"": [ ""int"", ""SOMEOLDKEY"" ],
                        ""newkey"": [ ""bigint"", ""SomeNewKey"" ],
                        ""identity"": ""ITEMNAMEIN"",
                        ""fields"": [
                          [ ""ITEMNAMEIN"", ""[ItemNameOut]"" ] ,
                          {2} ,
                          [ ""*"", ""[ExtJSON]"" ]
                        ]
                      }}{3}

                }}";

        private static string FakeTableWithFieldStr(string srcTableName = "FIREBIRDTABLE",
        string destTableName = "[dbo].[MSSqlTable]",
        string fieldDeclaration = "[ \"A\", \"[B]\" ]",
        string otherTables = "")
        {
            return string.Format(FmtTemplate1, srcTableName, destTableName, fieldDeclaration, otherTables);
        }

        protected ExportMapping fakeMapping(string tableName = "FAKE")
        {
            string mapDictStr = FakeTableWithFieldStr(tableName);
            var mapDict = JObject.Parse(mapDictStr);
            var fake = new ExportMapping(tableName, mapDict);
            return fake;
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        [Category("Controllers")]
        public void TestExportWriterBasics()
        {
            var owner = FixtureUtils.MockOwner();


            // The Writer which is under test has a relationship with base stuff we call
            // the fixture, and with the DAL, and lives between them like this:
            // FIXTURE --> [Writer] --> DAL
            // everything the writer needs except a data access layer
            var fixture = new SimpleFacilitiesFixture();
            var dal = FixtureUtils.MockDal(fixture.MockFbReader.Object,fixture.MockSqlColumnInfo.Object);


            // This would blow up if no column reader is returned
            var writer = new ExportWriter(owner.Object, dal.Object, fixture.Mapping, 100,null);

            var str = writer.ToString();
            var expectStr = "{ ExportWriter FACILITIES:[dbo].[Facility] }";
            Assert.AreEqual(expectStr,str); // makes knowing Which writer we're looking at easier.


            // ExportWriter must call dataAccessLayer.SetupBeforeMapping(mapping,null)
            dal.Verify(v => v.SetupBeforeMapping(fixture.Mapping, null),Times.Once());

            // ExportWriter must set up the insert SQL.
            dal.Verify(v => v.SetupInsertSql(It.IsAny<string>()), Times.Once());

        }



        [TestMethod]
        [Category("Controllers")]
        public void TestExportWriterExecuteZeroFacilityRows()
        {
            var owner = FixtureUtils.MockOwner();

            // We don't want to get into the inner loop of writer execute, if we did, throw Not Implemented exception.
            owner.Setup(o => o.SetContextRow(0)).Throws<NotImplementedException>();


            // The Writer which is under test has a relationship with base stuff we call
            // the fixture, and with the DAL, and lives between them like this:
            // FIXTURE --> [Writer] --> DAL
            // everything the writer needs except a data access layer
            var fixture = new SimpleFacilitiesFixture();
            var dal = FixtureUtils.MockDal(fixture.MockFbReader.Object, fixture.MockSqlColumnInfo.Object);


            // This would blow up if no column reader is returned
            var writer = new ExportWriter(owner.Object, dal.Object, fixture.Mapping, 100, null);

            // ExportWriter must call dataAccessLayer.SetupBeforeMapping(mapping,null)
            dal.Verify(v => v.SetupBeforeMapping(fixture.Mapping, null), Times.Once());

            // ExportWriter must set up the insert SQL.
            dal.Verify(v => v.SetupInsertSql(It.IsAny<string>()), Times.Once());

            // ExportWriter must not call dataAccessLayer.SetupBeforeExecute(0,mapping) before Execute
            dal.Verify(v => v.SetupBeforeExecute(0, fixture.Mapping), Times.Never);

            // We have not mocked ability to read any rows, so this should test the case where
            // we do not go into the inner loop at all, and we should return zero.
            int ret = writer.Execute();
            Assert.AreEqual(0,ret);

            // ExportWriter must call dataAccessLayer.SetupBeforeExecute(0,mapping) during Execute
            dal.Verify(v => v.SetupBeforeExecute(0,fixture.Mapping), Times.Once());


        }



        [TestMethod]
        [Category("Controllers")]

        public void TestExportWriterExecuteFiveFacilityRows()
        {
            const int maxRowCount = 5;
            var owner = FixtureUtils.MockOwner();

            // The Writer which is under test has a relationship with base stuff we call
            // the fixture, and with the DAL, and lives between them like this:
            // FIXTURE --> [Writer] --> DAL
            // everything the writer needs except a data access layer
            var fixture = new SimpleFacilitiesFixture();
            var dal = FixtureUtils.MockDal(fixture.MockFbReader.Object, fixture.MockSqlColumnInfo.Object);

            // infinite dataset.
            dal.Setup(d => d.FirebirdRead()).Returns(true);

            // one parameter, so we know if the writer actually does something with parameters, which is kind of the entire point of the writer.
            dal.Setup(d => d.GetInsertSqlParamCount(0)).Returns(1);
            dal.Setup(d => d.FirebirdReaderGetFieldCount()).Returns(1);


            // This would blow up if no column reader is returned
            var writer = new ExportWriter(owner.Object, dal.Object, fixture.Mapping, maxRowCount, null);

            // ExportWriter must call dataAccessLayer.SetupBeforeMapping(mapping,null)
            dal.Verify(v => v.SetupBeforeMapping(fixture.Mapping, null), Times.Once());

            // ExportWriter must set up the insert SQL.
            dal.Verify(v => v.SetupInsertSql(It.IsAny<string>()), Times.Once());

            // ExportWriter must not call dataAccessLayer.SetupBeforeExecute(0,mapping) before Execute
            dal.Verify(v => v.SetupBeforeExecute(0, fixture.Mapping), Times.Never);

            // We have not mocked ability to read any rows, so this should test the case where
            // we do not go into the inner loop at all, and we should return zero.
            int ret = writer.Execute();
            Assert.AreEqual(maxRowCount, ret);

            // ExportWriter must call dataAccessLayer.SetupBeforeExecute(0,mapping) during Execute
            dal.Verify(v => v.SetupBeforeExecute(0, fixture.Mapping), Times.Once());


        }





        [TestMethod]
        [Category("Controllers")]

        public void TestExportWriterExecuteFivePatientStudyRows()
        {
            const int maxRowCount = 5;
            var owner = FixtureUtils.MockOwner();

            // This fixture includes the writer and all its Dependant parts or some mock of them
            // including a fake DAL and mock Firebird and SQL column info readers.
            var fixture = new PatientStudyWriterFixture();
            fixture.SetupWriter(100, null); // fixture should eventually contain some repeatable data sequence generator.
            //fixture.Dal.FirebirdReaderFieldCount = 5;

            // ExportWriter must call dataAccessLayer.SetupBeforeMapping(mapping,null)
            //fixture.Dal.Verify(v => v.SetupBeforeMapping(fixture.Mapping, null), Times.Once());

            // ExportWriter must set up the insert SQL.
            //fixture.Dal.Verify(v => v.SetupInsertSql(It.IsAny<string>()), Times.Once());

            // ExportWriter must not call dataAccessLayer.SetupBeforeExecute(0,mapping) before Execute
            //fixture.Dal.Verify(v => v.SetupBeforeExecute(0, fixture.Mapping), Times.Never);

            // We have not mocked ability to read any rows, so this should test the case where
            // we do not go into the inner loop at all, and we should return zero.
            int ret = fixture.Writer.Execute();
            Assert.AreEqual(maxRowCount, ret);

            // ExportWriter must call dataAccessLayer.SetupBeforeExecute(0,mapping) during Execute
            //fixture.Dal.Verify(v => v.SetupBeforeExecute(0, fixture.Mapping), Times.Once());


        }

    }
}
