﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace RamSoft.FirebirdToSqlServer
{
    public enum TreeType {  Table, Warning, Error, Success, Failure, Info }


    internal class SafeNativeMethods
    {

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage
            (IntPtr hWnd, UInt32 msg, UIntPtr wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

    }

    /// <summary>
    /// Provides textual cues to a text box, as an extension class.
    /// </summary>
    public static class TextBoxExtensionClass
    {
        // ReSharper disable once InconsistentNaming
        private const int EM_SETCUEBANNER = 0x1501;

        /// <summary>
        /// Sets the watermark (cue) text for a text box.
        /// </summary>
        /// <param name="textBox">extension</param>
        /// <param name="cue">The cue text</param>
        public static void SetCue
            (this TextBox textBox,
                string cue)
        {
            SafeNativeMethods.SendMessage(textBox.Handle, EM_SETCUEBANNER, (UIntPtr) 0, cue);
        }
    }


}