@echo off
@echo Running FirebirdToSqlXIntegrationTests with vstest.console.
set OLDDIR=%CD%
if not defined DevEnvDir ( call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat" )
REM cd FirebirdToSqlIntegrationTest\bin\Debug\
REM mstest /testcontainer:FirebirdToSqlXIntegrationTests.dll 
vstest.console.exe FirebirdToSqlIntegrationTest\bin\Release\FirebirdToSqlXIntegrationTests.dll 
CD %OLDDIR%