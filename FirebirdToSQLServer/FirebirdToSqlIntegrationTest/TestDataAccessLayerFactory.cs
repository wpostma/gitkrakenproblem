using System.Data.SqlClient;
using FirebirdSql.Data.FirebirdClient;
using RamSoft.FirebirdToSqlServer.ConversionEngine;
using RamSoft.FirebirdToSqlServer.Interface;

namespace RamSoft.FirebirdToSqlServer
{
    public class TestDataAccessLayerFactory : DataAccessLayerFactory
    {
        public object IntegrationTestEnv;

        public override IDataAccessLayer CreateDataAccessLayer()
        {
            return new TestDataAccessLayer(GetFirebirdConnection() as FbConnection,
                GetMssqlConnection() as SqlConnection,
                IntegrationTestEnv,
                this);
        }

        public TestDataAccessLayerFactory(string fbConnStr, string msSqlConnStr) : base(fbConnStr, msSqlConnStr)
        {
            //
        }

    }
}