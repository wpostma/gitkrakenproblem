﻿using System;
using System.Windows.Forms;

// Utility to read RamSoft PACS Firebird database
// and export certain data to SQL Server database
// database.
//
// See main logic in Conversion.cs and
// ExportWriter.cs.
//
// Mapping.json must contain complete and correct information
// for this tool to work.  When foreign keys create dependencies,
// and you want to move a table, like ther study access log table
// from Firebird to MS SQL, we build a chain of dependencies
// and move all the rows from the one side to the other.

namespace RamSoft.FirebirdToSqlServer
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application, opens MainForm which has a SqlToDocumentDbConversion object inside.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
