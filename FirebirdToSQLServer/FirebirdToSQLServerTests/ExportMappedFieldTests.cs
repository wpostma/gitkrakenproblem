﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using RamSoft.FirebirdToSqlServer.Model;

namespace FirebirdToSQLServerTests
{
    /// <summary>
    /// Test ExportMappedField
    /// </summary>
    [TestClass]
    public class ExportMappedFieldTests
    {
        public ExportMappedFieldTests()
        {

            // TODO: Add constructor logic here

        }

        /*
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        */
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestCategory("Model")]
        [TestMethod]
        public void TestGetSubFieldNames()
        {
            // IField -> GetSubFieldNames()
            var field = new ExportMappedField();
            field.InField = "IN"; // firebird
            field.OutField = "Out"; // mssql
            field.SelectTable = "LookupTableName"; // mssql

            var subfield1 = new ExportMappedField();
            subfield1.InField = "SUBIN1";
            subfield1.OutField = "SUBOUT1";
            field.SubFieldAdd(subfield1);

            var subFieldNamesActual1 = field.GetSubFieldNames();
            var subFieldNamesExpected1 = "[SUBOUT1]";
            Assert.AreEqual(subFieldNamesExpected1, subFieldNamesActual1);

            var subfield2 = new ExportMappedField();
            subfield2.InField = "SUBIN2";
            subfield2.OutField = "SUBOUT2";
            field.SubFieldAdd(subfield2);

            var subFieldNamesActual2 = field.GetSubFieldNames();
            var subFieldNamesExpected2 = "[SUBOUT1], [SUBOUT2]";
            Assert.AreEqual(subFieldNamesExpected2, subFieldNamesActual2);




        }

        [TestCategory("Model")]
        [TestMethod]
        public void TestGetSubFieldParamNames()
        {
            // IField -> GetSubFieldParamNames()


            var field = new ExportMappedField();
            field.InField = "IN"; // firebird
            field.OutField = "Out"; // mssql
            field.SelectTable = "LookupTableName"; // mssql

            var subfield1 = new ExportMappedField();
            subfield1.InField = "SUBIN1";
            subfield1.OutField = "SUBOUT1";
            field.SubFieldAdd(subfield1);

            var subFieldParamNamesActual1 = field.GetSubFieldParamNames();
            var subFieldParamNamesExpected1 = "@SUBOUT1";
            Assert.AreEqual(subFieldParamNamesExpected1, subFieldParamNamesActual1);

            var subfield2 = new ExportMappedField();
            subfield2.InField = "SUBIN2";
            subfield2.OutField = "SUBOUT2";
            field.SubFieldAdd(subfield2);

            var subFieldParamNamesActual2 = field.GetSubFieldParamNames();
            var subFieldParamNamesExpected2 = "@SUBOUT1, @SUBOUT2";
            Assert.AreEqual(subFieldParamNamesExpected2, subFieldParamNamesActual2);

        }
    }
}
