﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using FirebirdSql.Data.FirebirdClient;
using RamSoft.FirebirdToSqlServer.Exceptions;
using RamSoft.FirebirdToSqlServer.Interface;
using RamSoft.FirebirdToSqlServer.Model;
using RamSoft.FirebirdToSqlServer.Util;

namespace RamSoft.FirebirdToSqlServer.ConversionEngine
{
    public class DataAccessLayer : IDataAccessLayer, IDisposable
    {
        // Physical connection stuff.
        private readonly FbConnection _fbConnection;
        private readonly SqlConnection _sqlConnection;
        private FbCommand _fbCommand;
        private FbDataReader _fbReader;


        // Dictionary to make field lookups fast
        private Dictionary<string, int> _fbReaderFieldLookup;
        // Fast DataReader field lookup: string fieldname -> int index

        string _sqlTableName;
        IColumnInfoCache _sqlColumnInfo;



        private List<SqlCommand> _sqlCommandIns; // first element in the list is the primary row to insert. secondary elements are elements 1..n

        private SqlCommand _sqlCommandExist;

        private string _lastSqlExcept = string.Empty; // keep SQL that caused exception.

        // last existing item check identity
        public object LastIdentity { get; set;  }

        public object GetLastIdentity()
        {
            return LastIdentity;
        }

        public DataAccessLayer(FbConnection fbConnection,
            SqlConnection sqlConnection,
            IColumnInfoCache sqlColumnInfo

            )
        {
            _fbConnection = fbConnection;
            _sqlConnection = sqlConnection;
            _sqlColumnInfo = sqlColumnInfo;

        }

        public string GetFailedSql()  // diagnostics and exception handling.
        {

            return _lastSqlExcept;
        }

        public string GetParamValueAsStr(IDbCommand dbCommand, int index) // diagnostics and exception handling
        {
            // ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
            if (dbCommand is DbCommand)
            {
                var cmd = (DbCommand) dbCommand;
                if (index >= 0 && index < cmd.Parameters.Count)
                {
                    return StrHelpers.NameOfObjectStr(cmd.Parameters[index].Value);
                }
                else
                    return index.ToString()+"?";


            }
            else
                return "X?";

        }

        public bool Commit(int statementIndex)
        {
            return DoCommit(statementIndex);
        }


        public void SetupBeforeMapping(IMapping mapping, SingleDefaultValue single)
        {
            DoSetupBeforeMapping(mapping, single);
        }


        public bool SetupFieldSelectCommandQuery(IMapping mapping,
            DataReaderQueryType aType, IField field, object incomingValue)
        {
            return DoSetupFieldSelectCommandQuery(mapping, aType, field, incomingValue);
        }

        /// <summary>
        ///     To be called by a Writer before it wants the Data Access Factory to write an MS SQL rows using defaults.
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns></returns>
        public int SetupBeforeWriteRowWithDefaults(IMapping mapping)
        {
            // validation:
            if (_sqlCommandIns == null)
            {
                throw new WriterFailException("Internal failure: SQL-insert command missing");
            }


            _sqlCommandIns[0].Parameters.Clear();
            string outfield;

            int mappedFieldCount = mapping.GetSelectedFieldsCount();
            // BEWARE: can contain the same field mapped more than once, so if there are 10 fields to copy, and one is copied twice, this could be 11!

            for (var paramIndex = 0; paramIndex < mappedFieldCount; paramIndex++)
            {
                outfield = mapping.GetSelectedField(paramIndex).OutField;
                if (string.IsNullOrEmpty(outfield))
                {
                    throw new WriterFailException(
                        "Data Access Layer Internal failure: mapping information missing outField value");
                }
                _sqlCommandIns[0].Parameters.Add(new SqlParameter("@" + StrHelpers.RemoveBrackets(outfield),
                    /*value set later*/DBNull.Value));
            }


            if (_sqlCommandIns[0].Parameters.Count == 0)
            {
                throw new WriterFailException(
                    "Data Access Layer Internal failure: insert command missing its parameters");
            }


            return _sqlCommandIns[0].Parameters.Count;
        }

        public bool InsertSqlParamValue(int statementIndex, int paramIndex, object value)
        {
            if (value == null)
                value = DBNull.Value;

            try
            {
                _sqlCommandIns[statementIndex].Parameters[paramIndex].Value = value;
                if (ObjHelper.ValueNullOrDbNull(value))
                {
                    Log.Write(LogLevel.LFine,
                        "INSERT PARAM " + _sqlCommandIns[statementIndex].Parameters[paramIndex].ParameterName +
                        " is null.");
                }
                return value != null;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                var msg = string.Format( "Data Access Layer INSERT PARAM value out of range: {0},{0}. "+ex.Message,statementIndex, paramIndex);
                Log.Write(LogLevel.LError,msg);
                throw new WriterFailException(msg);
            }

        }


        public virtual object InsertSqlExecute(int statementIndex)
        {
            if (_sqlCommandIns == null)
            {
                throw new WriterFailException("Data Access Layer Internal failure: SQL-insert command missing");
            }


            return _sqlCommandIns[statementIndex].ExecuteScalar(); // was ExecuteNonQuery();
        }

        ///<summary>
        ///  duplicate check before InsertSqlExecute: Does row which meets our primary identity rule (like patientID+accession) already exist in database?
        ///</summary>
        public bool IdentityAlreadyExists(IMapping mapping)
        {
            const int statementIndex = 0; // todo maybe flexible?
            if (!SetupExistingSql(statementIndex,mapping)) // first time we need this query it is created for us and initialized.
                return false; // skip identity checking because Mapping.json told me to.

            bool firstRead;
            // LastIdentity one layer up in Writer class should get the LastIdentity here.
            LastIdentity = null;
            using (var reader = _sqlCommandExist.ExecuteReader())
            {

                firstRead = reader.Read();
                if (firstRead)
                {
                    // primaryKeyIndex is always first thing in select statement.
                    LastIdentity = reader.GetValue( 0);
                }

                // reader.Close(); <-- IDisposable does this for us.
            }
            return firstRead;
        }



        public int SetupBeforeExecute(int statementIndex, IMapping mapping) // returns paramcount.
        {
            // validation:
            if (_sqlCommandIns == null)
            {
                throw new WriterFailException("Data Access Layer Internal failure: SQL-insert command missing");
            }

            int mappedFieldCount = mapping.GetSelectedFieldsCount();
            // BEWARE: can contain the same field mapped more than once, so if there are 10 fields to copy, and one is copied twice, this could be 11!

            /* Unlike delphi this kind of db control doesn't auto-populate parameters */
            _sqlCommandIns[statementIndex].Parameters.Clear();
            for (var paramIndex = 0; paramIndex < mappedFieldCount; paramIndex++)
            {
                _sqlCommandIns[statementIndex].Parameters.Add(
                    new SqlParameter("@" + StrHelpers.RemoveBrackets(mapping.GetSelectedField(paramIndex).OutField),
                        /*value set later*/DBNull.Value));
            }




            /* If we get here, and we're in single-case, there should be a parameter that is mapped to the values from SingleDefaultValue */


            int acount = _sqlCommandIns[statementIndex].Parameters.Count;
            if ((acount == 0) || (acount < mappedFieldCount))
            {
                throw new WriterFailException(
                    "Data Access Layer Internal failure: insert command missing its parameters");
            }
            return _sqlCommandIns[statementIndex].Parameters.Count;
        }


        public IColumnInfo GetSqlColumnInfo(string sqlTableName = null)
        {
            // Get RamSoft.FirebirdToSqlServer.Model.ColumnInfo, list of string DataTypeName, string Name.
            // Find in dictionary first
            if (sqlTableName == null)
            {
                sqlTableName = _sqlTableName;
            }
            return _sqlColumnInfo.GetInfoForTable(sqlTableName);
        }



        public int SetupInsertSql(string insertSql)
        {
            if (_sqlCommandIns == null)
            {
                if (_sqlConnection == null)
                    throw new WriterFailException(
                        "Data Access Layer failure: _sqlConnection is missing, can not set up and execute INSERT statements");

                _sqlCommandIns = new List<SqlCommand>();
            }

            int index = _sqlCommandIns.Count;
            _sqlCommandIns.Add(new SqlCommand(insertSql, _sqlConnection));
            return index;

        }
        /// <summary>
        ///  Inside the data access layer when asked if an equivalent row to the one we are about to insert already exists we must do a check.
        ///  Setting up the SQL Query object and setting its parameter values is the job of this little helper.
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public bool SetupExistingSql(int statementIndex, IMapping mapping)
        {
            if (_sqlCommandExist == null)
            {
                var existSql = mapping.GetExistingItemQuery(mapping.FirebirdTableName);
                if (string.IsNullOrEmpty(existSql))
                    return false;
                //OR?    throw new WriterFailException(
                //    "Data Access Layer failure: SetupExistingSql can not check if rows exist because existing item query logic failed");

                if (_sqlConnection == null)
                    throw new WriterFailException(
                        "Data Access Layer failure: _sqlConnection is missing, can not set up and execute INSERT statements");

                _sqlCommandExist = new SqlCommand(existSql, _sqlConnection);


            }
            // now update parameters.
            _sqlCommandExist.Parameters.Clear();
            foreach (SqlParameter paramItem in _sqlCommandIns[statementIndex].Parameters)
            {
                _sqlCommandExist.Parameters.AddWithValue(paramItem.ParameterName, paramItem.Value);
            }



            return true; // success.
        }


        public object FirebirdReaderGetValue(int indexIncoming)
        {
            return _fbReader.GetValue(indexIncoming);
        }

        public string FirebirdReaderGetColumnType(int indexIncoming)
        {
            return _fbReader.GetDataTypeName(indexIncoming);
        }

        public string FirebirdReaderGetValueByName(string fieldname)

        {
            int indexIncoming = _fbReaderFieldLookup[fieldname];
            var value = _fbReader.GetValue(indexIncoming);
            if ((value == null) || (value == DBNull.Value))
            {
                return null;
            }
            return (string) value;
            ;
        }

        public Dictionary<string, object> FirebirdReaderJsonSerializeRowFull()
        {
            return JsonHelpers.SerializeRowFull(_fbReader); // Create dictionary
        }


        public int FirebirdReaderGetFieldCount()
        {
            return _fbReader.FieldCount;
        }


        public string FirebirdLookupEntityName(IMapping mapping,
            IField field, object incomingValue)
        {
            string fbSql;
            var incomingValueEntityName = "";
            if (field.SelectFbCommand == null)
            {
                // fbSql=	"SELECT FIRST 1 MODALITYNAME FROM MODALITIES WHERE MODALITYID = @VALUE"
                // or "SELECT FIRST 1 GROUPNAME FROM GROUPLIST WHERE GROUPID = @VALUE", and VALUE is some integer placed in parameter[0].value
                fbSql = mapping.GetFieldLookupFbIdentityMapSql(field.SelectTable);
                if (string.IsNullOrEmpty(fbSql))
                {
                    Log.Write(LogLevel.LError, mapping.FirebirdTableName + ": Unable to remap " + field.InField);
                    return "";
                }
                var fbCommand = new FbCommand(fbSql, _fbConnection);
                field.SelectFbCommand = fbCommand;
                if (incomingValue == null)
                    incomingValue = DBNull.Value;
                fbCommand.Parameters.AddWithValue("@VALUE", incomingValue);
            }
            else
            {
                var fbCommand = (FbCommand) field.SelectFbCommand;
                fbSql = fbCommand.CommandText;
                if (incomingValue == null)
                    incomingValue = DBNull.Value;
                fbCommand.Parameters[0].Value = incomingValue;
            }

            try
            {
                using (var selectFbReader = field.SelectFbCommand.ExecuteReader())
                {
                    if (selectFbReader.Read())
                    {
                        // Expect an entity name like ADMIN or SYSTEM:
                        incomingValueEntityName = (string) selectFbReader.GetValue(0);
                    }
                }
                ;
            }
            catch (FbException ex)
            {
                throw new DataAccessLayerException("FB Lookup Failed: " + fbSql, ex);
            }
            catch (FormatException ex)
            {
                // If we get here, a common cause is a query like this:
                // SELECT FIRST 1 USERNAME FROM USERLIST WHERE USERID = @VALUE
                // If field.SelectFbCommand.Parameters[0] value is string we would get this exception.
                // The query is constructed for integer lookup, but we only gave it a string.
                if (incomingValue is string)
                    throw new DataAccessLayerException("FB Lookup Failed / String Value Unexpected: " + fbSql, ex);
            }


            if (string.IsNullOrEmpty(incomingValueEntityName))
            {
                incomingValueEntityName = (string) field.SelectDefaultValue;
            }

            Log.Write(LogLevel.LFine, "LookupFB:" + fbSql);
            Log.Write(LogLevel.LFine, "Value:" + StrHelpers.NameOfObjectStr(incomingValue));

            return incomingValueEntityName;
        }

        public bool FirebirdRead()
        {
            return _fbReader.Read();
        }



        public IDataReader FirebirdColumnReader()
        {
            return _fbReader;
        }


        public string GetInsertSqlParamTypeStr(int statementIndex, int paramIndex)
        {
            return _sqlCommandIns[statementIndex].Parameters[paramIndex].SqlDbType.ToString();
        }

        public bool GetInsertSqlParamValueIsNull(int statementIndex, int paramIndex)
        {
            return ObjHelper.ValueNullOrDbNull(_sqlCommandIns[statementIndex].Parameters[paramIndex].Value);
        }

        public int GetInsertSqlParamCount(int statementIndex)
        {
            return _sqlCommandIns[statementIndex].Parameters.Count;
        }

        public string GetInsertSqlParamDebugStr(int statementIndex, int paramIndex)
        {
            return StrHelpers.NameOfObjectStr(_sqlCommandIns[statementIndex].Parameters[paramIndex].Value);
        }

        public string GetLastInsertSql(int statementIndex)
        {
            return GetLastInsertSqlEx(statementIndex,"");
        }

        public bool HandleExceptionLogging(int statementIndex, IMapping mapping, Exception ex, IDbCommand command)
            // returns shouldRaise
        {
            if (ex is SqlException)
            {
                var exsql = ex as SqlException;
                var constraintFailure = false;
                //var nullCheckFailure = false;
                Log.Write(LogLevel.LWarning, mapping.FirebirdTableName + ": SQL insert id " +
                                          StrHelpers.NameOfObjectStr(_sqlCommandIns[statementIndex].Parameters[0].Value) + " into " +
                                          mapping.GetMicrosoftSqlTableName() + ":" + ex.Message);

                var fieldNameBad = "";

                foreach (SqlError singleError in exsql.Errors)
                {
                    Log.Write("Line " + singleError.LineNumber + ":" + singleError.Message);
                    if (singleError.Number == SqlConsts.ConstraintViolation)
                    {
                        if (ex.Message.Contains("FOREIGN KEY"))
                        {
                            int posColumn = ex.Message.IndexOf("column '") + 8;
                            int posColumn2 = ex.Message.IndexOf("'", posColumn + 3);
                            fieldNameBad = ex.Message.Substring(posColumn, posColumn2 - posColumn);

                            Log.Write(LogLevel.LWarning,
                                mapping.FirebirdTableName + ": Value of field " + fieldNameBad + " is not valid. ");
                        }
                        else
                        {
                            constraintFailure = true;
                        }
                    }
                    if (singleError.Number == SqlConsts.UniqueIndexValueConstraintViolation)
                    {
                        constraintFailure = true;
                        // something like Cannot insert duplicate key row in object 'PHI.Patient' with unique index 'IX_IssuerPatientID'. The duplicate key value is (PETE, 262059212).
                    }
                    else if (singleError.Number == SqlConsts.PrimaryKeyConstraintViolation)
                    {
                        constraintFailure = true;
                    }
                    else if (singleError.Number == SqlConsts.NullCheckViolation)
                    {
                        //nullCheckFailure = true;
                        Log.Write(LogLevel.LInfo, "Null Check Violation / " + mapping.FirebirdTableName);
                    }
                } //foreach.

                string cmd;
                if (command == null)
                {
                    cmd = GetLastInsertSqlEx(statementIndex,fieldNameBad);
                }
                else
                {
                    cmd = GetLastSqlFromCommand(command);
                }
                Log.Write(LogLevel.LInfo, cmd);
                _lastSqlExcept = cmd;
                //Log.Write(ex.Data.Values.ToString());

                return !constraintFailure;
            }
            else
                return false;

        }

        public int GetInsertSqlParamValueNullCount(int statementIndex)
        {
            var nullCount = 0;
            for (var i = 0; i < _sqlCommandIns[statementIndex].Parameters.Count; i++)
            {
                if (ObjHelper.ValueNullOrDbNull(_sqlCommandIns[statementIndex].Parameters[i].Value))
                    nullCount++;
            }
            return nullCount;
        }

        public void SetupBeforeSubFieldInsert(int statementIndex, IField field)
        {
            // Get the existing INSERT statement from the field and place it into _sqlCommandIns list, at some
            // index other than zero, the field and its subfield declarations should be enough to set up the data
            // access layer.
            if (statementIndex < 1)
                throw new ArgumentException("SetupBeforeSubFieldInsert requires statementIndex >= 1");
            if (_sqlCommandIns.Count > statementIndex) // if already done, return.
                return;
            if (_sqlCommandIns.Count == statementIndex)
            {
                // insertSql=  "INSERT INTO [dbo].[Table] table (col1,col2) VALUES (@col1,@col2)"
                var insertSql = field.GetSubFieldInsertSQL(); // generating insert SQL is the job of a mapping object.
                if (string.IsNullOrEmpty(insertSql))
                {
                    throw new ArgumentException(
                        "SetupBeforeSubFieldInsert requires field.SubFieldInsertSQL value to exist");

                }
                var insertSqlCmd = new SqlCommand(insertSql);
                // Add parameters
                for (var i = 0; i < field.GetSubFieldCount(); i++)
                {
                    var paramName = "@" + field.GetSubField(i).OutField; // @FIELD1
                    insertSqlCmd.Parameters.AddWithValue(paramName,null);


                }


                _sqlCommandIns.Add(insertSqlCmd); // next time someone calls  SetupBeforeSubFieldInsert this won't be executed or we'll get duplicates.
            }
            else
                throw new ArgumentException(
                    "SetupBeforeSubFieldInsert requires statementIndex values to be generated in order");


        }


        public void Dispose()
        {
            Dispose(false);
        }

        private void Dispose(bool managed)
        {   DoCommit(0);

            _sqlCommandExist?.Dispose();
            _fbCommand?.Dispose();

            if (_sqlCommandIns != null)
            {
                foreach (var item in _sqlCommandIns)
                {
                    item.Dispose();
                }
            }

            _sqlConnection?.Dispose(); //  dispose of per writer connection.
            // Do not dispose _fbConnection, it's a shared read-only connection.
        }

        private bool DoCommit(int statementIndex)
        {
            // We are not using transactions yet.
            /*
            if ((_sqlCommandIns != null) && (_sqlCommandIns[statementIndex].Transaction != null))
            {
                Log.Write("*********** COMMIT TRANSACTION *********** ");
                _sqlCommandIns[].Transaction.Commit();
                return true;
            }*/
            return false;
        }

        public void DoSetupBeforeMapping(IMapping mapping, SingleDefaultValue single)
        {
            string writerSql;
            writerSql = mapping.CustomFbQuery;
            if (string.IsNullOrEmpty(writerSql))
            {
                // NO CUSTOM QUERY PROVIDED, EITHER SELECT * WITHOUT WHERE (ALL ROWS)
                // OR GET SINGLE ROW.
                writerSql = "SELECT * FROM " + mapping.FirebirdTableName;
                if (single != null)
                {
                    writerSql = writerSql + " WHERE " + single.ValueField + " = @SINGLEVALUE";
                }

                _fbCommand = new FbCommand(writerSql);
                if (single != null)
                {
                    var incomingValue = single.Value;
                    if (incomingValue == null)
                        incomingValue = DBNull.Value;

                    _fbCommand.Parameters.Add("@SINGLEVALUE", incomingValue);
                    Log.Write("Single Query mode for one row from firebird table " + mapping.FirebirdTableName +
                              " with id " + StrHelpers.NameOfObjectStr(single.Value));
                    Log.Write("FbLookupSingleSQL:" + writerSql);
                }
                else
                {
                    Log.Write("Standard Query mode for all rows incoming from firebird table " +
                              mapping.FirebirdTableName);
                }
            }
            else
            {
                // custom query can restrict incoming data, or add extra fields.
                _fbCommand = new FbCommand(writerSql);
                Log.Write("Custom Query mode for " + mapping.FirebirdTableName);
            }

            try
            {
                _fbCommand.Connection = _fbConnection;
                _fbReader = _fbCommand.ExecuteReader();
                // throws formatexception if parameter types mismatch field types.
            }
            catch (FormatException ex)
            {
                if ((single != null) && single.Value is string)
                    throw new DataAccessLayerException("FB Lookup Failed / String Value Unexpected: " + writerSql, ex);
            }


            _fbReaderFieldLookup = new Dictionary<string, int>();
            for (var i = 0; i < _fbReader.FieldCount; i++)
            {
                _fbReaderFieldLookup[_fbReader.GetName(i)] = i;
            }


            _sqlTableName =  mapping.GetMicrosoftSqlTableName();


        }


        //  IDataAccessLayer internal implementor that generates the SQL
        private bool DoSetupFieldSelectCommandQuery(IMapping mapping,
            DataReaderQueryType aType,
            IField field,
            object incomingValue)
        {
            // TODO: Verifying correct generation of SQL should be possible without using the physical data access layer.  We shouldn't be generating SQL directly in this module.

            var sqlCmdStr = "";
            string exportedIdFieldName = StrHelpers.AddBrackets(field.OutField);

            Log.Write(LogLevel.LInfo, "DoSetupFieldSelectCommandQuery: mapping= "+mapping);

            // This default is really only for when you're feeling REALLY lucky. It is wrong more than it's right.

            // The goal here is to have enough type information that we know if we're doing something sane, and if not
            // to write out a warning or message to the user so they can fix the mapping.json content.
            if (!string.IsNullOrEmpty(field.SelectTable))
            {
                string fbLookupTableName = mapping.ReverseLookupTableName(field.SelectTable);
                // if  field.selectTable is dbo.User, then the original Firebird table name might be USERLIST, say.
                string inkeyfieldtype = null; // Firebird field.
                string inkeyfieldname = null;
                string outkeyfieldtype = null;
                string outkeyfieldname = null; // MS SQL field.
                if (mapping.GetKeyFieldInfo(fbLookupTableName,
                    out inkeyfieldtype,
                    out inkeyfieldname,
                    out outkeyfieldtype,
                    out outkeyfieldname))
                {
                    if (!string.IsNullOrEmpty(inkeyfieldname))
                    {
                        exportedIdFieldName = StrHelpers.AddBrackets(outkeyfieldname);
                    }
                }
            }
            ;


            /* TODO: Different logic for different field types or query types?

            switch  (AType)
            {
                case DataReaderQueryType.Top1Where:
                    exportedIdFieldName = field.outField; // TODO: Or something else fetched from schema?
                    break;

                case DataReaderQueryType.EntityNamed:
                    exportedIdFieldName = field.outField; // TODO: Or something else fetched from schema?
                    break;

                case DataReaderQueryType.AllRows:
                    exportedIdFieldName = field.outField; // TODO: Or something else fetched from schema?
                    break;


            }*/


            if (aType == DataReaderQueryType.JsonValue)
            {
                // Get value incoming from ExtJSON. (This is perhaps currently broken, unless we're copying this data across when we import)
                if (field.SelectCommand == null)
                {
                    sqlCmdStr = "select " + exportedIdFieldName + " from " +
                                mapping.GetMicrosoftSqlTableName() + " where " +
                                "JSON_VALUE(ExtJSON, '$." + field.InField + "') = @VALUE";
                    var sqlCmd = new SqlCommand(sqlCmdStr, _sqlConnection);
                    field.SelectCommand = sqlCmd;
                    if (incomingValue == null)
                        incomingValue = DBNull.Value;

                    sqlCmd.Parameters.AddWithValue("@VALUE", incomingValue);
                }
                else
                {
                    var sqlCmd = (SqlCommand) field.SelectCommand;
                    if (incomingValue == null)
                        incomingValue = DBNull.Value;
                    sqlCmd.Parameters[0].Value = incomingValue;
                }
            }
            else
            {
                if (field.SelectTable == null)
                {
                    throw new DataAccessLayerException(
                        "FB Lookup Failed. Select Field value is missing from lookup for " + field.InField, null);
                }

                if (field.SelectCommand == null)
                {
                    string selField = field.GetSelectFieldActual();  // If this is invalid or of the wrong type, things blow up much later. This is a source of PAIN in the current design.
                    // The last three values in the field definition SelectField,SelectDefaultValue, SelectFieldType are permitted so we can detect stupid/bad SQL.
                    // fields: [
                    //       [ InField, OutField, SelectTable, SelectField, SelectDefaultValue, SelectFieldType],
                    // ]
                    // If SelectFieldType=="int" but the value of the @Param we pass in is a string, or vice versa, a more sensible error message
                    // will help us find out what is going on and fix it, instead of being lost figuring out the internal failure.

                    // TODO: figure out if selField fieldtype is SelectFieldType, if SelectFieldType is provided.

                    if (aType == DataReaderQueryType.EntityNamed)
                    {
                        sqlCmdStr = "SELECT TOP 1 " + StrHelpers.AddBrackets(exportedIdFieldName) + "\n" +
                                    " FROM " + "\n" +
                                    StrHelpers.AddBrackets(field.SelectTable) + "\n" +
                                    " WHERE " + selField + " = @VALUE";
                    }
                    else
                    {
                        // simple lookup
                        sqlCmdStr = "SELECT TOP 1 " + StrHelpers.AddBrackets(exportedIdFieldName) + "\n" +
                                    " FROM " + "\n" +
                                    StrHelpers.AddBrackets(field.SelectTable) + "\n" +
                                    " WHERE " + selField + " = @VALUE";
                    }
                    var sqlCmd = new SqlCommand(sqlCmdStr, _sqlConnection);
                    field.SelectCommand = sqlCmd;
                    if (incomingValue == null)
                        incomingValue = DBNull.Value;
                    sqlCmd.Parameters.AddWithValue("@VALUE", incomingValue);
                }
                else
                {
                    var sqlCmd = (SqlCommand) field.SelectCommand;
                    Log.Write(LogLevel.LInfo, "Reusing existing lookup query command.");
                    sqlCmdStr = sqlCmd.CommandText;
                    if (incomingValue == null)
                        incomingValue = DBNull.Value;
                    sqlCmd.Parameters[0].Value = incomingValue;
                }

                /*

                simple lookup:

                if (field.selectCommand == null)
                {
                    // Direct Entity Copy without Integer Foreign Key Remap, but which
                    // may require a dependant sub-entity insertion (such as a STATE char(2) FK).
                    // Doesn't require any firebird SELECT
                    // Just requires the MS SQL selectCommand
                    string exportedIdFieldName = field.outField; // TODO: Or something else fetched from schema?
                    sqlCmdStr = "SELECT TOP 1 " + StrHelpers.AddBrackets(exportedIdFieldName) + "\n" +
                                       " FROM " + "\n" +
                                            StrHelpers.AddBrackets(field.selectTable) + "\n" +
                                       " WHERE " + StrHelpers.AddBrackets(field.selectField) + " = @VALUE";
                    var sqlCmd = new SqlCommand(sqlCmdStr, _sqlCommandIns.Connection);
                    field.selectCommand = sqlCmd;
                    if (incomingValue == null)
                        incomingValue = DBNull.Value;
                    sqlCmd.Parameters.AddWithValue("@VALUE", incomingValue);
                }
                else
                {
                    var sqlCmd = (SqlCommand)field.selectCommand;
                    if (incomingValue == null)
                        incomingValue = DBNull.Value;
                    sqlCmd.Parameters[0].Value = incomingValue;
                }


                */
            }

            Log.Write(mapping.FirebirdTableName + "." + field.InField + ": Lookup AType=" + aType);
            Log.Write("LookupMS:" + sqlCmdStr);
            Log.Write("@value = " + StrHelpers.NameOfObjectStr(incomingValue));
            return incomingValue != null;
        }


        static protected string DoGetLastSqlFrom(SqlCommand command, string fieldNameBad)
        {
            /* make it easy to copy debug output and run it from MS Sql Mgmt tools*/
            string cmd = command.CommandText;
            string parm, val, repl;


            for (var param = 0; param< command.Parameters.Count; param++)
            {
                parm = command.Parameters[param].ParameterName;
                val = StrHelpers.NameOfObjectStr(command.Parameters[param].Value);

                string parmx = parm.Substring(1);
                if (parmx == fieldNameBad)
                {
                    parmx = "!!!FK_CONSTRAINT!!! " + parmx;
                }
                repl = " /* " + parmx + " */ " + val;
                cmd = cmd.Replace(parm, repl);
            }
            return cmd;
        }
        // Exception Logging Handlers


        protected string GetLastSqlFromCommand(IDbCommand command)
        {
            return DoGetLastSqlFrom(command as SqlCommand, string.Empty );

        }

        protected string GetLastInsertSqlEx(int statementIndex,string fieldNameBad)
        {
            return DoGetLastSqlFrom(_sqlCommandIns[statementIndex], fieldNameBad);
        }

    }
}